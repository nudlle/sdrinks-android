package ru.nudlik.utils

import ru.nudlik.adapter.recipe.items.RecipeItem
import ru.nudlik.domain.Recipe

interface RecipeUtil {

    companion object {
        fun convert(data: List<Recipe>, lastRI: RecipeItem?, beforeLastRI: RecipeItem?): List<RecipeItem> {
            val recipes = ArrayList<Recipe>(data.size)
            recipes.addAll(data)
            val prec = recipes.size % 3
            val result = ArrayList<RecipeItem>()
            if (prec == 0) {
                fillRecipeItems(recipes, lastRI, beforeLastRI, result)
            } else {
                if (prec == 1) {
                    val tmp = recipes[recipes.size - 1]
                    recipes.removeAt(recipes.size - 1)
                    fillRecipeItems(recipes, lastRI, beforeLastRI, result)
                    when {
                        result.size >= 2 -> fillLastRecipeItem(result[result.size - 1], result[result.size - 2], result, tmp)
                        result.size >= 1 -> fillLastRecipeItem(result[result.size - 1], null, result, tmp)
                        else -> fillLastRecipeItem(null, null, result, tmp)
                    }
                } else {
                    val tmp1 = recipes[recipes.size - 1]
                    val tmp2 = recipes[recipes.size - 2]
                    recipes.removeAt(recipes.size - 1)
                    recipes.removeAt(recipes.size - 1)
                    fillRecipeItems(recipes, lastRI, beforeLastRI, result)
                    when {
                        result.size >= 2 -> fillLastRecipeItem(result[result.size - 1], result[result.size - 2], result, tmp1, tmp2)
                        result.size >= 1 -> fillLastRecipeItem(result[result.size - 1], null, result, tmp1, tmp2)
                        else -> fillLastRecipeItem(null, null, result, tmp1, tmp2)
                    }
                }
            }
            return result
        }

        private fun fillRecipeItems(recipes: MutableList<Recipe>, lastRI: RecipeItem?, beforeLastRI: RecipeItem?, out: MutableList<RecipeItem>) {
            if (lastRI == null) {
                add(recipes, out, RecipeItem.SIMPLE_ROW)
            } else {
                if (beforeLastRI == null) {
                    when (lastRI.typeRow) {
                        RecipeItem.SIMPLE_ROW -> {
                            addImpl(lastRI, recipes, out, RecipeItem.LEFT_ROW)
                        }
                        RecipeItem.LEFT_ROW -> {
                            addImpl(lastRI, recipes, out, RecipeItem.SIMPLE_ROW)
                        }
                        RecipeItem.RIGHT_ROW -> {
                            addImpl(lastRI, recipes, out, RecipeItem.SIMPLE_ROW)
                        }
                    }
                } else {
                    when (lastRI.typeRow) {
                        RecipeItem.SIMPLE_ROW -> {
                            if (beforeLastRI.typeRow == RecipeItem.LEFT_ROW) {
                                addImpl(lastRI, recipes, out, RecipeItem.RIGHT_ROW)
                            } else {
                                addImpl(lastRI, recipes, out, RecipeItem.LEFT_ROW)
                            }

                        }
                        RecipeItem.LEFT_ROW -> {
                            addImpl(lastRI, recipes, out, RecipeItem.SIMPLE_ROW)
                        }
                        RecipeItem.RIGHT_ROW -> {
                            addImpl(lastRI, recipes, out, RecipeItem.SIMPLE_ROW)
                        }
                    }
                }
            }
        }

        private fun fillLastRecipeItem(lastRI: RecipeItem?,
                                       beforeRI: RecipeItem?,
                                       out: MutableList<RecipeItem>,
                                       one: Recipe) {
            if (lastRI == null) {
                val recipeItem = RecipeItem(RecipeItem.SIMPLE_ROW)
                recipeItem.items.add(one)
                out.add(recipeItem)
            } else {
                when (lastRI.typeRow) {
                    RecipeItem.SIMPLE_ROW -> {
                        if (beforeRI == null) {
                            val recipeItem = RecipeItem(RecipeItem.LEFT_ROW)
                            recipeItem.items.add(one)
                            out.add(recipeItem)
                        } else {
                            if (beforeRI.typeRow == RecipeItem.LEFT_ROW) {
                                val recipeItem = RecipeItem(RecipeItem.RIGHT_ROW)
                                recipeItem.items.add(one)
                                out.add(recipeItem)
                            } else {
                                val recipeItem = RecipeItem(RecipeItem.LEFT_ROW)
                                recipeItem.items.add(one)
                                out.add(recipeItem)
                            }
                        }
                    }
                    RecipeItem.LEFT_ROW -> {
                        val recipeItem = RecipeItem(RecipeItem.SIMPLE_ROW)
                        recipeItem.items.add(one)
                        out.add(recipeItem)
                    }
                    RecipeItem.RIGHT_ROW -> {
                        val recipeItem = RecipeItem(RecipeItem.SIMPLE_ROW)
                        recipeItem.items.add(one)
                        out.add(recipeItem)
                    }
                }
            }
        }

        private fun fillLastRecipeItem(lastRI: RecipeItem?,
                                       beforeLastRI: RecipeItem?,
                                       out: MutableList<RecipeItem>,
                                       one: Recipe, two: Recipe) {
            if (lastRI == null) {
                val recipeItem = RecipeItem(RecipeItem.SIMPLE_ROW)
                recipeItem.items.add(one)
                recipeItem.items.add(two)
                out.add(recipeItem)
            } else {
                when (lastRI.typeRow) {
                    RecipeItem.SIMPLE_ROW -> {
                        if (beforeLastRI == null) {
                            val recipeItem = RecipeItem(RecipeItem.LEFT_ROW)
                            recipeItem.items.add(one)
                            recipeItem.items.add(two)
                            out.add(recipeItem)
                        } else {
                            if (beforeLastRI.typeRow == RecipeItem.LEFT_ROW) {
                                val recipeItem = RecipeItem(RecipeItem.RIGHT_ROW)
                                recipeItem.items.add(one)
                                recipeItem.items.add(two)
                                out.add(recipeItem)
                            } else {
                                val recipeItem = RecipeItem(RecipeItem.LEFT_ROW)
                                recipeItem.items.add(one)
                                recipeItem.items.add(two)
                                out.add(recipeItem)
                            }
                        }
                    }
                    RecipeItem.LEFT_ROW -> {
                        val recipeItem = RecipeItem(RecipeItem.SIMPLE_ROW)
                        recipeItem.items.add(one)
                        recipeItem.items.add(two)
                        out.add(recipeItem)
                    }
                    RecipeItem.RIGHT_ROW -> {
                        val recipeItem = RecipeItem(RecipeItem.SIMPLE_ROW)
                        recipeItem.items.add(one)
                        recipeItem.items.add(two)
                        out.add(recipeItem)
                    }
                }
            }
        }

        private fun addImpl(
            lastRecipeItem: RecipeItem,
            recipes: MutableList<Recipe>,
            out: MutableList<RecipeItem>,
            type: Int
        ) {
            if (lastRecipeItem.items[1] == null) {
                addTwoToLastRecipeItem(lastRecipeItem, recipes)
            } else if (lastRecipeItem.items[2] == null) {
                addOneLastRecipeItem(lastRecipeItem, recipes)
            }
            add(recipes, out, type)
        }

        private fun addOneLastRecipeItem(last: RecipeItem, recipes: MutableList<Recipe>) {
            last.items.add(recipes[0])
            recipes.removeAt(0)
        }

        private fun addTwoToLastRecipeItem(last: RecipeItem, recipes: MutableList<Recipe>) {
            last.items.add(recipes[0])
            last.items.add(recipes[1])
            recipes.removeAt(0)
            recipes.removeAt(0)
        }

        private fun add(recipes: List<Recipe>, out: MutableList<RecipeItem>, t: Int) {
            var type = t
            var i = 0
            var j = 0
            while (i < recipes.size) {
                out.add(getRecipeItem(type, recipes[j++], recipes[j++], recipes[j++]))
                type = if (out.size > 2) {
                    getNextType(type, out[out.size - 2].typeRow)
                } else {
                    getNextType(type, type)
                }
                i += 3
            }
        }

        private fun getNextType(current: Int, beforeCurrent: Int): Int {
            return if (current == RecipeItem.SIMPLE_ROW) {
                if (beforeCurrent == RecipeItem.LEFT_ROW) {
                    RecipeItem.RIGHT_ROW
                } else {
                    RecipeItem.LEFT_ROW
                }
            } else if (current == RecipeItem.LEFT_ROW) {
                RecipeItem.SIMPLE_ROW
            } else {
                RecipeItem.SIMPLE_ROW
            }
        }

        private fun getRecipeItem(type: Int, first: Recipe, second: Recipe, third: Recipe): RecipeItem {
            val item = RecipeItem(type)
            item.items.add(first)
            item.items.add(second)
            item.items.add(third)
            return item
        }
    }
}