package ru.nudlik.utils

import android.animation.Animator
import android.view.ViewAnimationUtils
import android.view.ViewGroup
import kotlin.math.hypot
import kotlin.math.max
import ru.nudlik.extentions.gone
import ru.nudlik.extentions.visible

interface SplashAnimUtil {

    companion object {
        var x: Int = 0
        var y: Int = 0
        fun viewLoading(isOpen: Boolean,
                        layoutContent: ViewGroup,
                        layoutMain: ViewGroup,
                        layoutProgress: ViewGroup) {

            if (!isOpen) {
                val startRadius = 0
                val endRadius = hypot(layoutMain.width.toDouble(), layoutMain.height.toDouble()).toInt()
                val anim =
                    ViewAnimationUtils.createCircularReveal(layoutProgress, x, y, startRadius.toFloat(), endRadius.toFloat())

                layoutProgress.visible()
                anim.start()
            } else {
                val startRadius = max(layoutContent.width, layoutContent.height)
                val endRadius = 0

                val anim =
                    ViewAnimationUtils.createCircularReveal(layoutProgress, x, y, startRadius.toFloat(), endRadius.toFloat())
                anim.addListener(object : Animator.AnimatorListener {
                    override fun onAnimationStart(animator: Animator) {

                    }

                    override fun onAnimationEnd(animator: Animator) {
                        layoutProgress.gone()
                    }

                    override fun onAnimationCancel(animator: Animator) {

                    }

                    override fun onAnimationRepeat(animator: Animator) {

                    }
                })
                anim.start()
            }
        }
    }
}