package ru.nudlik.security

import ru.nudlik.domain.User

import java.io.Serializable
import java.util.Objects

class UserRequest : Serializable {
    var user: User? = null
    var timestamp: Long = 0

    constructor()
    constructor(user: User, timestamp: Long) {
        this.user = user
        this.timestamp = timestamp
    }

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (other == null || javaClass != other.javaClass) return false
        val that = other as UserRequest?
        return timestamp == that!!.timestamp && user == that.user
    }

    override fun hashCode(): Int {
        return Objects.hash(user, timestamp)
    }

    override fun toString(): String {
        return "UserRequest{" +
                "user=" + user +
                ", timestamp=" + timestamp +
                '}'.toString()
    }

    companion object {
        private const val serialVersionUID = -123400038833L
    }
}
