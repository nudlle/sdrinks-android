package ru.nudlik.security

import android.security.keystore.KeyGenParameterSpec
import android.security.keystore.KeyProperties
import android.util.Base64
import java.lang.Exception
import java.lang.IllegalArgumentException
import java.lang.IllegalStateException
import java.security.*
import java.security.spec.MGF1ParameterSpec
import java.security.spec.X509EncodedKeySpec
import javax.crypto.Cipher
import javax.crypto.spec.OAEPParameterSpec
import javax.crypto.spec.PSource

class CryptoUtilImpl : CryptoUtil {
    private fun loadKeyStore(): KeyStore {
        try {
            val keyStore = KeyStore.getInstance("AndroidKeyStore")
            keyStore.load(null)
            return keyStore
        } catch (e: Exception) {
            e.printStackTrace()
        }
        throw IllegalStateException("Could not load keystore")
    }

    private fun getEncodeCipher(alias: String): Cipher {
        val cipher = getCipherInstance()
        val keyStore = loadKeyStore()
        generateKeyIfNecessary(keyStore, alias)
        initEncodeCipher(cipher, alias, keyStore)
        return cipher
    }
    private fun generateKeyIfNecessary(keyStore: KeyStore, alias: String): Boolean {
        try {
            return keyStore.containsAlias(alias) || generateKey(alias)
        } catch (e: KeyStoreException) {
            e.printStackTrace()
        }
        return false
    }
    private fun generateKey(alias: String): Boolean {
        try {
            val keyGenerator =
                KeyPairGenerator.getInstance(KeyProperties.KEY_ALGORITHM_RSA, "AndroidKeyStore")
            keyGenerator.initialize(
                KeyGenParameterSpec.Builder(alias,
                    KeyProperties.PURPOSE_ENCRYPT or KeyProperties.PURPOSE_DECRYPT)
                    .setDigests(KeyProperties.DIGEST_SHA256)
                    .setEncryptionPaddings(KeyProperties.ENCRYPTION_PADDING_RSA_OAEP)
                    .setUserAuthenticationRequired(false).build())
            keyGenerator.generateKeyPair()
            return true
        } catch (e: Exception) {
            e.printStackTrace()
        }
        return false
    }
    private fun decode(encoded: String, cipher: Cipher): String {
        try {
            val bytes = Base64.decode(encoded, Base64.NO_WRAP)
            return String(cipher.doFinal(bytes))
        } catch (e: Exception) {
            throw IllegalStateException("Decoding error: ${e.message}")
        }
    }
    private fun getCipherInstance(): Cipher {
        try {
            return Cipher.getInstance("RSA/ECB/OAEPWithSHA-256AndMGF1Padding")
        } catch (e: Exception) {
            throw IllegalStateException("Could not get cipher instance")
        }
    }
    private fun initEncodeCipher(cipher: Cipher, alias: String, keyStore: KeyStore) {
        try {
            val key = keyStore.getCertificate(alias).publicKey
            val unrestricted = KeyFactory.getInstance(key.algorithm).generatePublic(X509EncodedKeySpec(key.encoded))
            val spec = OAEPParameterSpec("SHA-256", "MGF1", MGF1ParameterSpec.SHA1, PSource.PSpecified.DEFAULT)
            cipher.init(Cipher.ENCRYPT_MODE, unrestricted, spec)
        } catch (e: Exception) {
            throw IllegalArgumentException("Could not initialize encoder cipher: ${e.message}")
        }
    }
    private fun initDecodeCipher(cipher: Cipher, alias: String) {
        try {
            val keyStore = loadKeyStore()
            val key = keyStore.getKey(alias, null) as PrivateKey
            cipher.init(Cipher.DECRYPT_MODE, key)
        } catch (e: Exception) {
            throw IllegalStateException("Could not initialize decoder cipher: ${e.message}")
        }
    }
    override fun encode(alias: String, input: String): String {
        try {
            val cipher = getEncodeCipher(alias)
            val bytes = cipher.doFinal(input.toByteArray())
            return Base64.encodeToString(bytes, Base64.NO_WRAP)
        } catch (e: Exception) {
            e.printStackTrace()
        }
        return ""
    }
    override fun decode(alias: String, encoded: String): String {
        try {
            val cipher = getCipherInstance()
            initDecodeCipher(cipher, alias)
            val bytes = Base64.decode(encoded, Base64.NO_WRAP)
            return String(cipher.doFinal(bytes))
        } catch (e: Exception) {
            e.printStackTrace()
            throw IllegalStateException("Decoding error: ${e.message}")
        }
    }
    override fun deleteKey(alias: String) {
        val keyStore = loadKeyStore()
        try {
            keyStore.deleteEntry(alias)
        } catch (e: Exception) {
            throw IllegalStateException("Could not delete key: ${e.message}")
        }
    }
}