package ru.nudlik.security

import java.io.Serializable
import java.util.Objects

class SessionToken(var id: String?) : Serializable {
    var token: String? = null
    var timestamp: Long = 0

    init {
        this.timestamp = System.currentTimeMillis()
        this.token = ""
    }

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (other == null || javaClass != other.javaClass) return false
        val that = other as SessionToken?
        return timestamp == that!!.timestamp &&
                id == that.id &&
                token == that.token
    }

    override fun hashCode(): Int {
        return Objects.hash(id, token, timestamp)
    }

    override fun toString(): String {
        return "SessionToken{" +
                "id='" + id + '\''.toString() +
                ", token='" + token + '\''.toString() +
                ", timestamp=" + timestamp +
                '}'.toString()
    }

    companion object {
        private const val serialVersionUID = 122123214010234L
    }
}
