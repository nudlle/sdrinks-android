package ru.nudlik.listener

import android.text.Editable
import android.text.TextWatcher
import android.view.View
import com.google.android.material.textfield.TextInputLayout

class EditTextErrorCleaner(private val til: TextInputLayout) : TextWatcher {
    override fun afterTextChanged(s: Editable?) {}

    override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}

    override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
        til.error = null
    }
}

interface ItemClickListener {
    fun onItemClick(v: View?, position: Int, ext: Any? = null)
}

class FormatChecker(
    private val totalSymbols: Int,
    private val totalDigits: Int,
    private val dividerModulo: Int,
    private val divider: Char,
    private val textInputLayout: TextInputLayout
) : TextWatcher {
    private val dividerPosition: Int = dividerModulo - 1

    override fun beforeTextChanged(charSequence: CharSequence, i: Int, i1: Int, i2: Int) {

    }

    override fun onTextChanged(charSequence: CharSequence, i: Int, i1: Int, i2: Int) {
        if (textInputLayout.error != null && textInputLayout.error!!.toString().isNotEmpty()) {
            textInputLayout.error = null
        }
    }

    override fun afterTextChanged(editable: Editable) {
        if (!isInputCorrect(editable, totalSymbols, dividerModulo, divider)) {
            editable.replace(
                0, editable.length,
                concatString(getDigitsArray(editable, totalDigits), dividerPosition, divider)
            )
        }
    }

    private fun isInputCorrect(s: Editable, size: Int, dividerPosition: Int, divider: Char): Boolean {
        var isCorrect = s.length <= size
        for (i in 0 until s.length) {
            if (i > 0 && (i + 1) % dividerPosition == 0) {
                isCorrect = isCorrect and (divider == s[i])
            } else {
                isCorrect = isCorrect and Character.isDigit(s[i])
            }
        }
        return isCorrect
    }

    private fun concatString(digits: CharArray, dividerPosition: Int, divider: Char): String {
        val formatted = StringBuilder()
        for (i in digits.indices) {
            if (digits[i].toInt() != 0) {
                formatted.append(digits[i])
                if (i > 0 && i < digits.size - 1 && (i + 1) % dividerPosition == 0) {
                    formatted.append(divider)
                }
            }
        }
        return formatted.toString()
    }

    private fun getDigitsArray(s: Editable, size: Int): CharArray {
        val digits = CharArray(size)
        var index = 0
        var i = 0
        while (i < s.length && index < size) {
            val current = s[i]
            if (Character.isDigit(current)) {
                digits[index] = current
                index++
            }
            i++
        }
        return digits
    }
}

interface BuySubscriptionListener {
    fun buySub(price: Double)
}

interface FinishWithGoto {
    fun goto(tag: Any?)
}