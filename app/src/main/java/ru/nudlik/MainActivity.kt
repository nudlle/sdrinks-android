package ru.nudlik

import android.app.Activity
import android.app.SearchManager
import android.content.Context
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.*
import androidx.appcompat.widget.SearchView
import androidx.appcompat.app.ActionBarDrawerToggle
import androidx.appcompat.widget.Toolbar
import androidx.coordinatorlayout.widget.CoordinatorLayout
import androidx.core.view.GravityCompat
import androidx.drawerlayout.widget.DrawerLayout
import com.google.android.material.appbar.CollapsingToolbarLayout
import com.google.android.material.floatingactionbutton.FloatingActionButton
import com.google.android.material.navigation.NavigationView
import okhttp3.Interceptor
import okhttp3.Response
import ru.nudlik.adapter.recipe.items.RecipeItem
import ru.nudlik.domain.Recipe
import ru.nudlik.domain.User
import ru.nudlik.extentions.*
import ru.nudlik.fragment.FragmentManagerHelper
import ru.nudlik.fragment.snackbar.SnackbarMaker
import ru.nudlik.fragment.toast.ToastMaker
import ru.nudlik.interfaces.RequestNextPageListener
import ru.nudlik.listener.ItemClickListener
import ru.nudlik.provider.utils.ProviderHelper
import ru.nudlik.response.ServerResponseCode
import ru.nudlik.security.CryptoUtil
import ru.nudlik.security.CryptoUtilImpl
import ru.nudlik.security.SessionToken
import ru.nudlik.security.UserRequest
import ru.nudlik.service.rest.RestClient
import ru.nudlik.service.rest.RestUtilService
import ru.nudlik.singleton.SingletonInitializer
import ru.nudlik.singleton.UserSingleton
import ru.nudlik.utils.RecipeUtil
import ru.nudlik.widget.PulseLayout
import android.content.ComponentName
import android.view.MotionEvent
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import ru.nudlik.activity.*
import ru.nudlik.fragment.bottomsheet.CustomizeSearchBSDialog
import ru.nudlik.singleton.CurrentActiveRecipe
import ru.nudlik.utils.SplashAnimUtil
import ru.nudlik.widget.CustomSearchView


class MainActivity : AppCompatActivity(), NavigationView.OnNavigationItemSelectedListener, SwipeRefreshLayout.OnRefreshListener {
    private var user: User? = null
    private lateinit var navHeader: View
    private lateinit var toolbar: Toolbar
    private lateinit var navigationView: NavigationView
    private lateinit var parentContainer: CoordinatorLayout
    private lateinit var progressHolder: RelativeLayout
    private lateinit var fragmentContainer: FrameLayout
    private lateinit var restUtilService: RestUtilService
    private val recipes = ArrayList<RecipeItem>()
    private lateinit var fragmentHelper: FragmentManagerHelper
    private lateinit var createRecipePulse: PulseLayout
    private lateinit var searchView: CustomSearchView
    private lateinit var customizeSearchItem: MenuItem
    private lateinit var createRecipeFab: FloatingActionButton
    private lateinit var swipeToRefresh: SwipeRefreshLayout

    override fun dispatchTouchEvent(ev: MotionEvent?): Boolean {
        if (ev != null && ev.action == MotionEvent.ACTION_DOWN) {
            SplashAnimUtil.x = ev.x.toInt()
            SplashAnimUtil.y = ev.y.toInt() - 100
        }
        return super.dispatchTouchEvent(ev)
    }

    private val loginUserCallback = object : RestUtilService.ResponseCallback {
        override fun <T> success(data: T?) {
            val st = data as SessionToken
            SingletonInitializer.addCredentials(st.id!!, st.token!!)

            restUtilService.getUser(st.id!!, object : RestUtilService.ResponseCallback {
                override fun <T> success(data: T?) {
                    user = data as User
                    user?.avatarUrl?.let {
                        navHeader.findViewById<ImageView>(R.id.avatar)
                            .loadUri("${RestClient.URL}/resources/avatars/${user?.userId}/$it")
                    }
                    Thread().run {
                        UserSingleton.initUser(user!!)
                        UserSingleton.updateUserInDb(contentResolver)
                    }
                }
            })


        }
        override fun errorCode(responseCode: ServerResponseCode?) {
            ToastMaker.showLongToast(this@MainActivity, "Error: $responseCode")
        }
    }

    private val loadRequest = object : RequestNextPageListener {
        override fun loadNextPage(page: Int, size: Int, callback: RestUtilService.ResponseCallback) {
            restUtilService.getRecipes(page, size, callback)
        }
    }

    private fun initSingletons() {
        SingletonInitializer.init(this@MainActivity, AuthFailInterceptor())
        restUtilService = RestUtilService(this@MainActivity)
        restUtilService.showExceptionDialog = false
        loginUser(loginUserCallback)
    }

    private fun loginUser(loginCallback: RestUtilService.ResponseCallback) {
        if (user != null) {
            val loginUser = User()
            loginUser.userId = user!!.userId
            loginUser.password = CryptoUtilImpl().decode(CryptoUtil.PIN_ALIAS, user!!.password!!)
            val userRequest = UserRequest(loginUser, System.currentTimeMillis())
            restUtilService.loginUser(userRequest, loginCallback)
        }
    }

    private fun initUser() {
        UserSingleton.initUser(contentResolver)
        user = UserSingleton.getUser()
    }

    private fun showRecipeActivity(recipe: Recipe?, position: Int, ext: Int) {
        //TODO разобраться с размером родительского контейнера
        if (ext != -2) SplashAnimUtil.viewLoading(false, fragmentContainer, parentContainer, progressHolder)
        val intent = Intent(this@MainActivity, RecipeViewActivity::class.java)
        intent.putExtra(RECIPE_CREATION_FLAG, false)
        CurrentActiveRecipe.recipe = recipe
        intent.putExtra(RECIPE_POSITION_FLAG, position)
        intent.putExtra(RECIPE_POSITION_EXT_FLAG, ext)
        startActivityForResult(intent, VIEW_RECIPE_REQUEST)
    }

    private val recipeItemClickListener = object : ItemClickListener {
        override fun onItemClick(v: View?, position: Int, ext: Any?) {
            showRecipeActivity(recipes[position].items[ext as Int], position, ext)
        }
    }

    private val navRecipeClickListener = object : ItemClickListener {
        override fun onItemClick(v: View?, position: Int, ext: Any?) {
            showRecipeActivity(ext as Recipe, position, -2)
        }
    }

    private val likedRecipeDeleteListener = object : ItemClickListener {
        override fun onItemClick(v: View?, position: Int, ext: Any?) {
            if (user != null) {
                restUtilService.deleteLikeRecipe(user!!.userId!!, ext as String, object : RestUtilService.ResponseCallback {
                    override fun <T> success(data: T?) {
                        user!!.likedRecipes.removeAt(position)
                        ProviderHelper.deleteLikedRecipe(ext, contentResolver)
                    }
                })
            } else {
               ProviderHelper.deleteLikedRecipe(ext as String, contentResolver)
            }
        }
    }

    private val addedRecipeDeleteListener = object : ItemClickListener {
        override fun onItemClick(v: View?, position: Int, ext: Any?) {
            if (user != null) {
                restUtilService.deleteRecipe(ext as String, user!!.userId!!, object : RestUtilService.ResponseCallback {
                    override fun <T> success(data: T?) {
                        ProviderHelper.deleteOwnRecipe(ext, contentResolver)
                        user!!.ownRecipes.removeAt(position)
                    }
                })
            }
        }
    }

    private val loadRecipesCallback = object : RestUtilService.ResponseCallback {
        override fun <T> success(data: T?) {
            showLoading(false)
            if (data is List<*>) {
                fillRecipeItems(data.filterIsInstance<Recipe>())
                fragmentHelper.addRecipes(recipes)
            }
        }
        override fun errorCode(responseCode: ServerResponseCode?) {
            showLoading(false)
        }
        override fun exception(e: Throwable?) {
            showLoading(false)
        }
    }

    private fun fillRecipeItems(data: List<Recipe>) {
        val converted = RecipeUtil.convert(data, null, null)
        recipes.addAll(converted)
    }

    private fun loadContent() {
        showLoading(true)
        restUtilService.getRecipes(0, 36, loadRecipesCallback)
    }

    private val searchTextListener = object : SearchView.OnQueryTextListener {
        override fun onQueryTextSubmit(query: String?): Boolean {
            fragmentHelper.filterFromSearch(query)
            return false
        }

        override fun onQueryTextChange(newText: String?): Boolean {
            fragmentHelper.filterFromSearch(newText)
            return false
        }

    }

    private val createNewRecipeClickListener = View.OnClickListener {
        val intent: Intent
        val rCode: Int
        if (user == null) {
            intent = Intent(this@MainActivity, LoginActivity::class.java)
            rCode = LOGIN_USER_REQUEST
        } else {
            intent = Intent(this@MainActivity, NewRecipeActivity::class.java)
            rCode = NEW_RECIPE_REQUEST
        }
        SplashAnimUtil.viewLoading(false, fragmentContainer, parentContainer, progressHolder)
        startActivityForResult(intent, rCode)
    }

    private val actionSearchClickListener = View.OnClickListener {
        if (searchView.query.isEmpty()) {
            searchView.setQuery("none", true)
        } else {
            searchView.setQuery(searchView.query, true)
        }
    }

    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.nav_added -> {
                toolbar.setTitle(R.string.added_recipes)
                fragmentHelper.showAddedRecipesFragment(user?.ownRecipes!!, navRecipeClickListener, addedRecipeDeleteListener)
                createRecipePulse.gone()
                createRecipePulse.stop()
                searchView.setSearchableInfo(null)
                searchView.setOnQueryTextListener(searchTextListener)
                customizeSearchItem.setOnMenuItemClickListener(null)
            }
            R.id.nav_liked -> {
                toolbar.setTitle(R.string.favorite_recipes)
                fragmentHelper.showLikedRecipesFragment(user?.likedRecipes ?:
                ProviderHelper.getLikedRecipes(contentResolver), navRecipeClickListener, likedRecipeDeleteListener)
                createRecipePulse.gone()
                createRecipePulse.stop()
                searchView.setSearchableInfo(null)
                searchView.setOnQueryTextListener(searchTextListener)
                customizeSearchItem.setOnMenuItemClickListener(null)
            }
            R.id.nav_store -> {

            }
            R.id.nav_cart -> {

            }
            R.id.nav_orders -> {

            }
            R.id.nav_profile -> {
                if (user == null) {
                    val intent = Intent(this@MainActivity, LoginActivity::class.java)
                    startActivityForResult(intent, LOGIN_USER_REQUEST)
                } else {
                    val intent = Intent(this@MainActivity, ProfileActivity::class.java)
                    startActivityForResult(intent, PROFILE_USER_REQUEST)
                }
            }
            R.id.nav_about -> {
                val intent = Intent(this@MainActivity, AboutAppActivity::class.java)
                startActivity(intent)
            }
        }

        val drawer = findViewById<DrawerLayout>(R.id.drawer_layout)
        drawer.closeDrawer(GravityCompat.START)
        return true
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        setTheme(R.style.AppTheme)
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        toolbar = findViewById(R.id.toolbar_main)
        toolbar.setTitle(R.string.app_name)
        setSupportActionBar(toolbar)
        val collapsingToolbar = findViewById<CollapsingToolbarLayout>(R.id.collapsing_toolbar_main)
        collapsingToolbar.isTitleEnabled = false

        val drawer = findViewById<DrawerLayout>(R.id.drawer_layout)
        val toggle = ActionBarDrawerToggle(
            this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close
        )
        drawer.addDrawerListener(toggle)
        toggle.syncState()
        initUser()
        navigationView = findViewById(R.id.nav_view)
        navigationView.setNavigationItemSelectedListener(this)
        /** цветные иконки в нав. баре */
//        navigationView.itemIconTintList = null
        navHeader = navigationView.getHeaderView(0)
        if (user == null) {
            navigationView.menu.setGroupEnabled(R.id.nav_menu_group_1, false)
        } else {
            navHeader.findViewById<TextView>(R.id.user_email).apply {
                text = user?.email
            }
        }

        createRecipeFab = findViewById(R.id.create_new_recipe_fab)
        createRecipeFab.setOnClickListener(createNewRecipeClickListener)

        createRecipePulse = findViewById(R.id.add_action_pulse_layout)

        parentContainer = findViewById(R.id.parent_container_main)

        progressHolder = findViewById(R.id.progress_holder)
        fragmentContainer = findViewById(R.id.main_fragment_container)

        fragmentHelper = FragmentManagerHelper(supportFragmentManager)
        fragmentHelper.showRecipesFragment(recipes, loadRequest, recipeItemClickListener)

        swipeToRefresh = findViewById(R.id.swipe_refresh)
        swipeToRefresh.setOnRefreshListener(this)
        swipeToRefresh.setColorSchemeResources(android.R.color.holo_blue_bright,
            android.R.color.holo_green_light, android.R.color.holo_orange_light,
            android.R.color.holo_red_light)
        swipeToRefresh.setProgressViewOffset(false, 0, 100)

        initSingletons()

        loadContent()
    }

    override fun onRefresh() {
        when (fragmentHelper.getCurrentActiveFragmentTag()) {
            FragmentManagerHelper.LIKED_RECIPES_FRAG_TAG ->
                fragmentHelper.reloadLikedRecipesFragment(user?.likedRecipes ?: ProviderHelper.getLikedRecipes(contentResolver), navRecipeClickListener, likedRecipeDeleteListener)
            FragmentManagerHelper.ADDED_RECIPES_FRAG_TAG ->
                fragmentHelper.reloadAddedRecipesFragment(user?.ownRecipes!!, navRecipeClickListener, addedRecipeDeleteListener)
            FragmentManagerHelper.RECIPES_FRAG_TAG -> {
                recipes.clear()
                fragmentHelper.reloadMainFragment(recipes, loadRequest, recipeItemClickListener)
                restUtilService.getRecipes(0, 36, loadRecipesCallback)
            }
        }
        swipeToRefresh.postDelayed({
            swipeToRefresh.isRefreshing = false
        }, 2000)
    }

    override fun onResume() {
        super.onResume()
        createRecipePulse.start()
    }

    override fun onPause() {
        super.onPause()
        createRecipePulse.stop()
    }

    private fun showLoading(isLoading: Boolean) {
        if (isLoading) {
            progressHolder.visible()
            fragmentContainer.gone()
        } else {
            progressHolder.gone()
            fragmentContainer.visible()
        }
    }

    private val showHideSearchListener = object : CustomSearchView.OnCollapsedExpandedListener {
        override fun onCollapsed() {
            customizeSearchItem.isVisible = false
            createRecipeFab.setImageDrawable(getDrawable(R.drawable.ic_add_white_24dp))
            createRecipeFab.setOnClickListener(createNewRecipeClickListener)
        }

        override fun onExpanded() {
            customizeSearchItem.isVisible = true
            createRecipeFab.setImageDrawable(getDrawable(R.drawable.ic_search_white_24dp))
            createRecipeFab.setOnClickListener(actionSearchClickListener)
        }
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.main_toolbar, menu)
        searchView =  menu?.findItem(R.id.action_search)?.actionView as CustomSearchView
        customizeSearchItem = menu.findItem(R.id.action_customize_search)
        searchView.setOnCollapsedExpandedListener(showHideSearchListener)
        initSearchableInfo()
        return true
    }

    private fun initSearchableInfo() {
        val searchManager = getSystemService(Context.SEARCH_SERVICE) as SearchManager
        searchView.setSearchableInfo(searchManager.getSearchableInfo(ComponentName(this@MainActivity,
            SearchResultsActivity::class.java)))
        searchView.setOnQueryTextListener(null)
        customizeSearchItem.setOnMenuItemClickListener {
            val cbs = CustomizeSearchBSDialog.createInstance()
            cbs.show(supportFragmentManager, CustomizeSearchBSDialog.TAG)
            true
        }
    }

    override fun onBackPressed() {
        val drawer = findViewById<DrawerLayout>(R.id.drawer_layout)
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START)
        } else {
            if (fragmentHelper.isCurrentNavFragShowing()) {
                navigationView.setCheckedItem(R.id.menu_none_2)
                createRecipePulse.start()
                createRecipePulse.visible()
                toolbar.setTitle(R.string.app_name)
                initSearchableInfo()
            }
            super.onBackPressed()
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        SplashAnimUtil.viewLoading(true, fragmentContainer, parentContainer, progressHolder)
        when (requestCode) {
            LOGIN_USER_REQUEST -> {
                if (resultCode == Activity.RESULT_OK) {
                    val usr = UserSingleton.getUser()
                    if (usr != null) {
                        user = usr
                        navigationView.menu.setGroupEnabled(R.id.nav_menu_group_1, true)
                        navHeader.findViewById<TextView>(R.id.user_email).apply {
                            text = user?.email
                        }

                        user?.avatarUrl?.let {
                            if (it.isNotEmpty()) {
                                navHeader.findViewById<ImageView>(R.id.avatar).loadUri("${RestClient.URL}/resources/avatars/${user?.userId}/$it")
                            }
                        }
                        ProviderHelper.getLikedRecipes(contentResolver).forEach { recipeId ->
                            val ifAlreadyExists = user?.likedRecipes?.contains(recipeId) ?: false
                            if (!ifAlreadyExists) {
                                restUtilService.addLikeRecipe(user?.userId!!, recipeId)
                            }
                        }
                        user?.likedRecipes?.apply {
                            forEach {
                                ProviderHelper.addLikedRecipe(it, contentResolver)
                            }
                        }
                        user?.ownRecipes?.apply {
                            forEach {
                                ProviderHelper.addOwnRecipe(it, contentResolver)
                            }
                        }
                        SnackbarMaker.showLongSnackbar(parentContainer, R.string.login_register_success, R.string.ok)
                    }
                }
            }
            PROFILE_USER_REQUEST -> {
                if (resultCode == Activity.RESULT_OK) {
                    val usr = UserSingleton.getUser()
                    if (usr != null) {
                        if (UserSingleton.isChanged()) {
                            usr.avatarUrl?.let {
                                if (it.isNotEmpty()) {
                                    navHeader.findViewById<ImageView>(R.id.avatar).reloadUri("${RestClient.URL}/resources/avatars/${user?.userId}/$it")
                                }
                            }
                        }
                        user = usr
                    } else {
                        user?.ownRecipes?.apply {
                            forEach {
                                ProviderHelper.deleteOwnRecipe(it, contentResolver)
                            }
                        }
                        user = null
                        SingletonInitializer.deleteCredentials()
                        navHeader.findViewById<ImageView>(R.id.avatar).loadVectorDrawable(R.drawable.ic_no_name_avatar)
                        navigationView.menu.setGroupEnabled(R.id.nav_menu_group_1, false)
                        navHeader.findViewById<TextView>(R.id.user_email).apply {
                            setText(R.string.user_not_login)
                        }
                    }
                } else if (resultCode == GOTO_LIKED_RECIPES) {
                    navigationView.setCheckedItem(R.id.nav_liked)
                    onNavigationItemSelected(navigationView.menu.findItem(R.id.nav_liked))
                } else if (resultCode == GOTO_OWN_RECIPES) {
                    navigationView.setCheckedItem(R.id.nav_added)
                    onNavigationItemSelected(navigationView.menu.findItem(R.id.nav_added))
                }
            }
            NEW_RECIPE_REQUEST -> {
                if (resultCode == Activity.RESULT_OK) {
                    SnackbarMaker.showLongSnackbar(parentContainer, R.string.new_recipe_added, R.string.ok)
                } else {
                    SnackbarMaker.showLongSnackbar(parentContainer, R.string.new_recipe_add_failed, R.string.ok)
                }
            }
            VIEW_RECIPE_REQUEST -> {
                if (resultCode == RecipeViewActivity.POST_COMMENT_RESULT) {
                    val pos = data?.extras?.getInt(RECIPE_POSITION_FLAG) ?: -1
                    val extPos = data?.extras?.getInt(RECIPE_POSITION_EXT_FLAG) ?: -1
                    if (extPos >= 0) {
                        recipes[pos].items[extPos] = CurrentActiveRecipe.recipe!!
                    } else if (extPos == -2) {
                        fragmentHelper.updateLikedRecipe(pos)
                        Thread {
                            findRecipeAndUpdateIfExists(CurrentActiveRecipe.recipe!!)
                        }.start()
                    }
                }
            }
        }
    }

    private fun findRecipeAndUpdateIfExists(recipe: Recipe) {
        for (ri in recipes) {
            when {
                ri.items[0]?.recipeId?.equals(recipe.recipeId) ?: false -> {
                    ri.items[0] = recipe
                }
                ri.items[1]?.recipeId?.equals(recipe.recipeId) ?: false -> {
                    ri.items[1] = recipe
                }
                ri.items[2]?.recipeId?.equals(recipe.recipeId) ?: false -> {
                    ri.items[2] = recipe
                }
            }
        }
    }

    override fun onDestroy() {
        restUtilService.destroy()
        super.onDestroy()
    }

    companion object {
        private const val LOGIN_USER_REQUEST = 11101
        private const val PROFILE_USER_REQUEST = 11102
        private const val NEW_RECIPE_REQUEST = 11103
        const val VIEW_RECIPE_REQUEST = 11104
        const val GOTO_PROFILE = 911101
        const val GOTO_OWN_RECIPES = 911102
        const val GOTO_LIKED_RECIPES = 911103
        const val RECIPE_CREATION_FLAG = "recipe_creation_flag"
        const val RECIPE_POSITION_FLAG = "recipe_position_flag"
        const val RECIPE_POSITION_EXT_FLAG = "recipe_position_ext_flag"
    }

    inner class AuthFailInterceptor : Interceptor {
        private var loginProceeding = false
        override fun intercept(chain: Interceptor.Chain): Response {
            val request = chain.request()
            var response = chain.proceed(request)

            if (response.code() == 401) {
                if (!loginProceeding) {
                    if (user != null) {
                        SingletonInitializer.deleteCredentials()
                        loginProceeding = true
                        val loginUser = User()
                        loginUser.userId = user!!.userId
                        loginUser.password = CryptoUtilImpl().decode(CryptoUtil.PIN_ALIAS, user!!.password!!)
                        val userRequest = UserRequest(loginUser, System.currentTimeMillis())
                        val result = RestClient.loginImpl(restUtilService.restClient, userRequest)?.blockingGet()
                        if (result != null && result.code == ServerResponseCode.OK_RESPONSE) {
                            SingletonInitializer.addCredentials(result.data!!.id!!, result.data!!.token!!)
                            response = chain.proceed(request)
                        }
                        loginProceeding = false
                    }
                }
            }

            return response
        }
    }
}
