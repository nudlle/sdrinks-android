package ru.nudlik.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ProgressBar
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import ru.nudlik.R
import ru.nudlik.extentions.gone
import ru.nudlik.extentions.visible
import ru.nudlik.interfaces.LoadNextPageCallback
import ru.nudlik.interfaces.RequestNextPageListener
import ru.nudlik.response.ServerResponseCode
import ru.nudlik.service.rest.RestUtilService

abstract class PageableAdapter<T> (protected val dataList: MutableList<T?>)
    : RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    private var requestNextPageListener: RequestNextPageListener? = null
    private var loadCallback: LoadNextPageCallback? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val context = parent.context
        val inflater = LayoutInflater.from(context)
        return if (viewType == VIEW_TYPE_ITEM) {
            initViewHolder(inflater, parent)
        } else {
            LoadingViewHolder(inflater.inflate(R.layout.item_loading, parent, false))
        }
    }

    override fun getItemCount(): Int = dataList.size

    override fun getItemViewType(position: Int): Int {
        return if (dataList[position] == null) VIEW_TYPE_LOADING else VIEW_TYPE_ITEM
    }

    protected fun loadFinished() {
        loadCallback?.loadFinished()
    }

    fun showLoadingView(holder: LoadingViewHolder, position: Int) {
        fun showProgress(visible: Boolean) {
            if (visible) {
                holder.noMoreData.gone(100)
                holder.progressBar.visible(100)
            } else {
                holder.progressBar.gone(100)
                holder.noMoreData.visible(100)
            }
        }
        showProgress(true)
        holder.progressBar.postDelayed({
            requestNextPageListener?.loadNextPage(getNextPage(), getSizeOfPage(), object : RestUtilService.ResponseCallback {
                override fun <K> success(data: K?) {
                    makeSuccessOperation(data, holder)
                }

                override fun errorCode(responseCode: ServerResponseCode?) {
                    showProgress(false)
                }

                override fun exception(e: Throwable?) {
                    showProgress(false)
                }
            })
        }, 5000)
    }

    open fun doLoad(): Boolean {
        return dataList.size % getSizeOfPage() == 0
    }

    open fun getNextPage(): Int {
        val dd = dataList.size % getSizeOfPage()
        if (dd == 0) {
            return dataList.size / getSizeOfPage()
        }
        return 0
    }

    abstract fun getSizeOfPage(): Int
    abstract fun getItemClass(): Class<T>
    abstract fun initViewHolder(inflater: LayoutInflater, parent: ViewGroup): RecyclerView.ViewHolder
    abstract fun makeSuccessOperation(data: Any?, holder: LoadingViewHolder)

    fun setRequestNextPageListener(requestNextPageListener: RequestNextPageListener?) {
        this.requestNextPageListener = requestNextPageListener
    }

    fun setLoadCallback(loadCallback: LoadNextPageCallback?) {
        this.loadCallback = loadCallback
    }

    class LoadingViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val progressBar: ProgressBar = view.findViewById(R.id.item_loading_progressbar)
        val noMoreData: TextView = view.findViewById(R.id.no_more_data)
    }

    companion object {
        const val VIEW_TYPE_ITEM = 0
        const val VIEW_TYPE_LOADING = 1
    }
}