package ru.nudlik.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import ru.nudlik.listener.ItemClickListener

abstract class BaseRecyclerAdapter<T>(private val list: MutableList<T?>,
                                      private val clickListener: ItemClickListener?) : RecyclerView.Adapter<BaseRecyclerAdapter.ViewHolder<T>>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder<T> {
        val context = parent.context
        val inflater = LayoutInflater.from(context)

        val view = inflater.inflate(getLayoutRes(), parent, false)

        return initViewHolder(view)
    }

    override fun getItemCount(): Int {
        return list.size
    }

    override fun onBindViewHolder(holder: ViewHolder<T>, position: Int) {
        holder.bind(list[position])
        holder.addListener(clickListener)
    }

    fun removeAt(position: Int) {
        list.removeAt(position)
        notifyItemRemoved(position)
        for (i in position until list.size) {
            notifyItemChanged(i)
        }
    }

    fun addAt(data: T?, position: Int) {
        list.add(position, data)
        notifyItemInserted(position)
        for (i in 0 until position) {
            notifyItemChanged(position)
        }
    }

    fun updateAt(address: T?, position: Int) {
        list[position] = address
        notifyItemChanged(position)
    }

    abstract fun getLayoutRes(): Int
    abstract fun initViewHolder(view: View): ViewHolder<T>

    abstract class ViewHolder<T>(view: View) : RecyclerView.ViewHolder(view), View.OnClickListener {
        private var itemClickListener: ItemClickListener? = null
        override fun onClick(p0: View?) {
            itemClickListener?.onItemClick(p0, adapterPosition)
        }
        abstract fun bind(data: T?)

        fun addListener(listener: ItemClickListener?) {
            this.itemClickListener = listener
        }
    }
}
