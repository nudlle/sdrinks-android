package ru.nudlik.adapter.recipe

import android.content.Context
import android.view.View
import android.widget.FrameLayout
import android.widget.ImageView
import android.widget.TextView
import androidx.cardview.widget.CardView
import com.google.android.material.button.MaterialButton
import com.google.android.material.textfield.TextInputLayout
import ru.nudlik.domain.RecipeStepCook
import ru.nudlik.R
import ru.nudlik.activity.NewRecipeActivity
import ru.nudlik.domain.Ad
import ru.nudlik.extentions.*
import ru.nudlik.fragment.toast.ToastMaker
import ru.nudlik.interfaces.RequestImage
import ru.nudlik.listener.EditTextErrorCleaner
import ru.nudlik.listener.ItemClickListener
import ru.nudlik.service.rest.RestClient
import java.io.File


class RecipeStepAdapter(private val requestImage: RequestImage?,
                        ad: Ad?, editable: Boolean) : BaseRecipeAdapter<RecipeStepCook?>(ad, editable) {

    var adClickListener: ItemClickListener? = null

    override fun getResId(): Int = R.layout.rv_item_recipe_steps

    override fun initViewHolder(view: View): ViewHolder<RecipeStepCook?> = RecipeStepViewHolder(view, ad, editable)

    override fun deleteItemAt(position: Int) {
        val step = list[position]
        step?.imageUrl?.apply {
            val file = File(this)
            file.deleteOnExit()
        }
        for (i in position until list.size - 1) {
            list[i]?.number = i
        }
        super.deleteItemAt(position)
    }

    override fun onBindViewHolder(holder: ViewHolder<RecipeStepCook?>, position: Int) {
        super.onBindViewHolder(holder, position)
        (holder as RecipeStepViewHolder).addRequestImage(requestImage)
        holder.addItemClickListener(adClickListener)
    }

    class RecipeStepViewHolder(view: View, private var ad: Ad?, private val editable: Boolean) : ViewHolder<RecipeStepCook?>(view) {
        private val context: Context = view.context
        private val addCardContainer: CardView = view.findViewById(R.id.add_recipe_step)
        private val addRecipeStepImage: ImageView = view.findViewById(R.id.add_recipe_image)
        private val addRecipeStepImagePlaceholder: ImageView = view.findViewById(R.id.add_recipe_image_placeholder)
        private val addRecipeStepDescription: TextInputLayout = view.findViewById(R.id.add_recipe_step_description_input_layout)
        private val addRecipeStep: MaterialButton = view.findViewById(R.id.add_recipe_step_button)

        private val addedCardContainer: CardView = view.findViewById(R.id.added_recipe_step)
        private val addedRecipeStepTitle: TextView = view.findViewById(R.id.added_recipe_step_title)
        private val addedRecipeStepImage: ImageView = view.findViewById(R.id.added_recipe_step_image)
        private val addedRecipeStepDescription: TextView = view.findViewById(R.id.added_recipe_step_description)
        private val deleteRecipeStep: MaterialButton = view.findViewById(R.id.delete_recipe_step)

        private val cardAdContainer: CardView = view.findViewById(R.id.recipe_ad_step)
        private val recipeAdStepImage: ImageView = view.findViewById(R.id.recipe_ad_step_image)
        private val recipeAdStepDescription: TextView = view.findViewById(R.id.recipe_ad_step_description)

        private var file: File? = null

        private var requestImage: RequestImage? = null

        private var openAdListener: ItemClickListener? = null

        fun addItemClickListener(listener: ItemClickListener?) {
            this.openAdListener = listener
        }

        init {
            addRecipeStepDescription.editText?.addTextChangedListener(EditTextErrorCleaner(addRecipeStepDescription))
        }

        fun addRequestImage(requestImage: RequestImage?) {
            this.requestImage = requestImage
        }

        fun addResource(file: File) {
            if (this.file == null) {
                this.file = file
                addRecipeStepImagePlaceholder.gone()
                addRecipeStepImage.loadPath(this.file!!)
            } else {
                this.file = file
                addRecipeStepImage.reloadPath(this.file!!)
            }
        }

        fun clearCash() {
            file?.delete()
        }

        private fun validate(): Boolean {
            return when {
                file == null -> {
                    ToastMaker.showShortWarningToastGravityCenter(context, R.string.choose_image_warning)
                    false
                }
                addRecipeStepDescription.editText?.text!!.isEmpty() -> {
                    addRecipeStepDescription.error = context.getString(R.string.not_be_empty)
                    false
                }
                addRecipeStepDescription.editText?.text!!.length < 10 -> {
                    addRecipeStepDescription.error = "Минимальное количество символов: 10"
                    false
                }
                addRecipeStepDescription.editText?.text!!.length > 512 -> {
                    addRecipeStepDescription.error = context.getString(R.string.max_length_error)
                    false
                }
                else -> {
                    true
                }
            }
        }

        override fun bind(data: RecipeStepCook?) {
            if (ad != null && ad?.stepPosition == adapterPosition) {
                addedCardContainer.gone()
                addCardContainer.gone()
                recipeAdStepImage.loadUri("${RestClient.URL}/resources/ad/${ad?.recipeId}/${ad?.adId}/${ad?.imageUrl}")
                if (ad?.description == null || ad?.description!!.isEmpty()) {
                    recipeAdStepDescription.gone()
                    val layoutParams = FrameLayout.LayoutParams(FrameLayout.LayoutParams.MATCH_PARENT,
                        FrameLayout.LayoutParams.MATCH_PARENT)
                    recipeAdStepImage.layoutParams = layoutParams
                } else {
                    recipeAdStepDescription.text = ad?.description
                }
                cardAdContainer.setOnClickListener {
                    openAdListener?.onItemClick(it, adapterPosition, ad?.externalLink)
                }
            } else {
                cardAdContainer.gone()
                if (data == null) {
                    addedCardContainer.gone()
                    addRecipeStepImage.setOnClickListener {
                        requestImage?.requestImage(NewRecipeActivity.REQUEST_STEP_IMAGE_MODE,
                            "${adapterPosition + 1}", this)
                    }
                    addRecipeStep.setOnClickListener {
                        if (validate()) {
                            val addS = RecipeStepCook()
                            addS.number = adapterPosition + 1
                            addS.description = addRecipeStepDescription.editText?.text.toString()
                            addS.imageUrl = file?.absolutePath
                            listener?.add(addS)
                            addRecipeStepImagePlaceholder.visible()
                            addRecipeStepImage.setImageResource(android.R.color.transparent)
                            addRecipeStepDescription.editText?.text?.clear()
                            file = null
                        }
                    }
                } else {
                    addCardContainer.gone()
                    addedRecipeStepTitle.text = getStepNumber(data.number)
                    addedRecipeStepDescription.text = data.description
                    if (editable) {
                        data.imageUrl?.apply {
                            addedRecipeStepImage.loadPath(File(this))
                        }
                    } else {
                        addedRecipeStepImage.loadUri("${RestClient.URL}/resources/recipes/${data.recipeId}/${data.recipeStepCookId}/${data.imageUrl}")
                        deleteRecipeStep.gone()
                    }
                    deleteRecipeStep.setOnClickListener {
                        listener?.delete(adapterPosition)
                    }
                }
            }
        }

        private fun getStepNumber(number: Int): String = "Шаг $number"
    }
}