package ru.nudlik.adapter.recipe

import android.view.View
import android.widget.LinearLayout
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import com.google.android.material.button.MaterialButton
import com.google.android.material.textfield.TextInputLayout
import ru.nudlik.R
import ru.nudlik.domain.Ad
import ru.nudlik.extentions.gone
import ru.nudlik.listener.EditTextErrorCleaner

class RecipeIngredientsAdapter(ad: Ad?, editable: Boolean)
    : BaseRecipeAdapter<String?>(ad, editable) {
    override fun getResId(): Int = R.layout.rv_item_recipe_ingredients
    override fun initViewHolder(view: View): ViewHolder<String?> = RecipeIngredientsViewHolder(view, editable)

    class RecipeIngredientsViewHolder(view: View, private val editable: Boolean) : ViewHolder<String?>(view) {
        private val addIngredHolder: LinearLayout = view.findViewById(R.id.recipe_ingredients_add_container)
        private val addedIngredHolder: LinearLayout = view.findViewById(R.id.recipe_ingredients_container)
        private val addIngredInput: TextInputLayout = view.findViewById(R.id.recipe_add_ingredients_input_layout)
        private val addButton: MaterialButton = view.findViewById(R.id.add_ingredients)
        private val addedIngred: TextView = view.findViewById(R.id.recipe_added_ingredients)
        private val deleteIngred: MaterialButton = view.findViewById(R.id.delete_ingredients)
        private val context = view.context

        override fun bind(data: String?) {
            if (data == null) {
                addedIngredHolder.gone()

                addIngredInput.editText?.addTextChangedListener(EditTextErrorCleaner(addIngredInput))
                addButton.setOnClickListener {
                    if (validate()) {
                        listener?.add(addIngredInput.editText?.text!!.toString())
                        addIngredInput.editText?.text?.clear()
                    }
                }
            } else {
                addIngredHolder.gone()
                addedIngred.text = data//getItemText(data)
                deleteIngred.setOnClickListener {
                    listener?.delete(adapterPosition)
                }
                if (!editable) {
                    deleteIngred.gone()
                    val params = addedIngredHolder.layoutParams as ConstraintLayout.LayoutParams
                    params.setMargins(32, 16, 16, 0)
                }
            }
        }

        private fun validate(): Boolean {
            return when {
                addIngredInput.editText?.text!!.isEmpty() -> {
                    addIngredInput.error = context.getString(R.string.not_be_empty)
                    false
                }
                addIngredInput.editText?.text!!.length > 35 -> {
                    addIngredInput.error = context.getString(R.string.max_length_error)
                    false
                }
                else -> true
            }
        }

//        private fun getItemText(item: String): String = "• $item"
    }
}