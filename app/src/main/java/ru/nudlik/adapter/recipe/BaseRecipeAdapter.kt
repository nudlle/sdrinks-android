package ru.nudlik.adapter.recipe

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import ru.nudlik.domain.Ad

abstract class BaseRecipeAdapter<T>(protected val ad: Ad?, protected val editable: Boolean) : RecyclerView.Adapter<BaseRecipeAdapter.ViewHolder<T>>() {
    val list = ArrayList<T?>()

    init {
        if (editable) {
            list.add(null)
        }
    }

    open fun addItem(data: T) {
        val addPos = list.size - 1
        list.add(addPos, data)
        notifyItemInserted(addPos)
    }

    open fun deleteItemAt(position: Int) {
        list.removeAt(position)
        notifyItemRemoved(position)
        for (i in position until list.size - 1) {
            notifyItemChanged(i)
        }
    }

    fun getList(): MutableList<T?> {
        return list
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder<T> {
        val context = parent.context
        val inflater = LayoutInflater.from(context)

        val view = inflater.inflate(getResId(), parent, false)

        return initViewHolder(view)
    }

    override fun getItemCount(): Int = list.size

    override fun onBindViewHolder(holder: ViewHolder<T>, position: Int) {
        holder.bind(list[position])
        holder.addListener(object : ItemAddDeleteListener<T> {
            override fun add(data: T) {
                addItem(data)
            }

            override fun delete(position: Int) {
                deleteItemAt(position)
            }
        })
    }

    abstract fun getResId(): Int
    abstract fun initViewHolder(view: View): ViewHolder<T>

    abstract class ViewHolder<T>(view: View) : RecyclerView.ViewHolder(view) {
        var listener: ItemAddDeleteListener<T>? = null

        abstract fun bind(data: T?)

        fun addListener(listener: ItemAddDeleteListener<T>?) {
            this.listener = listener
        }
    }

    interface ItemAddDeleteListener<T> {
        fun add(data: T)
        fun delete(position: Int)
    }
}