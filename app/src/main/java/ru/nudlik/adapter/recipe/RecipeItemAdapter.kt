package ru.nudlik.adapter.recipe

import android.util.TypedValue
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import androidx.cardview.widget.CardView
import androidx.recyclerview.widget.RecyclerView
import ru.nudlik.R
import ru.nudlik.adapter.PageableAdapter
import ru.nudlik.adapter.recipe.items.RecipeItem
import ru.nudlik.domain.Recipe
import ru.nudlik.extentions.gone
import ru.nudlik.extentions.invisible
import ru.nudlik.extentions.loadUri
import ru.nudlik.extentions.visible
import ru.nudlik.listener.ItemClickListener
import ru.nudlik.service.rest.RestClient
import ru.nudlik.utils.RecipeUtil

class RecipeItemAdapter(list: MutableList<RecipeItem?>,
                        private var itemClickListener: ItemClickListener?)
    : PageableAdapter<RecipeItem>(list) {

    override fun doLoad(): Boolean {
        return dataList.size % (getSizeOfPage() / 3) == 0
    }

    override fun getNextPage(): Int {
        val dd = (dataList.size - 1) * 3 % getSizeOfPage()
        if (dd == 0) {
            return (dataList.size - 1) * 3 / getSizeOfPage()
        }
        return 0
    }

    override fun getSizeOfPage(): Int = 60

    override fun getItemClass(): Class<RecipeItem> = RecipeItem::class.java

    override fun initViewHolder(inflater: LayoutInflater, parent: ViewGroup): RecyclerView.ViewHolder =
        RecipeItemViewHolder(inflater.inflate(R.layout.rv_item_recipe_content, parent, false))

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        if (holder is RecipeItemViewHolder) {
            populateItemRows(holder, position)
        } else {
            showLoadingView(holder as LoadingViewHolder, position)
        }
    }

    override fun makeSuccessOperation(data: Any?, holder: LoadingViewHolder) {
        if (data is List<*>) {
            val li = data.filterIsInstance(Recipe::class.java)
            if (li.isNotEmpty()) {
                val pos = dataList.size - 1
                dataList.removeAt(pos)
                notifyItemRemoved(pos)
                val converted = when {
                    dataList.size >= 2 -> RecipeUtil.convert(li, dataList[dataList.size - 1], dataList[dataList.size - 2])
                    dataList.size >= 1 -> RecipeUtil.convert(li, dataList[dataList.size - 1], null)
                    else -> RecipeUtil.convert(li, null, null)
                }

                converted.forEach {
                    dataList.add(it)
                    notifyItemInserted(dataList.size - 1)
                }
            } else {
                holder.progressBar.gone(100)
                holder.noMoreData.visible(100)
            }
        }
        loadFinished()
    }

    private fun populateItemRows(holder: RecipeItemViewHolder, position: Int) {
        holder.bind(dataList[position])
        holder.addListener(itemClickListener)
    }

    class RecipeItemViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        private val simpleItemContainer: LinearLayout = view.findViewById(R.id.recipe_item_simple_row)
        private val simpleFirstImageHolder: CardView = view.findViewById(R.id.simple_first_item_holder)
        private val simpleFirstImage: ImageView = view.findViewById(R.id.simple_item_first)
        private val simpleSecondImageHolder: CardView = view.findViewById(R.id.simple_second_item_holder)
        private val simpleSecondImage: ImageView = view.findViewById(R.id.simple_item_second)
        private val simpleThirdImageHolder: CardView = view.findViewById(R.id.simple_third_item_holder)
        private val simpleThirdImage: ImageView = view.findViewById(R.id.simple_item_third)

        private val bigLeftItemContainer: LinearLayout = view.findViewById(R.id.recipe_item_big_left_row)
        private val bigLeftFirstImageHolder: CardView = view.findViewById(R.id.big_left_first_item_holder)
        private val bigLeftFirstImage: ImageView = view.findViewById(R.id.big_left_item_first)
        private val bigLeftSecondImageHolder: CardView = view.findViewById(R.id.big_left_second_item_holder)
        private val bigLeftSecondImage: ImageView = view.findViewById(R.id.big_left_item_second)
        private val bigLeftThirdImageHolder: CardView = view.findViewById(R.id.big_left_third_item_holder)
        private val bigLeftThirdImage: ImageView = view.findViewById(R.id.big_left_item_third)

        private val bigRightItemContainer: LinearLayout = view.findViewById(R.id.recipe_item_big_right_row)
        private val bigRightFirstImageHolder: CardView = view.findViewById(R.id.big_right_first_item_holder)
        private val bigRightFirstImage: ImageView = view.findViewById(R.id.big_right_item_first)
        private val bigRightSecondImageHolder: CardView = view.findViewById(R.id.big_right_second_item_holder)
        private val bigRightSecondImage: ImageView = view.findViewById(R.id.big_right_item_second)
        private val bigRightThirdImageHolder: CardView = view.findViewById(R.id.big_right_third_item_holder)
        private val bigRightThirdImage: ImageView = view.findViewById(R.id.big_right_item_third)

        private var itemClickListener: ItemClickListener? = null

        fun addListener(l: ItemClickListener?) {
            this.itemClickListener = l
        }

        fun bind(recipeItem: RecipeItem?) {
            when (recipeItem!!.typeRow) {
                RecipeItem.SIMPLE_ROW -> {
                    simpleItemContainer.visible()
                    bigLeftItemContainer.gone()
                    bigRightItemContainer.gone()

                    when (recipeItem.items.size) {
                        1 -> {
                            loadImages(recipeItem.items[0], recipeItem.items[0]?.imageUrls?.medium,
                                simpleFirstImage, simpleFirstImageHolder, 0)
                        }
                        2 -> {
                            loadImages(recipeItem.items[0], recipeItem.items[0]?.imageUrls?.medium,
                                simpleFirstImage, simpleFirstImageHolder, 0)
                            loadImages(recipeItem.items[1], recipeItem.items[1]?.imageUrls?.medium,
                                simpleSecondImage, simpleSecondImageHolder, 1)
                        }
                        3 -> {
                            loadImages(recipeItem.items[0], recipeItem.items[0]?.imageUrls?.medium,
                                simpleFirstImage, simpleFirstImageHolder, 0)
                            loadImages(recipeItem.items[1], recipeItem.items[1]?.imageUrls?.medium,
                                simpleSecondImage, simpleSecondImageHolder, 1)
                            loadImages(recipeItem.items[2], recipeItem.items[2]?.imageUrls?.medium,
                                simpleThirdImage, simpleThirdImageHolder, 2)
                        }
                    }

                }
                RecipeItem.LEFT_ROW -> {
                    simpleItemContainer.gone()
                    bigLeftItemContainer.visible()
                    bigRightItemContainer.gone()

                    when (recipeItem.items.size) {
                        1 -> {
                            loadImages(recipeItem.items[0], recipeItem.items[0]?.imageUrls?.large,
                                bigLeftFirstImage, bigLeftFirstImageHolder, 0)
                        }
                        2 -> {
                            loadImages(recipeItem.items[0], recipeItem.items[0]?.imageUrls?.large,
                                bigLeftFirstImage, bigLeftFirstImageHolder, 0)
                            loadImages(recipeItem.items[1], recipeItem.items[1]?.imageUrls?.medium,
                                bigLeftSecondImage, bigLeftSecondImageHolder, 1)
                        }
                        3 -> {
                            loadImages(recipeItem.items[0], recipeItem.items[0]?.imageUrls?.large,
                                bigLeftFirstImage, bigLeftFirstImageHolder, 0)
                            loadImages(recipeItem.items[1], recipeItem.items[1]?.imageUrls?.medium,
                                bigLeftSecondImage, bigLeftSecondImageHolder, 1)
                            loadImages(recipeItem.items[2], recipeItem.items[2]?.imageUrls?.medium,
                                bigLeftThirdImage, bigLeftThirdImageHolder, 2)
                        }
                    }

                }
                RecipeItem.RIGHT_ROW -> {
                    simpleItemContainer.gone()
                    bigLeftItemContainer.gone()
                    bigRightItemContainer.visible()

                    when (recipeItem.items.size) {
                        1 -> {
                            loadImages(recipeItem.items[0], recipeItem.items[0]?.imageUrls?.medium,
                                bigRightFirstImage, bigRightFirstImageHolder, 0)
                        }
                        2 -> {
                            loadImages(recipeItem.items[0], recipeItem.items[0]?.imageUrls?.medium,
                                bigRightFirstImage, bigRightFirstImageHolder, 0)
                            loadImages(recipeItem.items[1], recipeItem.items[1]?.imageUrls?.medium,
                                bigRightSecondImage, bigRightSecondImageHolder, 1)
                        }
                        3 -> {
                            loadImages(recipeItem.items[0], recipeItem.items[0]?.imageUrls?.medium,
                                bigRightFirstImage, bigRightFirstImageHolder, 0)
                            loadImages(recipeItem.items[1], recipeItem.items[1]?.imageUrls?.medium,
                                bigRightSecondImage, bigRightSecondImageHolder, 1)
                            loadImages(recipeItem.items[2], recipeItem.items[2]?.imageUrls?.large,
                                bigRightThirdImage, bigRightThirdImageHolder, 2)
                        }
                    }

                }
            }
            if (adapterPosition == 0) {
                val dimsInDp = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP,
                    136f, itemView.resources.displayMetrics)
                val params = LinearLayout.LayoutParams(dimsInDp.toInt(), dimsInDp.toInt())
                val margin = itemView.resources.getDimension(R.dimen.recipe_item_main_view_margin).toInt()
                params.topMargin = margin
                params.bottomMargin = margin
                simpleFirstImageHolder.layoutParams = params
                simpleSecondImageHolder.layoutParams = params
                simpleThirdImageHolder.layoutParams = params
            }
        }


        private fun loadImages(recipe: Recipe?,
                               url: String?,
                               imageView: ImageView,
                               cardView: CardView,
                               correction: Int) {
            if (recipe != null) {
                imageView.loadUri("${RestClient.URL}/resources/recipes/${recipe.recipeId}/$url")
                cardView.setOnClickListener {
                    itemClickListener?.onItemClick(it, adapterPosition, correction)
                }
            } else {
                cardView.invisible()
            }
        }
    }
}