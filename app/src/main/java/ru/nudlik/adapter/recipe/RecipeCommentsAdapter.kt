package ru.nudlik.adapter.recipe

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.RatingBar
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import ru.nudlik.adapter.PageableAdapter
import ru.nudlik.domain.UserComment
import ru.nudlik.R
import ru.nudlik.extentions.gone
import ru.nudlik.extentions.toTextDate
import ru.nudlik.extentions.visible

class RecipeCommentsAdapter(list: MutableList<UserComment?>) : PageableAdapter<UserComment>(list) {
    fun addItem(item: UserComment) {
        dataList.add(0, item)
        notifyItemInserted(0)
    }

    override fun getSizeOfPage(): Int = 60

    override fun getItemClass(): Class<UserComment> = UserComment::class.java

    override fun initViewHolder(inflater: LayoutInflater, parent: ViewGroup): RecyclerView.ViewHolder =
        RecipeCommentViewHolder(inflater.inflate(R.layout.rv_item_recipe_comment, parent, false))

    override fun makeSuccessOperation(data: Any?, holder: LoadingViewHolder) {
        if (data is List<*>) {
            val li = data.filterIsInstance(UserComment::class.java)
            if (li.isNotEmpty()) {
                val pos = dataList.size - 1
                dataList.removeAt(pos)
                notifyItemRemoved(pos)
                li.forEach {
                    dataList.add(it)
                    notifyItemInserted(dataList.size - 1)
                }
            } else {
                holder.progressBar.gone(100)
                holder.noMoreData.visible(100)
            }
        }
        loadFinished()
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        if (holder is RecipeCommentViewHolder) {
            populateItemRows(holder, position)
        } else {
            showLoadingView(holder as LoadingViewHolder, position)
        }
    }

    private fun populateItemRows(holder: RecipeCommentViewHolder, position: Int) {
        holder.bind(dataList[position])
    }

    class RecipeCommentViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        private val author: TextView = view.findViewById(R.id.recipe_comment_author)
        private val ratingBar: RatingBar = view.findViewById(R.id.recipe_rate_by_user)
        private val recipeComment: TextView = view.findViewById(R.id.recipe_comment)
        private val postDate: TextView = view.findViewById(R.id.recipe_comment_date)

        fun bind(c: UserComment?) {
            c?.apply {
                author.text = "${userEmail?.substring(0, userEmail?.indexOf('@')?.plus(1) ?: 0)}"
                ratingBar.rating = rate?.toFloat() ?: 5f
                recipeComment.text = comment
                postDate.text = date?.toTextDate("dd-MM-yyyy HH:mm") ?: ""
            }
        }
    }
}