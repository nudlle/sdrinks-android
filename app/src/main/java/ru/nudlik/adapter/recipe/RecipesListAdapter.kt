package ru.nudlik.adapter.recipe

import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.cardview.widget.CardView
import androidx.recyclerview.widget.RecyclerView
import ru.nudlik.domain.Recipe
import ru.nudlik.listener.ItemClickListener
import ru.nudlik.R
import ru.nudlik.extentions.gone
import ru.nudlik.extentions.inflate
import ru.nudlik.extentions.loadUri
import ru.nudlik.extentions.visible
import ru.nudlik.fragment.nav.BaseRecyclerViewFragment
import ru.nudlik.service.rest.RestClient

class RecipesListAdapter(private val dataList: MutableList<Recipe>,
                         private var filteredList: MutableList<Recipe>,
                         private var itemClickListener: ItemClickListener?,
                         private val ownRecipes: Boolean = false) :
    RecyclerView.Adapter<RecipesListAdapter.LikeRecipesViewHolder>(), Filterable {
    var filterCallback: BaseRecyclerViewFragment.FilterChangesListener? = null

    override fun getFilter(): Filter = object : Filter() {
        override fun performFiltering(charSequence: CharSequence?): FilterResults {
            if (charSequence == null || charSequence.isEmpty()) {
                filteredList = dataList
            } else {
                val filtered = ArrayList<Recipe>()
                for (r in dataList) {
                    if (r.name?.contains(charSequence, true) == true ||
                        r.secondaryName?.contains(charSequence, true) == true ||
                        r.userEmail?.contains(charSequence, true) == true) {
                        filtered.add(r)
                    }
                }
                filteredList = filtered
            }
            val result = FilterResults()
            result.values = filteredList
            return result
        }

        @Suppress("UNCHECKED_CAST")
        override fun publishResults(p0: CharSequence?, result: FilterResults?) {
            result?.apply {
                filteredList = values as MutableList<Recipe>
                notifyDataSetChanged()
                filterCallback?.onPublishFilteredResult()
            }
        }
    }
    
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): LikeRecipesViewHolder =
        LikeRecipesViewHolder(parent.inflate(R.layout.rv_item_recipes_list), ownRecipes)

    override fun getItemCount(): Int = filteredList.size

    override fun onBindViewHolder(holder: LikeRecipesViewHolder, position: Int) {
        populateItemRows(holder, position)
    }

    private fun populateItemRows(holder: LikeRecipesViewHolder, position: Int) {
        holder.bind(filteredList[position])
        holder.addListener(itemClickListener)
    }

    fun removeAt(position: Int): String {
        val recipe = filteredList.removeAt(position)
        val index = dataList.indexOf(recipe)
        if (index != -1) {
            dataList.removeAt(index)
        }
        notifyItemRemoved(position)
        return recipe.recipeId!!
    }

    class LikeRecipesViewHolder(view: View, private val own: Boolean) : RecyclerView.ViewHolder(view) {
        private val parentContainer: CardView = view.findViewById(R.id.recipe_list_item_parent)
        private val recipeImage: ImageView = view.findViewById(R.id.recipe_list_item_image)
        private val recipeName: TextView = view.findViewById(R.id.recipe_list_item_name)
        private val recipeSecondName: TextView = view.findViewById(R.id.recipe_list_item_second_name)
        private val recipeRate: RatingBar = view.findViewById(R.id.recipe_list_item_rates)
        private val recipeRateCount: TextView = view.findViewById(R.id.recipe_list_item_rating_count)
        private val recipeAuthor: TextView = view.findViewById(R.id.recipe_list_item_author)
        private val recipeStatHolder: LinearLayout = view.findViewById(R.id.recipe_stat_holder)
        private val recipeViewCount: TextView = view.findViewById(R.id.recipe_view_count)
        private val recipeAdClickCount: TextView = view.findViewById(R.id.recipe_ad_click_count)
        private val recipeLikedCount: TextView = view.findViewById(R.id.recipe_liked_count)

        private var itemClickListener: ItemClickListener? = null

        fun bind(recipe: Recipe?) {
            recipe?.let { r ->
                loadImage(r, recipeImage, parentContainer)
                recipeName.text = r.name
                recipeSecondName.text = r.secondaryName
                recipeRate.rating = (r.rate / r.commentCount).toFloat()
                val commentCount = if (r.commentCount == 0L) "(нет отзывов)" else "(${r.commentCount})"
                recipeRateCount.text = commentCount
                val author = r.userEmail?.substring(0, (r.userEmail?.indexOf('@')?.plus(1)) ?: 0)
                if (own) {
                    recipeStatHolder.visible()
                    recipeAuthor.gone()
                    recipeViewCount.text = "${r.viewed}"
                    recipeAdClickCount.text = "${r.ad?.clicked ?: 0}"
                    recipeLikedCount.text = "${r.liked}"
                } else {
                    recipeStatHolder.gone()
                    recipeAuthor.text = author
                }

            }
        }

        fun addListener(l: ItemClickListener?) {
            this.itemClickListener = l
        }

        private fun loadImage(recipe: Recipe,
                               imageView: ImageView,
                               parent: CardView) {
            imageView.loadUri("${RestClient.URL}/resources/recipes/${recipe.recipeId}/${recipe.imageUrls.small}")
            parent.setOnClickListener {
                itemClickListener?.onItemClick(it, adapterPosition, recipe)
            }
        }
    }
}
