package ru.nudlik.adapter.recipe.items

import ru.nudlik.domain.Recipe

class RecipeItem {
    val items: MutableList<Recipe?>
    val typeRow: Int

    constructor(typeRow: Int) {
        this.typeRow = typeRow
        this.items = ArrayList(3)
    }

    companion object TYPEROW {
        const val SIMPLE_ROW = 1
        const val RIGHT_ROW = 2
        const val LEFT_ROW = 3
    }

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as RecipeItem

        if (items != other.items) return false
        if (typeRow != other.typeRow) return false

        return true
    }

    override fun hashCode(): Int {
        var result = items.hashCode()
        result = 31 * result + typeRow
        return result
    }

    override fun toString(): String {
        return "RecipeItem(items=$items, typeRow=$typeRow)"
    }
}