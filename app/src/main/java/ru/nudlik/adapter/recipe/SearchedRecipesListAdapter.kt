package ru.nudlik.adapter.recipe

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.RatingBar
import android.widget.TextView
import androidx.cardview.widget.CardView
import androidx.recyclerview.widget.RecyclerView
import ru.nudlik.R
import ru.nudlik.adapter.PageableAdapter
import ru.nudlik.domain.Recipe
import ru.nudlik.extentions.gone
import ru.nudlik.extentions.loadUri
import ru.nudlik.extentions.visible
import ru.nudlik.listener.ItemClickListener
import ru.nudlik.service.rest.RestClient

class SearchedRecipesListAdapter(list: MutableList<Recipe?>, private var itemClickListener: ItemClickListener?) :
    PageableAdapter<Recipe>(list){

    fun clearAdapter() {
        dataList.clear()
        notifyDataSetChanged()
    }

    fun addItems(list: List<Recipe>) {
        dataList.addAll(list)
        notifyDataSetChanged()
    }

    override fun getSizeOfPage(): Int = 60

    override fun getItemClass(): Class<Recipe> = Recipe::class.java

    override fun initViewHolder(inflater: LayoutInflater, parent: ViewGroup): RecyclerView.ViewHolder =
        SearchedRecipesViewHolder(inflater.inflate(R.layout.rv_item_recipes_list, parent, false))

    override fun makeSuccessOperation(data: Any?, holder: LoadingViewHolder) {
        if (data is List<*>) {
            val li = data.filterIsInstance(Recipe::class.java)
            if (li.isNotEmpty()) {
                val pos = dataList.size - 1
                dataList.removeAt(pos)
                notifyItemRemoved(pos)
                li.forEach {
                    dataList.add(it)
                    notifyItemInserted(dataList.size - 1)
                }
            } else {
                holder.progressBar.gone(100)
                holder.noMoreData.visible(100)
            }
        }
        loadFinished()
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        if (holder is SearchedRecipesViewHolder) {
            populateItemRows(holder, position)
        } else {
            showLoadingView(holder as LoadingViewHolder, position)
        }
    }

    private fun populateItemRows(holder: SearchedRecipesViewHolder, position: Int) {
        holder.bind(dataList[position])
        holder.addListener(itemClickListener)
    }

    class SearchedRecipesViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        private val parentContainer: CardView = view.findViewById(R.id.recipe_list_item_parent)
        private val recipeImage: ImageView = view.findViewById(R.id.recipe_list_item_image)
        private val recipeName: TextView = view.findViewById(R.id.recipe_list_item_name)
        private val recipeSecondName: TextView = view.findViewById(R.id.recipe_list_item_second_name)
        private val recipeRate: RatingBar = view.findViewById(R.id.recipe_list_item_rates)
        private val recipeRateCount: TextView = view.findViewById(R.id.recipe_list_item_rating_count)
        private val recipeAuthor: TextView = view.findViewById(R.id.recipe_list_item_author)

        private var itemClickListener: ItemClickListener? = null

        fun bind(recipe: Recipe?) {
            recipe?.let { r ->
                loadImage(r, recipeImage, parentContainer)
                recipeName.text = r.name
                recipeSecondName.text = r.secondaryName
                recipeRate.rating = (r.rate / r.commentCount).toFloat()
                val commentCount = if (r.commentCount == 0L) "(нет отзывов)" else "(${r.commentCount})"
                recipeRateCount.text = commentCount
                val author = r.userEmail?.substring(0, (r.userEmail?.indexOf('@')?.plus(1)) ?: 0)
                recipeAuthor.text = author
            }
        }

        fun addListener(l: ItemClickListener?) {
            this.itemClickListener = l
        }

        private fun loadImage(recipe: Recipe,
                              imageView: ImageView,
                              parent: CardView
        ) {
            imageView.loadUri("${RestClient.URL}/resources/recipes/${recipe.recipeId}/${recipe.imageUrls!!.small}")
            parent.setOnClickListener {
                itemClickListener?.onItemClick(it, adapterPosition, recipe)
            }
        }
    }
}