package ru.nudlik.provider.user.subscription

import android.content.*
import android.database.Cursor
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteOpenHelper
import android.database.sqlite.SQLiteQueryBuilder
import android.net.Uri
import android.provider.BaseColumns
import android.text.TextUtils

class SubscriptionProvider : ContentProvider() {
    private var dbHelper: OpenDatabaseHelper? = null
    private val uriMatcher: UriMatcher = UriMatcher(UriMatcher.NO_MATCH)

    init {
        uriMatcher.addURI(
            SubscriptionData.AUTHORITY,
            SubscriptionData.DColumns.SUB_DATA_NAME,
            SUB_DATA
        )
        uriMatcher.addURI(
            SubscriptionData.AUTHORITY,
            SubscriptionData.DColumns.SUB_DATA_NAME + "/#",
            SUB_DATA_ID
        )
    }

    companion object {
        const val SUB_DATA_NAME = "sub_data_name"
        const val SUB_DATA_TABLE_NAME = "sub_data_table_name"
        const val SUB_DATA = 1
        const val SUB_DATA_ID = 2
    }


    override fun insert(uri: Uri, values: ContentValues?): Uri? {
        val uriType = uriMatcher.match(uri)
        val id: Long
        val db = dbHelper!!.writableDatabase
        when(uriType) {
            SUB_DATA -> id = db!!.insert(SUB_DATA_TABLE_NAME, null, values)
            else -> throw IllegalArgumentException("Unknown URI: $uri")
        }
        context?.contentResolver?.notifyChange(uri, null)
        return ContentUris.withAppendedId(uri, id)
    }

    override fun query(
        uri: Uri,
        projection: Array<String>?,
        selection: String?,
        selectionArgs: Array<String>?,
        sortOrder: String?
    ): Cursor? {
        val queryBuilder = SQLiteQueryBuilder()
        queryBuilder.tables = SUB_DATA_TABLE_NAME

        when (uriMatcher.match(uri)) {
            SUB_DATA -> {}
            SUB_DATA_ID -> queryBuilder.appendWhere(BaseColumns._ID + "=" + uri.lastPathSegment)
            else -> throw IllegalArgumentException("Unknown URI")
        }

        val cursor = queryBuilder.query(dbHelper!!.readableDatabase, projection, selection,
            selectionArgs, null, null, sortOrder)
        cursor.setNotificationUri(context?.contentResolver, uri)
        return cursor
    }

    override fun onCreate(): Boolean {
        dbHelper = OpenDatabaseHelper(context)
        return true
    }

    override fun update(uri: Uri, values: ContentValues?, selection: String?, selectionArgs: Array<String>?): Int {
        val uriType = uriMatcher.match(uri)
        val db = dbHelper!!.writableDatabase
        val rowUpdated = when (uriType) {
            SUB_DATA -> db.update(SUB_DATA_TABLE_NAME, values, selection, selectionArgs)
            SUB_DATA_ID -> {
                val id = uri.lastPathSegment
                if (TextUtils.isEmpty(selection)) {
                    db.update(SUB_DATA_TABLE_NAME, values, BaseColumns._ID + "=" + id, null)
                } else {
                    db.update(SUB_DATA_TABLE_NAME, values, BaseColumns._ID + "=" + id + " and " + selection, selectionArgs)
                }
            }
            else -> throw IllegalArgumentException("Unknown URI: $uri")
        }
        context!!.contentResolver.notifyChange(uri, null)
        return rowUpdated
    }

    override fun delete(uri: Uri, selection: String?, selectionArgs: Array<String>?): Int {
        val uriType = uriMatcher.match(uri)
        val db = dbHelper!!.writableDatabase
        val rowDeleted = when (uriType) {
            SUB_DATA -> db.delete(SUB_DATA_TABLE_NAME, selection, selectionArgs)
            SUB_DATA_ID -> {
                val id = uri.lastPathSegment
                if (TextUtils.isEmpty(selection)) {
                    db.delete(SUB_DATA_TABLE_NAME, BaseColumns._ID + "=" + id, null)
                } else {
                    db.delete(SUB_DATA_TABLE_NAME, BaseColumns._ID + "=" + id + " and " + selection, selectionArgs)
                }
            }
            else -> throw IllegalArgumentException("Unknown URI: $uri")
        }
        context!!.contentResolver.notifyChange(uri, null)
        return rowDeleted
    }

    override fun getType(uri: Uri): String? {
        return when (uriMatcher.match(uri)) {
            SUB_DATA -> SubscriptionData.DColumns.CONTENT_TYPE
            SUB_DATA_ID -> SubscriptionData.DColumns.CONTENT_ITEM_TYPE
            else -> throw IllegalArgumentException("Unknown URI: $uri")
        }
    }

    class OpenDatabaseHelper(context: Context?) : SQLiteOpenHelper(context,
        DATABASE_NAME, null,
        DATABASE_VERSION
    ) {
        override fun onCreate(db: SQLiteDatabase?) {
            createTable(db)
        }

        override fun onUpgrade(db: SQLiteDatabase?, oldVersion: Int, newVersion: Int) {
            db!!.execSQL("DROP TABLE IF EXISTS $SUB_DATA_TABLE_NAME;")
            createTable(db)
        }

        companion object {
            const val DATABASE_NAME = "$SUB_DATA_NAME.db"
            const val DATABASE_VERSION = 1
        }

        private fun createTable(db: SQLiteDatabase?) {
            val qs = "CREATE TABLE $SUB_DATA_TABLE_NAME (" +
                    "${BaseColumns._ID} INTEGER PRIMARY KEY," +
                    "${SubscriptionData.DColumns.SUB_ID} TEXT," +
                    "${SubscriptionData.DColumns.SUB_PRICE} TEXT," +
                    "${SubscriptionData.DColumns.SUB_END_DATE} TEXT," +
                    "${SubscriptionData.DColumns.SUB_USER_ID} TEXT," +
                    "${SubscriptionData.DColumns.SUB_ACTIVATED} INTEGER" +
                    ");"
            db!!.execSQL(qs)
        }
    }
}