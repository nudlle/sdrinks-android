package ru.nudlik.provider.user

import android.net.Uri
import android.provider.BaseColumns

class UserData {
    companion object {
        const val UD_ID = 0
        const val UD_USERID = 1
        const val UD_USEREMAIL = 2
        const val UD_USERPASSWORD = 3
        const val UD_USERAVATAR_URL = 4
        const val UD_USERBLACKLIST = 5

        const val AUTHORITY = "ru.nudlik.provider.user.UserData"
    }

    class DColumns : BaseColumns {

        companion object {
            const val USER_DATA_NAME = "userData"
            private val USER_DATA_URI: Uri = Uri.parse("content://$AUTHORITY/$USER_DATA_NAME")
            val USER_DATA_CONTENT_URI = USER_DATA_URI
            const val CONTENT_TYPE = "vnd.android.cursor.dir/vnd.userdata"
            const val CONTENT_ITEM_TYPE = "vnd.android.cursor.item/vnd.userdata"

            const val UD_USERID = "userId"
            const val UD_USEREMAIL = "email"
            const val UD_USERPASSWORD = "password"
            const val UD_USERAVATAR_URL = "user_avatar_url"
            const val UD_USERBLACKLIST = "user_in_blacklist"
        }
    }
}
