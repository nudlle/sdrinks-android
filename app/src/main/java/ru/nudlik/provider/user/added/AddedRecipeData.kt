package ru.nudlik.provider.user.added

import android.net.Uri
import android.provider.BaseColumns

class AddedRecipeData {
    companion object {
        const val ARD_ID = 0
        const val ARD_RECIPE_ID = 1

        const val AUTHORITY = "ru.nudlik.provider.user.added.AddedRecipeData"
    }

    class DColumns : BaseColumns {
        companion object {
            const val ADDED_RECIPE_DATA_NAME = "userCreditCardData"
            private val ADDED_RECIPE_DATA_URI: Uri = Uri.parse("content://$AUTHORITY/$ADDED_RECIPE_DATA_NAME")
            val ADDED_RECIPE_DATA_CONTENT_URI = ADDED_RECIPE_DATA_URI
            const val CONTENT_TYPE = "vnd.android.cursor.dir/vnd.addeddata"
            const val CONTENT_ITEM_TYPE = "vnd.android.cursor.item/vnd.addeddata"

            const val ARD_RECIPE_ID = "recipe_id"
        }
    }
}