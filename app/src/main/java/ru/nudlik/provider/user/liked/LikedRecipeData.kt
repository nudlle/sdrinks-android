package ru.nudlik.provider.user.liked

import android.net.Uri
import android.provider.BaseColumns

class LikedRecipeData {
    companion object {
        const val LRD_ID = 0
        const val LRD_RECIPE_ID = 1

        const val AUTHORITY = "ru.nudlik.provider.user.liked.LikedRecipeData"
    }

    class DColumns : BaseColumns {
        companion object {
            const val LIKED_RECIPE_DATA_NAME = "userCreditCardData"
            private val LIKED_RECIPE_DATA_URI: Uri = Uri.parse("content://$AUTHORITY/$LIKED_RECIPE_DATA_NAME")
            val LIKED_RECIPE_DATA_CONTENT_URI = LIKED_RECIPE_DATA_URI
            const val CONTENT_TYPE = "vnd.android.cursor.dir/vnd.likeddata"
            const val CONTENT_ITEM_TYPE = "vnd.android.cursor.item/vnd.likeddata"

            const val LRD_RECIPE_ID = "recipe_id"
        }
    }
}