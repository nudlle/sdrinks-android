package ru.nudlik.provider.user.added

import android.content.*
import android.database.Cursor
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteOpenHelper
import android.database.sqlite.SQLiteQueryBuilder
import android.net.Uri
import android.provider.BaseColumns
import android.text.TextUtils

class AddedRecipeProvider : ContentProvider() {
    private var dbHelper: OpenDatabaseHelper? = null
    private val uriMatcher: UriMatcher = UriMatcher(UriMatcher.NO_MATCH)

    init {
        uriMatcher.addURI(
            AddedRecipeData.AUTHORITY,
            AddedRecipeData.DColumns.ADDED_RECIPE_DATA_NAME,
            ADDED_DATA
        )
        uriMatcher.addURI(
            AddedRecipeData.AUTHORITY,
            AddedRecipeData.DColumns.ADDED_RECIPE_DATA_NAME + "/#",
            ADDED_DATA_ID
        )
    }

    companion object {
        const val ADDED_RECIPE_DATA = "added_recipe_data"
        const val ADDED_RECIPE_DATA_TABLE_NAME = "added_recipes_data"
        const val ADDED_DATA = 1
        const val ADDED_DATA_ID = 2
    }


    override fun insert(uri: Uri, values: ContentValues?): Uri? {
        val uriType = uriMatcher.match(uri)
        val id: Long
        val db = dbHelper!!.writableDatabase
        when(uriType) {
            ADDED_DATA -> id = db!!.insert(ADDED_RECIPE_DATA_TABLE_NAME, null, values)
            else -> throw IllegalArgumentException("Unknown URI: $uri")
        }
        context?.contentResolver?.notifyChange(uri, null)
        return ContentUris.withAppendedId(uri, id)
    }

    override fun query(
        uri: Uri,
        projection: Array<String>?,
        selection: String?,
        selectionArgs: Array<String>?,
        sortOrder: String?
    ): Cursor? {
        val queryBuilder = SQLiteQueryBuilder()
        queryBuilder.tables = ADDED_RECIPE_DATA_TABLE_NAME

        when (uriMatcher.match(uri)) {
            ADDED_DATA -> {}
            ADDED_DATA_ID -> queryBuilder.appendWhere(BaseColumns._ID + "=" + uri.lastPathSegment)
            else -> throw IllegalArgumentException("Unknown URI")
        }

        val cursor = queryBuilder.query(dbHelper!!.readableDatabase, projection, selection,
            selectionArgs, null, null, sortOrder)
        cursor.setNotificationUri(context?.contentResolver, uri)
        return cursor
    }

    override fun onCreate(): Boolean {
        dbHelper = OpenDatabaseHelper(context)
        return true
    }

    override fun update(uri: Uri, values: ContentValues?, selection: String?, selectionArgs: Array<String>?): Int {
        val uriType = uriMatcher.match(uri)
        val db = dbHelper!!.writableDatabase
        val rowUpdated = when (uriType) {
            ADDED_DATA -> db.update(ADDED_RECIPE_DATA_TABLE_NAME, values, selection, selectionArgs)
            ADDED_DATA_ID -> {
                val id = uri.lastPathSegment
                if (TextUtils.isEmpty(selection)) {
                    db.update(ADDED_RECIPE_DATA_TABLE_NAME, values, BaseColumns._ID + "=" + id, null)
                } else {
                    db.update(ADDED_RECIPE_DATA_TABLE_NAME, values, BaseColumns._ID + "=" + id + " and " + selection, selectionArgs)
                }
            }
            else -> throw IllegalArgumentException("Unknown URI: $uri")
        }
        context!!.contentResolver.notifyChange(uri, null)
        return rowUpdated
    }

    override fun delete(uri: Uri, selection: String?, selectionArgs: Array<String>?): Int {
        val uriType = uriMatcher.match(uri)
        val db = dbHelper!!.writableDatabase
        val rowDeleted = when (uriType) {
            ADDED_DATA -> db.delete(ADDED_RECIPE_DATA_TABLE_NAME, selection, selectionArgs)
            ADDED_DATA_ID -> {
                val id = uri.lastPathSegment
                if (TextUtils.isEmpty(selection)) {
                    db.delete(ADDED_RECIPE_DATA_TABLE_NAME, BaseColumns._ID + "=" + id, null)
                } else {
                    db.delete(ADDED_RECIPE_DATA_TABLE_NAME, BaseColumns._ID + "=" + id + " and " + selection, selectionArgs)
                }
            }
            else -> throw IllegalArgumentException("Unknown URI: $uri")
        }
        context!!.contentResolver.notifyChange(uri, null)
        return rowDeleted
    }

    override fun getType(uri: Uri): String? {
        return when (uriMatcher.match(uri)) {
            ADDED_DATA -> AddedRecipeData.DColumns.CONTENT_TYPE
            ADDED_DATA_ID -> AddedRecipeData.DColumns.CONTENT_ITEM_TYPE
            else -> throw IllegalArgumentException("Unknown URI: $uri")
        }
    }

    class OpenDatabaseHelper(context: Context?) : SQLiteOpenHelper(context,
        DATABASE_NAME, null,
        DATABASE_VERSION
    ) {
        override fun onCreate(db: SQLiteDatabase?) {
            createTable(db)
        }

        override fun onUpgrade(db: SQLiteDatabase?, oldVersion: Int, newVersion: Int) {
            db!!.execSQL("DROP TABLE IF EXISTS $ADDED_RECIPE_DATA_TABLE_NAME;")
            createTable(db)
        }

        companion object {
            const val DATABASE_NAME = "$ADDED_RECIPE_DATA.db"
            const val DATABASE_VERSION = 1
        }

        private fun createTable(db: SQLiteDatabase?) {
            val qs = "CREATE TABLE $ADDED_RECIPE_DATA_TABLE_NAME (" +
                    "${BaseColumns._ID} INTEGER PRIMARY KEY," +
                    "${AddedRecipeData.DColumns.ARD_RECIPE_ID} TEXT" +
                    ");"
            db!!.execSQL(qs)
        }
    }
}