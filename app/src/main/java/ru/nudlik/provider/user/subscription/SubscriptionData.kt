package ru.nudlik.provider.user.subscription

import android.net.Uri
import android.provider.BaseColumns

class SubscriptionData {
    companion object {
        const val S_ID = 0
        const val SUB_ID = 1
        const val SUB_PRICE = 2
        const val SUB_END_DATE = 3
        const val SUB_USER_ID = 4
        const val SUB_ACTIVATED = 5

        const val AUTHORITY = "ru.nudlik.provider.user.subscription.SubscriptionData"
    }

    class DColumns : BaseColumns {
        companion object {
            const val SUB_DATA_NAME = "subData"
            private val SUB_DATA_URI: Uri = Uri.parse("content://$AUTHORITY/$SUB_DATA_NAME")
            val SUB_DATA_CONTENT_URI = SUB_DATA_URI
            const val CONTENT_TYPE = "vnd.android.cursor.dir/vnd.subdata"
            const val CONTENT_ITEM_TYPE = "vnd.android.cursor.item/vnd.subdata"

            const val SUB_ID = "sub_id"
            const val SUB_PRICE = "sub_price"
            const val SUB_END_DATE = "sub_end_date"
            const val SUB_USER_ID = "sub_user_id"
            const val SUB_ACTIVATED = "sub_activated"
        }
    }
}