package ru.nudlik.provider.user

import android.content.*
import android.database.Cursor
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteOpenHelper
import android.database.sqlite.SQLiteQueryBuilder
import android.net.Uri
import android.provider.BaseColumns
import android.text.TextUtils
import java.lang.IllegalArgumentException

class UserDataProvider : ContentProvider() {
    private var dbHelper: OpenDatabaseHelper? = null
    private val uriMatcher: UriMatcher = UriMatcher(UriMatcher.NO_MATCH)

    init {
        uriMatcher.addURI(
            UserData.AUTHORITY,
            UserData.DColumns.USER_DATA_NAME,
            USERDATA
        )
        uriMatcher.addURI(
            UserData.AUTHORITY, UserData.DColumns.USER_DATA_NAME + "/#",
            USERDATA_ID
        )
    }

    companion object {
        const val USER_DATA = "user_data"
        const val USER_DATA_TABLE_NAME = "users_data"
        const val USERDATA = 1
        const val USERDATA_ID = 2
    }


    override fun insert(uri: Uri, values: ContentValues?): Uri? {
        val uriType = uriMatcher.match(uri)
        val id: Long
        val db = dbHelper!!.writableDatabase
        when(uriType) {
            USERDATA -> id = db!!.insert(USER_DATA_TABLE_NAME, null, values)
            else -> throw IllegalArgumentException("Unknown URI: $uri")
        }
        context?.contentResolver?.notifyChange(uri, null)
        return ContentUris.withAppendedId(uri, id)
    }

    override fun query(
        uri: Uri,
        projection: Array<String>?,
        selection: String?,
        selectionArgs: Array<String>?,
        sortOrder: String?
    ): Cursor? {
        val queryBuilder = SQLiteQueryBuilder()
        queryBuilder.tables = USER_DATA_TABLE_NAME

        when (uriMatcher.match(uri)) {
            USERDATA -> {}
            USERDATA_ID -> queryBuilder.appendWhere(BaseColumns._ID + "=" + uri.lastPathSegment)
            else -> throw IllegalArgumentException("Unknown URI")
        }

        val cursor = queryBuilder.query(dbHelper!!.readableDatabase, projection, selection,
            selectionArgs, null, null, sortOrder)
        cursor.setNotificationUri(context?.contentResolver, uri)
        return cursor
    }

    override fun onCreate(): Boolean {
        dbHelper = OpenDatabaseHelper(context)
        return true
    }

    override fun update(uri: Uri, values: ContentValues?, selection: String?, selectionArgs: Array<String>?): Int {
        val uriType = uriMatcher.match(uri)
        val db = dbHelper!!.writableDatabase
        val rowUpdated = when (uriType) {
            USERDATA -> db.update(USER_DATA_TABLE_NAME, values, selection, selectionArgs)
            USERDATA_ID -> {
                val id = uri.lastPathSegment
                if (TextUtils.isEmpty(selection)) {
                    db.update(USER_DATA_TABLE_NAME, values, BaseColumns._ID + "=" + id, null)
                } else {
                    db.update(
                        USER_DATA_TABLE_NAME, values, BaseColumns._ID + "=" + id + " and " + selection,
                        selectionArgs)
                }
            }
            else -> throw IllegalArgumentException("Unknown URI: $uri")
        }
        context!!.contentResolver.notifyChange(uri, null)
        return rowUpdated
    }

    override fun delete(uri: Uri, selection: String?, selectionArgs: Array<String>?): Int {
        val uriType = uriMatcher.match(uri)
        val db = dbHelper!!.writableDatabase
        val rowDeleted = when (uriType) {
            USERDATA -> db.delete(USER_DATA_TABLE_NAME, selection, selectionArgs)
            USERDATA_ID -> {
                val id = uri.lastPathSegment
                if (TextUtils.isEmpty(selection)) {
                    db.delete(USER_DATA_TABLE_NAME, BaseColumns._ID + "=" + id, null)
                } else {
                    db.delete(USER_DATA_TABLE_NAME, BaseColumns._ID + "=" + id + " and " + selection, selectionArgs)
                }
            }
            else -> throw IllegalArgumentException("Unknown URI: $uri")
        }
        context!!.contentResolver.notifyChange(uri, null)
        return rowDeleted
    }

    override fun getType(uri: Uri): String? {
        return when (uriMatcher.match(uri)) {
             USERDATA -> UserData.DColumns.CONTENT_TYPE
             USERDATA_ID -> UserData.DColumns.CONTENT_ITEM_TYPE
             else -> throw IllegalArgumentException("Unknown URI: $uri")
         }
    }

    class OpenDatabaseHelper(context: Context?) : SQLiteOpenHelper(context,
        DATABASE_NAME, null,
        DATABASE_VERSION
    ) {
        override fun onCreate(db: SQLiteDatabase?) {
            createTable(db)
        }

        override fun onUpgrade(db: SQLiteDatabase?, oldVersion: Int, newVersion: Int) {
            db!!.execSQL("DROP TABLE IF EXISTS $USER_DATA_TABLE_NAME;")
            createTable(db)
        }

        companion object {
            const val DATABASE_NAME = "$USER_DATA.db"
            const val DATABASE_VERSION = 1
        }

        private fun createTable(db: SQLiteDatabase?) {
            val qs = "CREATE TABLE $USER_DATA_TABLE_NAME (" +
                    "${BaseColumns._ID} INTEGER PRIMARY KEY," +
                    "${UserData.DColumns.UD_USERID} TEXT," +
                    "${UserData.DColumns.UD_USEREMAIL} TEXT," +
                    "${UserData.DColumns.UD_USERPASSWORD} TEXT," +
                    "${UserData.DColumns.UD_USERAVATAR_URL} TEXT," +
                    "${UserData.DColumns.UD_USERBLACKLIST} INTEGER" +
                    ");"
            db!!.execSQL(qs)
        }
    }
}