package ru.nudlik.provider.utils

import android.content.ContentResolver
import android.content.ContentValues
import ru.nudlik.domain.Subscription
import ru.nudlik.domain.User
import ru.nudlik.extentions.toDate
import ru.nudlik.extentions.toTextDate
import ru.nudlik.provider.user.UserData
import ru.nudlik.provider.user.added.AddedRecipeData
import ru.nudlik.provider.user.liked.LikedRecipeData
import ru.nudlik.provider.user.subscription.SubscriptionData
import ru.nudlik.security.CryptoUtil

interface ProviderHelper {
    companion object {
        private fun getOwnRecipes(contentResolver: ContentResolver?): List<String> {
            val result = ArrayList<String>()
            val c = contentResolver?.query(AddedRecipeData.DColumns.ADDED_RECIPE_DATA_CONTENT_URI, null, null, null, null)
            c.use { cc ->
                if (cc != null && cc.count > 0) {
                    cc.moveToPosition(-1)
                    while (cc.moveToNext()) {
                        result.add(cc.getString(AddedRecipeData.ARD_RECIPE_ID))
                    }
                }
            }
            return result
        }
        private fun isOwnRecipe(recipeId: String, contentResolver: ContentResolver?): Boolean {
            val where = "${AddedRecipeData.DColumns.ARD_RECIPE_ID}=?"
            val c = contentResolver?.query(AddedRecipeData.DColumns.ADDED_RECIPE_DATA_CONTENT_URI, null, where, arrayOf(recipeId), null)
            c.use { cc ->
                return cc != null && c!!.count == 1
            }
        }
        fun storeUserData(user: User, cryptoUtil: CryptoUtil, contentResolver: ContentResolver?) {
            val values = ContentValues()
            values.put(UserData.DColumns.UD_USERID, user.userId)
            values.put(UserData.DColumns.UD_USEREMAIL, user.email)
            values.put(
                UserData.DColumns.UD_USERPASSWORD,
                cryptoUtil.encode(CryptoUtil.PIN_ALIAS, user.password!!))
            val blacklist = if (user.blacklist) { 1 } else { 0 }
            values.put(UserData.DColumns.UD_USERBLACKLIST, blacklist)
            values.put(UserData.DColumns.UD_USERAVATAR_URL, user.avatarUrl)

            contentResolver?.insert(UserData.DColumns.USER_DATA_CONTENT_URI, values)

            if (user.userSubscription != null) {
                storeUserSubscription(user.userSubscription!!, contentResolver)
            }
        }
        fun updateUserData(user: User, contentResolver: ContentResolver?) {
            val values = ContentValues()
            val blacklist = if (user.blacklist) { 1 } else { 0 }
            values.put(UserData.DColumns.UD_USERBLACKLIST, blacklist)
            values.put(UserData.DColumns.UD_USERAVATAR_URL, user.avatarUrl)

            contentResolver?.update(UserData.DColumns.USER_DATA_CONTENT_URI, values, null, null)

            updateUserLikedRecipes(user.likedRecipes, contentResolver)
            updateOwnRecipes(user.ownRecipes, contentResolver)
            user.userSubscription?.apply {
                storeUserSubscription(this, contentResolver)
            }
        }
        private fun updateUserLikedRecipes(likedRecipes: MutableList<String>, contentResolver: ContentResolver?) {
            val dbLikedRecipes = getLikedRecipes(contentResolver)
            dbLikedRecipes.forEach { id ->
                if (!likedRecipes.contains(id)) {
                    deleteLikedRecipe(id, contentResolver)
                }
            }
            likedRecipes.forEach { id ->
                if (!dbLikedRecipes.contains(id)) {
                    addLikedRecipe(id, contentResolver)
                }
            }
        }
        private fun updateOwnRecipes(ownRecipes: MutableList<String>, contentResolver: ContentResolver?) {
            val dbOwlRecipes = getOwnRecipes(contentResolver)
            dbOwlRecipes.forEach { id ->
                if (!ownRecipes.contains(id)) {
                    deleteOwnRecipe(id, contentResolver)
                }
            }
            ownRecipes.forEach { id ->
                if (!dbOwlRecipes.contains(id)) {
                    addOwnRecipe(id, contentResolver)
                }
            }
        }

        fun storeUserSubscription(sub: Subscription, contentResolver: ContentResolver?) {
            val values = ContentValues()
            values.put(SubscriptionData.DColumns.SUB_ID, sub.subscriptionId)
            values.put(SubscriptionData.DColumns.SUB_PRICE, sub.price.toString())
            values.put(SubscriptionData.DColumns.SUB_END_DATE, sub.endDate?.toTextDate("d MMM yyyy"))
            values.put(SubscriptionData.DColumns.SUB_USER_ID, sub.userId)
            values.put(SubscriptionData.DColumns.SUB_ACTIVATED, if (sub.activated) 1 else 0)

            val c = contentResolver?.query(SubscriptionData.DColumns.SUB_DATA_CONTENT_URI, null, null, null, null)
            c.use { cc ->
                if (cc != null && cc.count > 0) {
                    val where = "${SubscriptionData.DColumns.SUB_USER_ID}=?"
                    contentResolver?.delete(SubscriptionData.DColumns.SUB_DATA_CONTENT_URI, where, arrayOf(sub.userId))
                }
            }

            contentResolver?.insert(SubscriptionData.DColumns.SUB_DATA_CONTENT_URI, values)
        }
        fun storeUserAvatar(contentResolver: ContentResolver, url: String, userId: String) {
            val values = ContentValues(1)
            values.put(UserData.DColumns.UD_USERAVATAR_URL, url)
            val where = "${UserData.DColumns.UD_USERID}=?"
            contentResolver.update(UserData.DColumns.USER_DATA_CONTENT_URI, values, where, arrayOf(userId))
        }
        fun restoreUserData(contentResolver: ContentResolver): User? {
            var user: User? = null
            var c = contentResolver.query(UserData.DColumns.USER_DATA_CONTENT_URI, null, null, null, null)
            c.use { cc ->
                if (cc != null && cc.count > 0) {
                    cc.moveToFirst()
                    user = User()
                    user!!.userId = cc.getString(UserData.UD_USERID)
                    user!!.email = cc.getString(UserData.UD_USEREMAIL)
                    user!!.password = cc.getString(UserData.UD_USERPASSWORD)
                    user!!.avatarUrl = cc.getString(UserData.UD_USERAVATAR_URL)
                    user!!.blacklist = cc.getInt(UserData.UD_USERBLACKLIST) == 1
                }
            }
            c = contentResolver.query(SubscriptionData.DColumns.SUB_DATA_CONTENT_URI, null, null, null, null)
            c.use { cc ->
                if (cc != null && cc.count > 0) {
                    cc.moveToFirst()
                    user?.userSubscription = Subscription()
                    user?.userSubscription?.subscriptionId = cc.getString(SubscriptionData.SUB_ID)
                    user?.userSubscription?.price = cc.getDouble(SubscriptionData.SUB_PRICE)
                    user?.userSubscription?.endDate = cc.getString(SubscriptionData.SUB_END_DATE).toDate("d MMM yyyy")
                    user?.userSubscription?.userId = cc.getString(SubscriptionData.SUB_USER_ID)
                    user?.userSubscription?.activated = cc.getInt(SubscriptionData.SUB_ACTIVATED) == 1
                }
            }
            user?.likedRecipes?.addAll(getLikedRecipes(contentResolver))
            user?.ownRecipes?.addAll(getOwnRecipes(contentResolver))
            return user
        }
        fun deleteUser(user: User, contentResolver: ContentResolver?) {
            var where = "${UserData.DColumns.UD_USERID}=?"
            contentResolver?.delete(UserData.DColumns.USER_DATA_CONTENT_URI, where, arrayOf(user.userId))
            where = "${SubscriptionData.DColumns.SUB_USER_ID}=?"
            contentResolver?.delete(SubscriptionData.DColumns.SUB_DATA_CONTENT_URI, where, arrayOf(user.userId))
        }
        fun getLikedRecipes(contentResolver: ContentResolver?): List<String> {
            val result = ArrayList<String>()
            val c = contentResolver?.query(LikedRecipeData.DColumns.LIKED_RECIPE_DATA_CONTENT_URI, null, null, null, null)
            c.use { cc ->
                if (cc != null && cc.count > 0) {
                    cc.moveToPosition(-1)
                    while (cc.moveToNext()) {
                        result.add(cc.getString(LikedRecipeData.LRD_RECIPE_ID))
                    }
                }
            }
            return result
        }
        fun isLikedRecipe(recipeId: String, contentResolver: ContentResolver?): Boolean {
            val where = "${LikedRecipeData.DColumns.LRD_RECIPE_ID}=?"
            val c = contentResolver?.query(LikedRecipeData.DColumns.LIKED_RECIPE_DATA_CONTENT_URI, null, where, arrayOf(recipeId), null)
            c.use { cc ->
                return cc != null && c!!.count == 1
            }
        }
        fun addLikedRecipe(recipeId: String, contentResolver: ContentResolver?) {
            if (!isLikedRecipe(recipeId, contentResolver)) {
                val values = ContentValues(1)
                values.put(LikedRecipeData.DColumns.LRD_RECIPE_ID, recipeId)
                contentResolver?.insert(LikedRecipeData.DColumns.LIKED_RECIPE_DATA_CONTENT_URI, values)
            }
        }
        fun deleteLikedRecipe(recipeId: String, contentResolver: ContentResolver?) {
            val where = "${LikedRecipeData.DColumns.LRD_RECIPE_ID}=?"
            contentResolver?.delete(LikedRecipeData.DColumns.LIKED_RECIPE_DATA_CONTENT_URI, where, arrayOf(recipeId))
        }
        fun addOwnRecipe(recipeId: String, contentResolver: ContentResolver?) {
            if (!isOwnRecipe(recipeId, contentResolver)) {
                val values = ContentValues(1)
                values.put(AddedRecipeData.DColumns.ARD_RECIPE_ID, recipeId)
                contentResolver?.insert(AddedRecipeData.DColumns.ADDED_RECIPE_DATA_CONTENT_URI, values)
            }
        }
        fun deleteOwnRecipe(recipeId: String, contentResolver: ContentResolver?) {
            val where = "${AddedRecipeData.DColumns.ARD_RECIPE_ID}=?"
            contentResolver?.delete(AddedRecipeData.DColumns.ADDED_RECIPE_DATA_CONTENT_URI, where, arrayOf(recipeId))
        }
    }
}