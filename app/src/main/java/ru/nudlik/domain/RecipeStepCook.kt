package ru.nudlik.domain

import java.io.Serializable

class RecipeStepCook : Serializable {
    var recipeStepCookId: String? = null
    var number: Int = 0
    var imageUrl: String? = null
    var description: String? = null
    var recipeId: String? = "none"

    constructor()

    constructor(number: Int, imageUrl: String, description: String, recipeId: String) {
        this.number = number
        this.imageUrl = imageUrl
        this.description = description
        this.recipeId = recipeId
    }

    constructor(recipeStepCook: RecipeStepCook?) {
        this.recipeStepCookId = recipeStepCook?.recipeStepCookId
        this.number = recipeStepCook?.number ?: 0
        this.imageUrl = recipeStepCook?.imageUrl
        this.description = recipeStepCook?.description
        this.recipeId = recipeStepCook?.recipeId
    }

    companion object {
        private const val serialVersionUID = 12938701231234L
    }

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as RecipeStepCook

        if (recipeStepCookId != other.recipeStepCookId) return false
        if (number != other.number) return false
        if (imageUrl != other.imageUrl) return false
        if (description != other.description) return false
        if (recipeId != other.recipeId) return false

        return true
    }

    override fun hashCode(): Int {
        var result = recipeStepCookId?.hashCode() ?: 0
        result = 31 * result + number
        result = 31 * result + (imageUrl?.hashCode() ?: 0)
        result = 31 * result + (description?.hashCode() ?: 0)
        result = 31 * result + (recipeId?.hashCode() ?: 0)
        return result
    }

    override fun toString(): String {
        return "RecipeStepCook(recipeStepCookId=$recipeStepCookId, number=$number, imageUrl=$imageUrl, description=$description, recipeId=$recipeId)"
    }
}
