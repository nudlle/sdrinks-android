package ru.nudlik.domain

import java.io.Serializable
import java.util.Objects

class Ad : Serializable {
    var adId: String? = null
    var recipeId: String? = "none"
    var externalLink: String? = null
    var imageUrl: String? = null
    var description: String? = null
    var stepPosition: Int = 0
    var clicked: Long = 0

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (other == null || javaClass != other.javaClass) return false
        val ad = other as Ad?
        return stepPosition == ad!!.stepPosition &&
                adId == ad.adId &&
                recipeId == ad.recipeId &&
                externalLink == ad.externalLink &&
                imageUrl == ad.imageUrl &&
                description == ad.description &&
                clicked == ad.clicked
    }

    override fun hashCode(): Int {
        return Objects.hash(adId, recipeId, externalLink, imageUrl, description, stepPosition, clicked)
    }

    override fun toString(): String {
        return "Ad{" +
                "adId='" + adId + '\''.toString() +
                ", recipeId='" + recipeId + '\''.toString() +
                ", externalLink='" + externalLink + '\''.toString() +
                ", imageUrl='" + imageUrl + '\''.toString() +
                ", description='" + description + '\''.toString() +
                ", stepPosition=" + stepPosition +
                ", clicked=" + clicked +
                '}'.toString()
    }

    companion object {
        private const val serialVersionUID = 813499837L
    }
}
