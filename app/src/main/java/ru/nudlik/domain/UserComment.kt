package ru.nudlik.domain

import java.io.Serializable
import java.util.Date
import java.util.Objects

class UserComment : Serializable {
    var commentId: String? = null
    var userId: String? = null
    var recipeId: String? = null
    var userEmail: String? = null
    var comment: String? = null
    var rate: Double? = null
    var date: Date? = null

    constructor()

    constructor(commentId: String, userId: String, recipeId: String, userEmail: String, comment: String, rate: Double, date: Date) {
        this.commentId = commentId
        this.userId = userId
        this.recipeId = recipeId
        this.userEmail = userEmail
        this.comment = comment
        this.rate = rate
        this.date = date
    }

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (other == null || javaClass != other.javaClass) return false
        val comment = other as UserComment?
        return commentId == comment!!.commentId &&
                userId == comment.userId &&
                recipeId == comment.recipeId &&
                userEmail == comment.userEmail &&
                this.comment == comment.comment &&
                rate == comment.rate &&
                date == comment.date
    }

    override fun hashCode(): Int {
        return Objects.hash(commentId, userId, recipeId, userEmail, comment, rate, date)
    }

    override fun toString(): String {
        return "UserComment(commentId=$commentId, userId=$userId, recipeId=$recipeId, userEmail=$userEmail, comment=$comment, rate=$rate, date=$date)"
    }


    companion object {
        private const val serialVersionUID = 876521342888324L
    }
}
