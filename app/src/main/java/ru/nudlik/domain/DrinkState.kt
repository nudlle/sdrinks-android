package ru.nudlik.domain

enum class DrinkState {
    Hot, Cold, Unspecified
}