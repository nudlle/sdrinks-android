package ru.nudlik.domain

enum class DrinkTaste {
    Berries, Fruit, Vegetable, Tasty, Sour, Spicy, Herb
}
