package ru.nudlik.domain

import java.io.Serializable
import java.util.*

class UserMessage : Serializable {
    var userMessageId: String? = null
    var userId: String? = null
    var message: String? = null
    var appVersion: String? = null
    var date: Date? = null

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (other == null || javaClass != other.javaClass) return false
        val that = other as UserMessage?
        return date == that!!.date &&
                userMessageId == that.userMessageId &&
                userId == that.userId &&
                message == that.message &&
                appVersion == that.appVersion
    }

    override fun hashCode(): Int {
        return Objects.hash(userMessageId, userId, message, appVersion, date)
    }

    override fun toString(): String {
        return "UserMessage{" +
                "userMessageId='" + userMessageId + '\''.toString() +
                ", userId='" + userId + '\''.toString() +
                ", message='" + message + '\''.toString() +
                ", appVersion='" + appVersion + '\''.toString() +
                ", date=" + date +
                '}'.toString()
    }

    companion object {
        private const val serialVersionUID = 763421342888324L
    }
}
