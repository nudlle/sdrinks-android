package ru.nudlik.domain


import java.io.Serializable
import java.util.ArrayList
import java.util.Date

class Recipe : Serializable {
    var recipeId: String? = null
    var name: String? = null
    var secondaryName: String? = null
    var userId: String? = null
    var userEmail: String? = null
    var drinkState: DrinkState? = null
    var drinkType: DrinkType? = null
    var drinkTastes: MutableList<DrinkTaste>
    var recipeSteps: MutableList<RecipeStepCook>
    var ingredients: MutableList<String>
    var imageUrls: ImageUrls
    var rate: Double = 0.toDouble()
    var commentCount: Long = 0
    var date: Date? = null
    var enable: Boolean = false
    var alcohol: Boolean = false
    var score: Float? = null
    var searchIndex: String? = null
    var ad: Ad? = null
    var viewed: Long = 0
    var liked: Long = 0

    constructor() {
        drinkTastes = ArrayList()
        recipeSteps = ArrayList()
        ingredients = ArrayList()
        imageUrls = ImageUrls()
        enable = false
        alcohol = false
    }

    constructor(userId: String, userEmail: String) {
        this.userId = userId
        this.userEmail = userEmail
        this.drinkTastes = ArrayList()
        this.recipeSteps = ArrayList()
        this.ingredients = ArrayList()
        this.rate = 0.0
        this.commentCount = 0
        this.imageUrls = ImageUrls()
        this.enable = false
    }

    companion object {
        private const val serialVersionUID = 123421342888324L
    }

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as Recipe

        if (recipeId != other.recipeId) return false
        if (name != other.name) return false
        if (secondaryName != other.secondaryName) return false
        if (userId != other.userId) return false
        if (userEmail != other.userEmail) return false
        if (drinkState != other.drinkState) return false
        if (drinkType != other.drinkType) return false
        if (drinkTastes != other.drinkTastes) return false
        if (recipeSteps != other.recipeSteps) return false
        if (ingredients != other.ingredients) return false
        if (imageUrls != other.imageUrls) return false
        if (rate != other.rate) return false
        if (commentCount != other.commentCount) return false
        if (date != other.date) return false
        if (enable != other.enable) return false
        if (alcohol != other.alcohol) return false
        if (score != other.score) return false
        if (searchIndex != other.searchIndex) return false
        if (ad != other.ad) return false
        if (viewed != other.viewed) return false
        if (liked != other.liked) return false

        return true
    }

    override fun hashCode(): Int {
        var result = recipeId?.hashCode() ?: 0
        result = 31 * result + (name?.hashCode() ?: 0)
        result = 31 * result + (secondaryName?.hashCode() ?: 0)
        result = 31 * result + (userId?.hashCode() ?: 0)
        result = 31 * result + (userEmail?.hashCode() ?: 0)
        result = 31 * result + (drinkState?.hashCode() ?: 0)
        result = 31 * result + (drinkType?.hashCode() ?: 0)
        result = 31 * result + drinkTastes.hashCode()
        result = 31 * result + recipeSteps.hashCode()
        result = 31 * result + ingredients.hashCode()
        result = 31 * result + imageUrls.hashCode()
        result = 31 * result + rate.hashCode()
        result = 31 * result + commentCount.hashCode()
        result = 31 * result + (date?.hashCode() ?: 0)
        result = 31 * result + enable.hashCode()
        result = 31 * result + alcohol.hashCode()
        result = 31 * result + (score?.hashCode() ?: 0)
        result = 31 * result + (searchIndex?.hashCode() ?: 0)
        result = 31 * result + (ad?.hashCode() ?: 0)
        result = 31 * result + viewed.hashCode()
        result = 31 * result + liked.hashCode()
        return result
    }

    override fun toString(): String {
        return "Recipe(recipeId=$recipeId, name=$name, " +
                "secondaryName=$secondaryName, " +
                "userId=$userId, " +
                "userEmail=$userEmail, " +
                "drinkState=$drinkState, " +
                "drinkType=$drinkType, " +
                "drinkTastes=$drinkTastes, " +
                "recipeSteps=$recipeSteps, " +
                "ingredients=$ingredients, " +
                "imageUrls=$imageUrls, " +
                "rate=$rate, " +
                "commentCount=$commentCount, " +
                "date=$date, " +
                "enable=$enable, " +
                "alcohol=$alcohol, " +
                "score=$score," +
                "searchIndex=$searchIndex" +
                "ad=$ad" +
                "viewed=$viewed" +
                "liked=$liked" +
                ")"
    }
}
