package ru.nudlik.domain

import java.io.Serializable
import java.util.Objects


class Cart : Serializable {
    private val cartId: String? = null
    var userId: String? = null
    var totalPrice: Int = 0

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (other == null || javaClass != other.javaClass) return false
        val cart = other as Cart?
        return totalPrice == cart!!.totalPrice &&
                cartId == cart.cartId &&
                userId == cart.userId
    }

    override fun hashCode(): Int {
        return Objects.hash(cartId, userId, totalPrice)
    }

    override fun toString(): String {
        return "Cart{" +
                "cartId='" + cartId + '\''.toString() +
                ", userId='" + userId + '\''.toString() +
                ", totalPrice=" + totalPrice +
                '}'.toString()
    }

    companion object {
        private const val serialVersionUID = 1521342888324L
    }
}
