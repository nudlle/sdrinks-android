package ru.nudlik.domain

import java.io.Serializable
import java.util.Date
import java.util.Objects

class Subscription : Serializable {
    var subscriptionId: String? = null
    var endDate: Date? = null
    var price: Double? = null
    var userId: String? = null
    var activated: Boolean = false

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (other == null || javaClass != other.javaClass) return false
        val that = other as Subscription?
        return subscriptionId == that!!.subscriptionId &&
                endDate == that.endDate &&
                price == that.price &&
                userId == that.userId &&
                activated == that.activated
    }

    override fun hashCode(): Int {
        return Objects.hash(subscriptionId, endDate, price, userId, activated)
    }

    override fun toString(): String {
        return "Subscription{" +
                "subscriptionId='" + subscriptionId + '\''.toString() +
                ", startDate=" + endDate +
                ", price=" + price +
                ", userId='" + userId + '\''.toString() +
                ", activated=" + activated +
                '}'.toString()
    }

    companion object {
        private const val serialVersionUID = 58139493L
    }
}
