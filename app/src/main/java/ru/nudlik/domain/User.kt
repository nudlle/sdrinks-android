package ru.nudlik.domain

import java.io.Serializable
import java.util.ArrayList
import java.util.Objects

class User : Serializable {
    var userId: String? = null
    var email: String? = null
    var password: String? = null
    var avatarUrl: String? = null
    var blacklist: Boolean = false
    val ownRecipes: MutableList<String>
    val likedRecipes: MutableList<String>
    var userSubscription: Subscription? = null

    constructor() {
        ownRecipes = ArrayList()
        likedRecipes = ArrayList()
    }

    constructor(userId: String, email: String, password: String, avatarUrl: String) {
        this.userId = userId
        this.email = email
        this.password = password
        this.avatarUrl = avatarUrl
        this.ownRecipes = ArrayList()
        this.likedRecipes = ArrayList()
    }

    constructor(user: User) {
        this.userId = user.userId
        this.email = user.email
        this.avatarUrl = user.avatarUrl
        this.blacklist = user.blacklist
        this.password = user.password
        this.ownRecipes = user.ownRecipes
        this.likedRecipes = user.likedRecipes
        this.userSubscription = user.userSubscription
    }

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (other == null || javaClass != other.javaClass) return false
        val user = other as User?
        return blacklist == user!!.blacklist &&
                userId == user.userId &&
                email == user.email &&
                password == user.password &&
                ownRecipes == user.ownRecipes &&
                likedRecipes == user.likedRecipes &&
                avatarUrl == user.avatarUrl &&
                userSubscription == user.userSubscription
    }

    override fun hashCode(): Int {
        return Objects.hash(
            userId, email, password, blacklist, ownRecipes,
            likedRecipes, avatarUrl,
            userSubscription
        )
    }

    override fun toString(): String {
        return "User{" +
                "userId='" + userId + '\''.toString() +
                ", email='" + email + '\''.toString() +
                ", avatarUrl='" + avatarUrl + '\''.toString() +
                ", password='" + password + '\''.toString() +
                ", blacklist=" + blacklist +
                ", ownRecipes=" + ownRecipes +
                ", likedRecipes=" + likedRecipes +
                ", userSubscription=" + userSubscription +
                '}'.toString()
    }

    companion object {
        private const val serialVersionUID = 12380989342888324L
    }
}
