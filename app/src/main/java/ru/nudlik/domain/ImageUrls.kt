package ru.nudlik.domain

import java.io.Serializable
import java.util.Objects

class ImageUrls : Serializable {
    var large: String? = null
    var medium: String? = null
    var small: String? = null

    constructor()

    constructor(large: String, medium: String, small: String) {
        this.large = large
        this.medium = medium
        this.small = small
    }


    companion object {
        private const val serialVersionUID = 887324111341L
    }

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (other == null || javaClass != other.javaClass) return false
        val imageUrls = other as ImageUrls?
        return large == imageUrls!!.large &&
                medium == imageUrls.medium &&
                small == imageUrls.small
    }

    override fun hashCode(): Int {
        return Objects.hash(large, medium, small)
    }

    override fun toString(): String {
        return "ImageUrls{" +
                "large='" + large + '\''.toString() +
                ", medium='" + medium + '\''.toString() +
                ", small='" + small + '\''.toString() +
                '}'.toString()
    }
}
