package ru.nudlik.domain

import java.io.Serializable
import java.util.Objects

class RecipesToSearch : Serializable {
    var search: String? = null
    var state: DrinkState? = null
    var type: DrinkType? = null
    var tastes: List<DrinkTaste>? = null
    var alcohol: Boolean? = null

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (other == null || javaClass != other.javaClass) return false
        val that = other as RecipesToSearch?
        return alcohol == that!!.alcohol &&
                search == that.search &&
                state === that.state &&
                type === that.type &&
                tastes == that.tastes
    }

    override fun hashCode(): Int {
        return Objects.hash(search, state, type, tastes, alcohol)
    }

    override fun toString(): String {
        return "RecipesToSearch{" +
                "text='" + search + '\''.toString() +
                ", state=" + state +
                ", type=" + type +
                ", tastes=" + tastes +
                ", alcohol=" + alcohol +
                '}'.toString()
    }

    companion object {
        private const val serialVersionUID = 12322022238324L
    }
}
