package ru.nudlik.domain

enum class DrinkType {
    Lemonade, Tea, Coffee, Shot, Cocktail, Milkshake, Liqueur
}
