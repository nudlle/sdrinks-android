package ru.nudlik.activity

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import okhttp3.Interceptor
import okhttp3.Response
import ru.nudlik.MainActivity
import ru.nudlik.R
import ru.nudlik.domain.Recipe
import ru.nudlik.domain.User
import ru.nudlik.response.ServerResponseCode
import ru.nudlik.security.CryptoUtil
import ru.nudlik.security.CryptoUtilImpl
import ru.nudlik.security.UserRequest
import ru.nudlik.service.rest.RestClient
import ru.nudlik.service.rest.RestUtilService
import ru.nudlik.singleton.CurrentActiveRecipe
import ru.nudlik.singleton.SingletonInitializer
import ru.nudlik.singleton.UserSingleton

class DeepLinkResolverActivity : AppCompatActivity() {
    private lateinit var restUtilService: RestUtilService

    private fun startResultActivity(bad: Boolean, recipe: Recipe?) {
        if (bad) {
            startActivity(Intent(this@DeepLinkResolverActivity, MainActivity::class.java))
            finish()
        } else {
            val intent = Intent(this@DeepLinkResolverActivity, RecipeViewActivity::class.java)
            CurrentActiveRecipe.recipe = recipe
            intent.putExtra(MainActivity.RECIPE_CREATION_FLAG, false)
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
            startActivity(intent)
            finish()
        }
    }

    private val responseCallback = object : RestUtilService.ResponseCallback {
        override fun <T> success(data: T?) {
            startResultActivity(false, data as Recipe)
        }

        override fun errorCode(responseCode: ServerResponseCode?) {
            startResultActivity(true, null)
        }

        override fun exception(e: Throwable?) {
            startResultActivity(true, null)
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        setTheme(R.style.AppTheme)
        super.onCreate(savedInstanceState)

        setContentView(R.layout.deep_link_resolver_progress)

        UserSingleton.initUser(contentResolver)
        SingletonInitializer.init(this@DeepLinkResolverActivity, AuthFailInterceptor())

        restUtilService = RestUtilService(this@DeepLinkResolverActivity)
        restUtilService.showExceptionDialog = false

        val data = intent?.data
        if (data != null) {
            val recipeId = data.lastPathSegment
            if (recipeId != null) {
                restUtilService.getRecipeById(recipeId, responseCallback)
            } else {
                startResultActivity(true, null)
            }
        } else {
            startResultActivity(true, null)
        }
    }

    override fun onDestroy() {
        restUtilService.destroy()
        super.onDestroy()
    }

    inner class AuthFailInterceptor : Interceptor {
        private var loginProceeding = false
        override fun intercept(chain: Interceptor.Chain): Response {
            val request = chain.request()
            var response = chain.proceed(request)

            if (response.code() == 401) {
                if (!loginProceeding) {
                    val user = UserSingleton.getUser()
                    if (user != null) {
                        SingletonInitializer.deleteCredentials()
                        loginProceeding = true
                        val loginUser = User()
                        loginUser.userId = user.userId
                        loginUser.password = CryptoUtilImpl().decode(CryptoUtil.PIN_ALIAS, user.password!!)
                        val userRequest = UserRequest(loginUser, System.currentTimeMillis())
                        val result = RestClient.loginImpl(restUtilService.restClient, userRequest)?.blockingGet()
                        if (result != null && result.code == ServerResponseCode.OK_RESPONSE) {
                            SingletonInitializer.addCredentials(result.data!!.id!!, result.data!!.token!!)
                            response = chain.proceed(request)
                        }
                        loginProceeding = false
                    }
                }
            }

            return response
        }
    }
}