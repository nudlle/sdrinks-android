package ru.nudlik.activity

import android.content.DialogInterface
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import ru.nudlik.R

abstract class BaseActivity : AppCompatActivity() {
    private var alertDialog: AlertDialog? = null


    override fun onStop() {
        super.onStop()
        alertDialog?.apply {
            if (isShowing) {
                dismiss()
            }
        }
    }

    fun requestPermission(permission: String, rationale: String, requestCode: Int) {
        showAlertDialog(getString(R.string.permission_title_rationale), rationale,
            DialogInterface.OnClickListener { _, _ ->
                ActivityCompat.requestPermissions(this@BaseActivity, arrayOf(permission), requestCode)
            }, getString(R.string.ok), null, getString(R.string.cancel))
    }

    private fun showAlertDialog(title: String, message: String,
                                positiveClickListener: DialogInterface.OnClickListener,
                                positiveText: String,
                                negativeClickListener: DialogInterface.OnClickListener?,
                                negativeText: String) {
        val builder = AlertDialog.Builder(this@BaseActivity)
        builder.setTitle(title)
        builder.setMessage(message)
        builder.setPositiveButton(positiveText, positiveClickListener)
        builder.setNegativeButton(negativeText, negativeClickListener)
        alertDialog = builder.show()
    }

    companion object {
        const val REQUEST_STORAGE_READ_ACCESS_PERMISSION = 101
        const val REQUEST_STORAGE_WRITE_ACCESS_PERMISSION = 102
    }
}