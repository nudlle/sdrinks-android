package ru.nudlik.activity

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import com.google.android.material.appbar.CollapsingToolbarLayout
import ru.nudlik.R
import ru.nudlik.fragment.AboutAppFragment
import android.content.Intent
import android.content.ActivityNotFoundException
import android.net.Uri
import androidx.constraintlayout.widget.ConstraintLayout
import com.google.android.material.floatingactionbutton.ExtendedFloatingActionButton
import ru.nudlik.fragment.dialog.SendMessageDialogFragment
import ru.nudlik.fragment.snackbar.SnackbarMaker
import ru.nudlik.interfaces.DialogButtonsListener
import ru.nudlik.service.rest.RestUtilService
import ru.nudlik.singleton.UserSingleton
import ru.nudlik.widget.PulseLayout


class AboutAppActivity : AppCompatActivity() {
    private lateinit var parentContainer: ConstraintLayout
    private lateinit var restUtilService: RestUtilService
    private lateinit var appErrorPulse: PulseLayout
    private lateinit var rateAppPulse: PulseLayout
    private val positiveClick = object : DialogButtonsListener {
        override fun positiveClick(ext: Any?) {
            SnackbarMaker.showShortSnackbar(parentContainer, R.string.message_posted, R.string.ok)
        }

    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(R.layout.activity_about)

        val toolbar = findViewById<Toolbar>(R.id.toolbar_about)
        toolbar.setTitle(R.string.about_app)
        setSupportActionBar(toolbar)
        val collapsingToolbar = findViewById<CollapsingToolbarLayout>(R.id.collapsing_toolbar_about)
        collapsingToolbar.isTitleEnabled = false

        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setDisplayShowHomeEnabled(true)

        parentContainer = findViewById(R.id.app_error_parent_container)

        val pInfo = packageManager.getPackageInfo(packageName, 0)
        val version = pInfo.versionName

        val userId = UserSingleton.getUser()?.userId ?: "no_login"

        restUtilService = RestUtilService(this@AboutAppActivity)

        appErrorPulse = findViewById(R.id.send_app_error_request_pulse_layout)
        rateAppPulse = findViewById(R.id.rate_app_in_market_pulse_layout)

        findViewById<ExtendedFloatingActionButton>(R.id.send_message_us).apply {
            setOnClickListener {
                val dialog = SendMessageDialogFragment.createInstance(userId, version, restUtilService)
                dialog.buttonsClickListener = positiveClick
                dialog.show(supportFragmentManager, "app_error_request_dialog")
            }
        }

        findViewById<ExtendedFloatingActionButton>(R.id.rate_app_in_market).apply {
            setOnClickListener {
                rateThisApp()
            }
        }

        supportFragmentManager.beginTransaction().replace(R.id.about_fragment_container,
            AboutAppFragment.createInstance(), AboutAppFragment.TAG).commit()
    }

    private fun rateThisApp() {
        val uri = Uri.parse("market://details?id=$packageName")
        val goToMarket = Intent(Intent.ACTION_VIEW, uri)
        goToMarket.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY or
                            Intent.FLAG_ACTIVITY_NEW_DOCUMENT or
                            Intent.FLAG_ACTIVITY_MULTIPLE_TASK)
        try {
            startActivity(goToMarket)
        }
        catch (e:ActivityNotFoundException) {
            startActivity(Intent(Intent.ACTION_VIEW, Uri.parse("http://play.google.com/store/apps/details?id=$packageName")))
        }
    }

    override fun onResume() {
        super.onResume()
        appErrorPulse.start()
        rateAppPulse.start()
    }

    override fun onPause() {
        super.onPause()
        appErrorPulse.stop()
        rateAppPulse.stop()
    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed();
        return true
    }

    override fun onDestroy() {
        restUtilService.destroy()
        super.onDestroy()
    }
}