package ru.nudlik.activity

import android.Manifest
import android.app.Activity
import android.content.DialogInterface
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.graphics.drawable.Drawable
import android.net.Uri
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.widget.*
import androidx.appcompat.widget.Toolbar
import androidx.coordinatorlayout.widget.CoordinatorLayout
import androidx.core.app.ActivityCompat
import androidx.core.widget.NestedScrollView
import androidx.palette.graphics.Palette
import com.google.android.material.appbar.CollapsingToolbarLayout
import com.google.android.material.button.MaterialButton
import com.squareup.picasso.Picasso
import com.squareup.picasso.Target
import com.yalantis.ucrop.UCrop
import okhttp3.MediaType
import okhttp3.MultipartBody
import okhttp3.RequestBody
import ru.nudlik.R
import ru.nudlik.activity.ucrop.UCropHelper
import ru.nudlik.domain.Subscription
import ru.nudlik.domain.User
import ru.nudlik.extentions.*
import ru.nudlik.fragment.dialog.AlertDialogBuilder
import ru.nudlik.fragment.nav.ProfileContentFragment
import ru.nudlik.fragment.snackbar.SnackbarMaker
import ru.nudlik.fragment.toast.ToastMaker
import ru.nudlik.listener.BuySubscriptionListener
import ru.nudlik.listener.FinishWithGoto
import ru.nudlik.provider.utils.ProviderHelper
import ru.nudlik.response.ServerResponseCode
import ru.nudlik.service.rest.RestClient
import ru.nudlik.service.rest.RestUtilService
import ru.nudlik.singleton.UserSingleton
import java.io.File
import java.lang.Exception

class ProfileActivity : BaseActivity() {
    private lateinit var progress: ProgressBar
    private lateinit var avatar: ImageView
    private lateinit var user: User
    private lateinit var collapsingToolbar: CollapsingToolbarLayout
    private lateinit var contentHolder: NestedScrollView
    private lateinit var profileFragment: ProfileContentFragment
    private lateinit var parentContainer: CoordinatorLayout
    private val requestMode = 1
    private val restUtil = RestUtilService(this@ProfileActivity)
    private var requestBuySubscription: Boolean = false
    private var goto: Int = -1

    private val targetImage = object : Target {
        override fun onPrepareLoad(placeHolderDrawable: Drawable?) {}
        override fun onBitmapFailed(e: Exception?, errorDrawable: Drawable?) {}
        override fun onBitmapLoaded(bitmap: Bitmap?, from: Picasso.LoadedFrom?) {
            bitmap?.apply {
                avatar.setImageBitmap(this)
                Palette.from(bitmap).generate { p ->
                    val lightVibrantSwatch = p?.lightVibrantSwatch
                    val vibrantSwatch = p?.vibrantSwatch
                    val darkVibrantSwatch = p?.darkVibrantSwatch
                    val lightMutedSwatch = p?.lightMutedSwatch
                    val mutedSwatch = p?.mutedSwatch
                    val darkMutedSwatch = p?.darkMutedSwatch

                    val current: Palette.Swatch? = when {
                        darkMutedSwatch != null -> darkMutedSwatch
                        mutedSwatch != null -> mutedSwatch
                        darkVibrantSwatch != null -> darkVibrantSwatch
                        lightMutedSwatch != null -> lightMutedSwatch
                        vibrantSwatch != null -> vibrantSwatch
                        else -> lightVibrantSwatch
                    }

                    current?.apply {
                        window?.statusBarColor = rgb
                        var lighter = lightenColor(hsl, 0.1f)
                        collapsingToolbar.setBackgroundColor(lighter)
                        collapsingToolbar.setContentScrimColor(lighter)
                        lighter = lightenColor(hsl, 0.65f)
                        contentHolder.animateBackgroundColor(lighter)
                    }
                }
            }
        }
    }

    private val gotoListener = object : FinishWithGoto {
        override fun goto(tag: Any?) {
            goto = tag as Int
            onBackPressed()
        }
    }

    private val subCreateCallback = object : RestUtilService.ResponseCallback {
        override fun <T> success(data: T?) {
            user.userSubscription = data as Subscription
            ProviderHelper.storeUserSubscription(data, contentResolver)
            profileFragment.subscriptionBuyed(data)
            SnackbarMaker.showShortSnackbar(parentContainer, R.string.subscription_buyed, R.string.ok)
        }

        override fun errorCode(responseCode: ServerResponseCode?) {
            ToastMaker.showShortToast(this@ProfileActivity, restUtil.getStringResponseRes(responseCode))
        }
    }

    private val buySubscriptionListener = object : BuySubscriptionListener {
        override fun buySub(price: Double) {
            val sub = Subscription()
            sub.userId = user.userId
            sub.activated = true
            sub.price = price

            restUtil.createSub(sub, subCreateCallback)
        }
    }

    private fun addFragment() {
        val transaction = supportFragmentManager.beginTransaction()
        transaction.add(R.id.fragment_user_profile, profileFragment, USER_CONTENT_FRAGMENT_TAG)
        transaction.commit()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_profile)

        user = UserSingleton.getUser()!!

        parentContainer = findViewById(R.id.profile_parent_container)
        val toolbar = findViewById<Toolbar>(R.id.toolbar_profile)
        setSupportActionBar(toolbar)
        collapsingToolbar = findViewById(R.id.collapsing_toolbar_profile)
        collapsingToolbar.title = user.email

        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setDisplayShowHomeEnabled(true)

        avatar = findViewById(R.id.toolbar_profile_image)
        avatar.setOnClickListener {
            pickFromGallery()
        }

        progress = findViewById(R.id.load_avatar_progress)

        contentHolder = findViewById(R.id.profile_nested_scroll_view)

        if (user.avatarUrl != null || user.avatarUrl!!.isNotEmpty()) {
            avatar.loadUri("${RestClient.URL}/resources/avatars/${user.userId}/${user.avatarUrl}", targetImage)
        }

        profileFragment = ProfileContentFragment.createInstance(user, gotoListener, buySubscriptionListener)

        requestBuySubscription = intent.extras?.getBoolean(REQUEST_BUY_SUBSCRIPTION) ?: false

        findViewById<MaterialButton>(R.id.user_logout).apply {
            setOnClickListener {
                AlertDialogBuilder.createInfoDialogNPActions(this@ProfileActivity, "Вы уверены?",
                    "При выходе все данные пользователя будут удалены",
                    DialogInterface.OnClickListener { d, _ ->
                        restUtil.logoutUser(user.userId!!, object : RestUtilService.ResponseCallback {
                            override fun <T> success(data: T?) {
                                UserSingleton.deleteUserInDb(contentResolver)
                                onBackPressed()
                            }
                            override fun errorCode(responseCode: ServerResponseCode?) {
                                ToastMaker.showShortToast(this@ProfileActivity, "Error: $responseCode")
                            }
                        })
                        d.dismiss()
                    }).show()
            }
        }

        addFragment()
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        if (item?.itemId == R.id.action_make_avatar) {
            pickFromGallery()
        }
        return super.onOptionsItemSelected(item)
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.profile_toolbar, menu)

        return super.onCreateOptionsMenu(menu)
    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }

    private fun pickFromGallery() {
        if (ActivityCompat.checkSelfPermission(
                this,
                Manifest.permission.READ_EXTERNAL_STORAGE
            ) != PackageManager.PERMISSION_GRANTED
        ) {
            requestPermission(Manifest.permission.READ_EXTERNAL_STORAGE,
                getString(R.string.permission_read_storage_rationale),
                BaseActivity.REQUEST_STORAGE_READ_ACCESS_PERMISSION
            )
        } else {
            UCropHelper.pickFromGallery(this@ProfileActivity, requestMode)
        }
    }

    private fun startCrop(uri: Uri) {
        UCropHelper.startCrop(uri,
            user.email?.substring(0, user.email!!.indexOf('@')) + "_avatar" + ".jpeg",
            this@ProfileActivity, arrayOf(1f, 1f))
    }

    private fun handleCropResult(result: Intent) {
        val resultUri = UCrop.getOutput(result)
        if (resultUri != null) {
            loadImageToServer(resultUri)
        } else {
            ToastMaker.showShortToast(this@ProfileActivity, R.string.toast_cannot_retrieve_cropped_image)
        }
    }

    private fun saveAvatar(url: String) {
        ProviderHelper.storeUserAvatar(contentResolver, url, user.userId!!)
    }

    private fun deleteCashFile(path: String?) {
        val file = File(path)
        file.delete()
    }

    private fun loadImageToServer(uri: Uri) {
        val file = File(uri.path)
        val requestFile = RequestBody.create(MediaType.parse("*/*"), file)
        val filePart = MultipartBody.Part.createFormData("file", file.name, requestFile)

        progress.visible(100)
        restUtil.postUserAvatar(user.userId!!, filePart, object : RestUtilService.ResponseCallback {
            override fun <T> success(data: T?) {
                val imageName = data as String
                if (user.avatarUrl == null || user.avatarUrl!!.isEmpty()) {
                    avatar.layoutParams = RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT,
                        RelativeLayout.LayoutParams.MATCH_PARENT)
                }
                user.avatarUrl = imageName
                UserSingleton.changeUser(user)
                saveAvatar(user.avatarUrl!!)
                avatar.reloadUri("${RestClient.URL}/resources/avatars/${user.userId}/${user.avatarUrl}", targetImage)

                deleteCashFile(uri.path)
                progress.gone(100)
            }

            override fun errorCode(responseCode: ServerResponseCode?) {
                progress.gone(100)
                ToastMaker.showLongToast(this@ProfileActivity, restUtil.getStringResponseRes(responseCode))
            }

            override fun exception(e: Throwable?) {
                progress.gone(100)
            }
        })
    }

    private fun handleCropError(result: Intent) {
        UCropHelper.handleCropError(this@ProfileActivity, result)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == requestMode) {
                val selectedUri = data!!.data
                if (selectedUri != null) {
                    startCrop(selectedUri)
                } else {
                    ToastMaker.showShortToast(this@ProfileActivity, R.string.toast_cannot_retrieve_selected_image)
                }
            } else if (requestCode == UCrop.REQUEST_CROP) {
                handleCropResult(data!!)
            }
        }
        if (resultCode == UCrop.RESULT_ERROR) {
            handleCropError(data!!)
        }
    }

    override fun onBackPressed() {
        if (requestBuySubscription && user.userSubscription != null) {
            val intent = Intent()
            intent.putExtra(SUBSCRIPTION, user.userSubscription)
            setResult(SUBSCRIPTION_BUEYED, intent)
        } else if (goto != -1) {
            setResult(goto)
        } else {
            setResult(Activity.RESULT_OK)
        }
        super.onBackPressed()
    }

    override fun onDestroy() {
        restUtil.destroy()
        super.onDestroy()
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String>, grantResults: IntArray) {
        when (requestCode) {
                REQUEST_STORAGE_READ_ACCESS_PERMISSION -> if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                pickFromGallery()
            }
            else -> super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        }
    }

    companion object {
        const val REQUEST_STORAGE_READ_ACCESS_PERMISSION = 101
        const val USER_CONTENT_FRAGMENT_TAG = "user_content_fragment_tag"
        const val SUBSCRIPTION_BUEYED = 1001
        const val SUBSCRIPTION = "subscription"
        const val REQUEST_BUY_SUBSCRIPTION = "request_buy_subscription"
    }
}