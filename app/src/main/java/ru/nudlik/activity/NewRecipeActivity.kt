package ru.nudlik.activity

import android.app.Activity
import android.content.DialogInterface
import android.content.Intent
import android.content.pm.PackageManager
import android.net.Uri
import android.os.Bundle
import androidx.appcompat.widget.Toolbar
import androidx.core.net.toFile
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentStatePagerAdapter
import androidx.viewpager.widget.ViewPager
import com.google.android.material.floatingactionbutton.ExtendedFloatingActionButton
import com.google.android.material.tabs.TabLayout
import com.yalantis.ucrop.UCrop
import okhttp3.MediaType
import okhttp3.MultipartBody
import okhttp3.RequestBody
import ru.nudlik.MainActivity
import ru.nudlik.R
import ru.nudlik.activity.ucrop.UCropHelper
import ru.nudlik.domain.*
import ru.nudlik.extentions.isValid
import ru.nudlik.fragment.newrecipe.NewRecipeContentFragment1
import ru.nudlik.fragment.newrecipe.NewRecipeContentFragment2
import ru.nudlik.fragment.newrecipe.NewRecipeContentFragment3
import ru.nudlik.fragment.newrecipe.NewRecipeContentFragment4
import ru.nudlik.fragment.dialog.AlertDialogBuilder
import ru.nudlik.fragment.dialog.LoadingDialogFragment
import ru.nudlik.fragment.toast.ToastMaker
import ru.nudlik.listener.FinishWithGoto
import ru.nudlik.provider.utils.ProviderHelper
import ru.nudlik.response.ServerResponseCode
import ru.nudlik.service.rest.RestUtilService
import ru.nudlik.singleton.CurrentActiveRecipe
import ru.nudlik.singleton.UserSingleton
import ru.nudlik.widget.PulseLayout
import java.io.File
import kotlin.collections.ArrayList

class NewRecipeActivity : BaseActivity(), ViewPager.OnPageChangeListener {
    private lateinit var recipe: Recipe
    private lateinit var user: User
    private lateinit var recipeContentFragment1: NewRecipeContentFragment1
    private lateinit var recipeContentFragment2: NewRecipeContentFragment2
    private lateinit var recipeContentFragment3: NewRecipeContentFragment3
    private lateinit var recipeContentFragment4: NewRecipeContentFragment4
    private lateinit var viewPager: ViewPager
    private lateinit var sectionPagerAdapter: SectionTabPagerAdapter
    private lateinit var restUtil: RestUtilService
    private lateinit var loadingDialog: LoadingDialogFragment
    private lateinit var addButton: ExtendedFloatingActionButton
    private lateinit var addPulse: PulseLayout

    private fun validate(): Boolean {
        return if (!recipeContentFragment1.validate()) {
            viewPager.setCurrentItem(0, true)
            false
        } else if (!recipeContentFragment2.validate()) {
            viewPager.setCurrentItem(1, true)
            false
        } else if (!recipeContentFragment3.validate()) {
            viewPager.setCurrentItem(2, true)
            false
        } else if (recipeContentFragment4.includeAd && !recipeContentFragment4.validate()) {
            viewPager.setCurrentItem(3, true)
            false
        }
        else true
    }

    private fun checkPostAdPermission(user: User?): Boolean {
        return user?.userSubscription?.isValid() ?: false
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(R.layout.activity_new_recipe)

        val toolbar = findViewById<Toolbar>(R.id.toolbar_new_recipe)
        toolbar.setTitle(R.string.add_new_recipe_title)
        setSupportActionBar(toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setDisplayShowHomeEnabled(true)

        user = UserSingleton.getUser()!!

        recipeContentFragment1 = NewRecipeContentFragment1.createInstance()
        recipeContentFragment2 = NewRecipeContentFragment2.createInstance()
        recipeContentFragment3 = NewRecipeContentFragment3.createInstance()
        recipeContentFragment4 = NewRecipeContentFragment4.createInstance(checkPostAdPermission(user))
        recipeContentFragment4.gotoBuySubscription = gotoListener

        sectionPagerAdapter = SectionTabPagerAdapter(supportFragmentManager)
        sectionPagerAdapter.addFragment(recipeContentFragment1, "Рецепт")
        sectionPagerAdapter.addFragment(recipeContentFragment2, "Ингридиенты")
        sectionPagerAdapter.addFragment(recipeContentFragment3, "Приготовление")
        sectionPagerAdapter.addFragment(recipeContentFragment4, "Реклама")

        viewPager = findViewById(R.id.add_recipe_viewpager)
        viewPager.addOnPageChangeListener(this@NewRecipeActivity)
        viewPager.offscreenPageLimit = 3
        viewPager.adapter = sectionPagerAdapter
        findViewById<TabLayout>(R.id.add_recipe_tablayout).apply {
            setupWithViewPager(viewPager)
        }

        loadingDialog = LoadingDialogFragment.createInstance("Загрузка рецепта")

        addButton = findViewById(R.id.add_new_recipe)
        addButton.setOnClickListener {
            if (validate()) {
                saveRecipe()
            }
        }

        recipe = Recipe(user.userId!!, user.email!!)

        restUtil = RestUtilService(this@NewRecipeActivity)

        addPulse = findViewById(R.id.add_new_action_pulse_layout)
    }

    override fun onPageScrollStateChanged(state: Int) {}

    override fun onPageScrolled(position: Int, positionOffset: Float, positionOffsetPixels: Int) {}

    override fun onPageSelected(position: Int) {
        when (position) {
            0,1 -> {
                addButton.text = "Далее"
            }
            2 -> {
                addButton.text = getString(R.string.add_new_recipe)
            }
        }
    }
    
    private fun initSearchIndex(r: Recipe) {
        val builder = StringBuilder()
        if (r.drinkType != null) {
            builder.append(r.drinkType).append(" ")
        }
        if (r.drinkState != null) {
            builder.append(r.drinkState).append(" ")
        }
        if (r.drinkTastes.size != 0) {
            for (taste in r.drinkTastes) {
                builder.append(taste).append(" ")
            }
        }
        if (r.alcohol) {
            builder.append("Alco").append(" ")
        }
        r.searchIndex = builder.toString()
    }

    private fun saveRecipe() {
        recipe.name = recipeContentFragment1.getName()
        recipe.secondaryName = recipeContentFragment1.getSecondaryName()
        recipe.drinkState = recipeContentFragment1.recipeDrinkState
        recipe.drinkTastes = recipeContentFragment1.recipeDrinkTastes
        recipe.drinkType = recipeContentFragment1.recipeDrinkType
        recipe.alcohol = recipeContentFragment1.alcohol
        recipe.ingredients = recipeContentFragment2.getRecipeIngredients()
        recipe.recipeSteps = recipeContentFragment3.getRecipeSteps()
        initSearchIndex(recipe)
        if (recipeContentFragment4.includeAd) {
            val ad = recipeContentFragment4.getRecipeAd()
            ad.stepPosition = if (recipe.recipeSteps.size == 1) 1 else recipe.recipeSteps.size / 2
            recipe.ad = ad
        } else {
            recipe.ad = null
        }
        loadingDialog.isCancelable = false
        loadingDialog.show(supportFragmentManager, LoadingDialogFragment.TAG)
        loadingDialog.setContent("Загрузка рецепта")
        restUtil.postRecipe(recipe, object : RestUtilService.ResponseCallback {
            override fun <T> success(data: T?) {
                recipe = data as Recipe
                saveRecipeImage()
            }

            override fun errorCode(responseCode: ServerResponseCode?) {
                loadingDialog.setContent("Ошибка создания рецепта: ${resources.getString(restUtil.getStringResponseRes(responseCode))}")
                loadingDialog.isCancelable = true
            }

            override fun exception(e: Throwable?) {
                loadingDialog.setContent("Ошибка создания рецепта")
                loadingDialog.isCancelable = true
            }
        })
    }

    private fun saveRecipeImage() {
        loadingDialog.setContent("Загрузка изображений")
        val requestFile = RequestBody.create(MediaType.parse("*/*"), recipeContentFragment1.mainImageFile!!)
        val filePart = MultipartBody.Part.createFormData("file",
            recipeContentFragment1.mainImageFile!!.name, requestFile)
        restUtil.postRecipeImage(recipe.recipeId!!, filePart, object : RestUtilService.ResponseCallback {
            override fun <T> success(data: T?) {
                recipe.imageUrls = data as ImageUrls
                saveRecipeStepImages()
            }
            override fun errorCode(responseCode: ServerResponseCode?) {
                loadingDialog.setContent("Ошибка загрузки изображения рецепта: ${resources.getString(restUtil.getStringResponseRes(responseCode))}")
                loadingDialog.isCancelable = true
            }

            override fun exception(e: Throwable?) {
                loadingDialog.setContent("Ошибка загрузки изображения рецепта")
                loadingDialog.isCancelable = true
            }
        })
    }

    private fun saveRecipeStepImages() {
        var i = recipe.recipeSteps.size
        val hash = recipeContentFragment3.getStepImagesHash()
        for (step in recipe.recipeSteps) {
            val file = File(hash.get(step.number))
            val requestFile = RequestBody.create(MediaType.parse("*/*"), file)
            val filePart = MultipartBody.Part.createFormData("file", file.name, requestFile)
            restUtil.postRecipeStepImage(recipe.recipeId!!, step.recipeStepCookId!!, filePart, object : RestUtilService.ResponseCallback {
                override fun <T> success(data: T?) {
                    step.imageUrl = data as String
                    step.recipeId = recipe.recipeId
                    if (--i == 0) {
                        if (recipe.ad != null) {
                            saveRecipeAdImage()
                        } else {
                            loadingDialog.dismiss()
                            openRecipePreview()
                        }
                    }
                }
                override fun errorCode(responseCode: ServerResponseCode?) {
                    loadingDialog.setContent("Ошибка загрузки изображения шага: ${resources.getString(restUtil.getStringResponseRes(responseCode))}")
                    loadingDialog.isCancelable = true
                }

                override fun exception(e: Throwable?) {
                    loadingDialog.setContent("Ошибка загрузки изображения приготовления")
                    loadingDialog.isCancelable = true
                }
            })
        }
    }

    private fun saveRecipeAdImage() {
        val requestFile = RequestBody.create(MediaType.parse("*/*"), recipeContentFragment4.adImageFile!!)
        val filePart = MultipartBody.Part.createFormData("file", recipeContentFragment4.adImageFile!!.name, requestFile)
        restUtil.postRecipeAdImage(recipe.recipeId!!, recipe.ad!!.adId!!, filePart, object : RestUtilService.ResponseCallback {
            override fun <T> success(data: T?) {
                recipe.ad!!.imageUrl = data as String
                loadingDialog.dismiss()
                openRecipePreview()
            }
            override fun errorCode(responseCode: ServerResponseCode?) {
                loadingDialog.setContent("Ошибка загрузки изображения рекламы: ${resources.getString(restUtil.getStringResponseRes(responseCode))}")
                loadingDialog.isCancelable = true
            }

            override fun exception(e: Throwable?) {
                loadingDialog.setContent("Ошибка загрузки изображения рекламы")
                loadingDialog.isCancelable = true
            }
        })
    }

    private val gotoListener = object : FinishWithGoto {
        override fun goto(tag: Any?) {
            if (tag == MainActivity.GOTO_PROFILE) {
                val intent = Intent(this@NewRecipeActivity, ProfileActivity::class.java)
                intent.putExtra(ProfileActivity.REQUEST_BUY_SUBSCRIPTION, true)
                startActivityForResult(intent, PROFILE_ACTIVITY_REQUEST)
            }
        }
    }

    private fun openRecipePreview() {
        val intent = Intent(this@NewRecipeActivity, RecipeViewActivity::class.java)
        CurrentActiveRecipe.recipe = recipe
        intent.putExtra(MainActivity.RECIPE_CREATION_FLAG, true)
        startActivityForResult(intent, RECIPE_PREVIEW_REQUEST)
    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }


    override fun onBackPressed() {
        AlertDialogBuilder.createInfoDialogNPActions(this@NewRecipeActivity, "Вы уверены?",
            "Все данные будут потеряны, прекратить добавление рецепта?",
            DialogInterface.OnClickListener { _, _ ->
                setResult(Activity.RESULT_CANCELED)
                finish()
            }).show()
    }

    private fun saveImage(path: Uri) {
        when (rCode) {
            REQUEST_STEP_IMAGE_MODE -> recipeContentFragment3.addResource(path.toFile())
            REQUEST_MAIN_IMAGE_MODE -> recipeContentFragment1.loadImage(path.toFile())
            REQUEST_AD_IMAGE_MODE -> recipeContentFragment4.loadImage(path.toFile())
        }
    }

    private fun startCrop(uri: Uri, filename: String) {
        UCropHelper.startCrop(uri,"$filename.jpeg",
            this@NewRecipeActivity, arrayOf(3f, 2f))
    }

    private fun handleCropResult(result: Intent) {
        val resultUri = UCrop.getOutput(result)
        if (resultUri != null) {
            saveImage(resultUri)
        } else {
            ToastMaker.showShortToast(this@NewRecipeActivity, R.string.toast_cannot_retrieve_cropped_image)
        }
    }

    private fun handleCropError(result: Intent) {
        UCropHelper.handleCropError(this@NewRecipeActivity, result)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == rCode) {
                val selectedUri = data!!.data
                if (selectedUri != null) {
                    when (rCode) {
                        REQUEST_STEP_IMAGE_MODE -> startCrop(selectedUri, recipeContentFragment3.stepFilename!!)
                        REQUEST_MAIN_IMAGE_MODE -> startCrop(selectedUri, "recipeMain")
                        REQUEST_AD_IMAGE_MODE -> startCrop(selectedUri, "recipeAd")
                    }
                } else {
                    ToastMaker.showShortToast(this@NewRecipeActivity, R.string.toast_cannot_retrieve_selected_image)
                }
            } else if (requestCode == UCrop.REQUEST_CROP) {
                handleCropResult(data!!)
            } else if (requestCode == RECIPE_PREVIEW_REQUEST) {
                val recipeId = recipe.recipeId
                UserSingleton.getUser()?.ownRecipes?.add(recipeId!!)
                ProviderHelper.addOwnRecipe(recipeId!!, contentResolver)
                setResult(Activity.RESULT_OK)
                finish()
            }
        }
        if (resultCode == UCrop.RESULT_ERROR) {
            handleCropError(data!!)
        }
        if (resultCode == RecipeViewActivity.ACTIVATE_RESULT_CANCELED) {
            restUtil.deleteRecipe(recipe.recipeId!!, user.userId!!, object : RestUtilService.ResponseCallback {
                override fun <T> success(data: T?) {
                    recipe.recipeId = null
                    recipe.date = null
                    recipe.ad = null
                }
            })
        }
        if (resultCode == ProfileActivity.SUBSCRIPTION_BUEYED) {
            user.userSubscription = data?.extras?.get(ProfileActivity.SUBSCRIPTION) as Subscription
            recipeContentFragment4.updateSubscription(checkPostAdPermission(user))
        }
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String>, grantResults: IntArray) {
        when (requestCode) {
            ProfileActivity.REQUEST_STORAGE_READ_ACCESS_PERMISSION -> if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                when (rCode) {
                    REQUEST_STEP_IMAGE_MODE -> recipeContentFragment3.pickFromGallery()
                    REQUEST_MAIN_IMAGE_MODE -> recipeContentFragment1.pickFromGallery()
                    REQUEST_AD_IMAGE_MODE -> recipeContentFragment4.pickFromGallery()
                }
            }
            else -> super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        }
    }

    override fun onResume() {
        super.onResume()
        addPulse.start()
    }

    override fun onPause() {
        super.onPause()
        addPulse.stop()
    }

    override fun onDestroy() {
        if (loadingDialog.isVisible) {
            loadingDialog.dismiss()
        }
        restUtil.destroy()
        super.onDestroy()
    }

    companion object {
        const val REQUEST_STEP_IMAGE_MODE = 1022
        const val REQUEST_MAIN_IMAGE_MODE = 1021
        const val RECIPE_PREVIEW_REQUEST = 2112
        const val REQUEST_AD_IMAGE_MODE = 1023
        const val PROFILE_ACTIVITY_REQUEST = 3021
        var rCode: Int = -1
    }


    class SectionTabPagerAdapter(fragmentManager: FragmentManager) : FragmentStatePagerAdapter(fragmentManager, BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT) {
        private val fragments = ArrayList<Fragment>(3)
        private val titles = ArrayList<String>(3)

        fun addFragment(fragment: Fragment, title: String) {
            fragments.add(fragment)
            titles.add(title)
        }

        override fun getPageTitle(position: Int): CharSequence? = titles[position]

        override fun getItem(position: Int): Fragment = fragments[position]

        override fun getCount(): Int = fragments.size
    }
}