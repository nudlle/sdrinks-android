package ru.nudlik.activity.ucrop

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.graphics.Bitmap
import android.net.Uri
import androidx.core.content.ContextCompat
import com.yalantis.ucrop.UCrop
import ru.nudlik.R
import ru.nudlik.fragment.toast.ToastMaker
import java.io.File

interface UCropHelper {
    companion object {
        fun pickFromGallery(activity: Activity, requestMode: Int) {
            val intent = Intent(Intent.ACTION_GET_CONTENT)
                .setType("image/*")
                .addCategory(Intent.CATEGORY_OPENABLE)

            val mimeTypes = arrayOf("image/jpeg", "image/png")
            intent.putExtra(Intent.EXTRA_MIME_TYPES, mimeTypes)

            activity.startActivityForResult(Intent.createChooser(intent, activity.getString(R.string.title_select_picture)), requestMode)
        }

        fun startCrop(uri: Uri, filename: String, activity: Activity, aspectRatio: Array<Float>) {
            var uCrop = UCrop.of(uri, Uri.fromFile(File(activity.cacheDir, filename)))

            uCrop = basisConfig(uCrop, aspectRatio)
            uCrop = advancedConfig(uCrop, activity)


            uCrop.start(activity)
        }

        fun handleCropError(context: Context, result: Intent) {
            val cropError = UCrop.getError(result)
            if (cropError != null) {
                ToastMaker.showLongToast(context, cropError.message)
            } else {
                ToastMaker.showLongToast(context, R.string.unexpected_error)
            }
        }

        private fun basisConfig(uCrop: UCrop, aspectRatio: Array<Float>): UCrop {
            return uCrop.withAspectRatio(aspectRatio[0], aspectRatio[1])
        }

        private fun advancedConfig(uCrop: UCrop, context: Context): UCrop {
            val options = UCrop.Options()
            options.setCompressionFormat(Bitmap.CompressFormat.JPEG)

            options.setHideBottomControls(false)
            options.setCropGridColumnCount(0)
            options.setCropGridRowCount(0)
            options.setToolbarColor(ContextCompat.getColor(context, R.color.colorPrimary))
            options.setStatusBarColor(ContextCompat.getColor(context, R.color.colorPrimaryDark))
            options.setToolbarWidgetColor(ContextCompat.getColor(context, android.R.color.white))

            return uCrop.withOptions(options)
        }
    }
}