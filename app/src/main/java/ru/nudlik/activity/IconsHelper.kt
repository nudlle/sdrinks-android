package ru.nudlik.activity

import android.app.Activity
import android.view.View
import android.widget.ImageView
import ru.nudlik.R
import ru.nudlik.domain.DrinkState
import ru.nudlik.domain.DrinkTaste
import ru.nudlik.domain.DrinkType
import ru.nudlik.domain.Recipe
import ru.nudlik.extentions.gone
import ru.nudlik.extentions.visible
import ru.nudlik.fragment.toast.ToastMaker

class IconsHelper {
    private var iconLemonade: ImageView
    private var iconTeaCoffee: ImageView
    private var iconShot: ImageView
    private var iconCocktail: ImageView
    private var iconMilkshake: ImageView
    private var iconLiqueur: ImageView
    private var iconBerries: ImageView
    private var iconFruit: ImageView
    private var iconVegetable: ImageView
    private var iconSweet: ImageView
    private var iconSour: ImageView
    private var iconSpicy: ImageView
    private var iconHerb: ImageView
    private var iconHot: ImageView
    private var iconCold: ImageView
    private var iconAlcohol: ImageView
    private var recipe: Recipe

    constructor(activity: Activity, recipe: Recipe) {
        iconLemonade = activity.findViewById(R.id.recipe_type_ico_lemonade)
        iconTeaCoffee = activity.findViewById(R.id.recipe_type_ico_tea_coffee)
        iconShot = activity.findViewById(R.id.recipe_type_ico_shot)
        iconCocktail = activity.findViewById(R.id.recipe_type_ico_cocktail)
        iconMilkshake = activity.findViewById(R.id.recipe_type_ico_milkshake)
        iconLiqueur = activity.findViewById(R.id.recipe_type_ico_liqueur)
        iconBerries = activity.findViewById(R.id.recipe_taste_ico_berries)
        iconFruit = activity.findViewById(R.id.recipe_taste_ico_fruit)
        iconVegetable = activity.findViewById(R.id.recipe_taste_ico_vegetable)
        iconSweet = activity.findViewById(R.id.recipe_taste_ico_sweet)
        iconSour = activity.findViewById(R.id.recipe_taste_ico_sour)
        iconSpicy = activity.findViewById(R.id.recipe_taste_ico_spicy)
        iconHerb = activity.findViewById(R.id.recipe_taste_ico_herb)
        iconHot = activity.findViewById(R.id.recipe_state_ico_hot)
        iconCold = activity.findViewById(R.id.recipe_state_ico_cold)
        iconAlcohol = activity.findViewById(R.id.recipe_type_ico_alco)
        this.recipe = recipe
        initialize()
    }

    constructor(view: View, recipe: Recipe) {
        iconLemonade = view.findViewById(R.id.recipe_type_ico_lemonade)
        iconTeaCoffee = view.findViewById(R.id.recipe_type_ico_tea_coffee)
        iconShot = view.findViewById(R.id.recipe_type_ico_shot)
        iconCocktail = view.findViewById(R.id.recipe_type_ico_cocktail)
        iconMilkshake = view.findViewById(R.id.recipe_type_ico_milkshake)
        iconLiqueur = view.findViewById(R.id.recipe_type_ico_liqueur)
        iconBerries = view.findViewById(R.id.recipe_taste_ico_berries)
        iconFruit = view.findViewById(R.id.recipe_taste_ico_fruit)
        iconVegetable = view.findViewById(R.id.recipe_taste_ico_vegetable)
        iconSweet = view.findViewById(R.id.recipe_taste_ico_sweet)
        iconSour = view.findViewById(R.id.recipe_taste_ico_sour)
        iconSpicy = view.findViewById(R.id.recipe_taste_ico_spicy)
        iconHerb = view.findViewById(R.id.recipe_taste_ico_herb)
        iconHot = view.findViewById(R.id.recipe_state_ico_hot)
        iconCold = view.findViewById(R.id.recipe_state_ico_cold)
        iconAlcohol = view.findViewById(R.id.recipe_type_ico_alco)
        this.recipe = recipe
        initialize()
    }

    fun initialize() {
        iconLemonade.setOnClickListener {
            ToastMaker.showShortToast(it.context, "Лимонад")
        }
        iconTeaCoffee.setOnClickListener {
            ToastMaker.showShortToast(it.context, "Чай/кофе")
        }
        iconShot.setOnClickListener {
            ToastMaker.showShortToast(it.context, "Шот")
        }
        iconCocktail.setOnClickListener {
            ToastMaker.showShortToast(it.context, "Коктейль")
        }
        iconMilkshake.setOnClickListener {
            ToastMaker.showShortToast(it.context, "Молочный коктейль")
        }
        iconLiqueur.setOnClickListener {
            ToastMaker.showShortToast(it.context, "Настойка")
        }
        iconBerries.setOnClickListener {
            ToastMaker.showShortToast(it.context, "Ягодный")
        }
        iconFruit.setOnClickListener {
            ToastMaker.showShortToast(it.context, "Фруктовый")
        }
        iconVegetable.setOnClickListener {
            ToastMaker.showShortToast(it.context, "Овощной")
        }
        iconSweet.setOnClickListener {
            ToastMaker.showShortToast(it.context, "Сладкий")
        }
        iconSour.setOnClickListener {
            ToastMaker.showShortToast(it.context, "Кислый")
        }
        iconSpicy.setOnClickListener {
            ToastMaker.showShortToast(it.context, "Острый")
        }
        iconHerb.setOnClickListener {
            ToastMaker.showShortToast(it.context, "Травяной")
        }
        iconHot.setOnClickListener {
            ToastMaker.showShortToast(it.context, "Горячий")
        }
        iconCold.setOnClickListener {
            ToastMaker.showShortToast(it.context, "Холодный")
        }
        iconAlcohol.setOnClickListener {
            ToastMaker.showShortToast(it.context, "Алкогольный")
        }
        checkType(recipe.drinkType)

        recipe.drinkTastes?.forEach{ taste ->
            checkTaste(taste)
        }
        checkState(recipe.drinkState)
        checkAlco(recipe.alcohol)
    }

    private fun show(imageView: ImageView) {
        imageView.visible()
    }

    private fun checkType(type: DrinkType?) {
        when (type) {
            DrinkType.Lemonade -> show(iconLemonade)
            DrinkType.Tea, DrinkType.Coffee -> show(iconTeaCoffee)
            DrinkType.Shot -> show(iconShot)
            DrinkType.Cocktail -> show(iconCocktail)
            DrinkType.Milkshake -> show(iconMilkshake)
            DrinkType.Liqueur -> show(iconLiqueur)
        }
    }

    private fun checkTaste(taste: DrinkTaste) {
        when (taste) {
            DrinkTaste.Berries -> show(iconBerries)
            DrinkTaste.Fruit -> show(iconFruit)
            DrinkTaste.Vegetable -> show(iconVegetable)
            DrinkTaste.Tasty -> show(iconSweet)
            DrinkTaste.Sour -> show(iconSour)
            DrinkTaste.Spicy -> show(iconSpicy)
            DrinkTaste.Herb -> show(iconHerb)
        }
    }

    private fun checkState(state: DrinkState?) {
        when (state) {
            DrinkState.Hot -> show(iconHot)
            DrinkState.Cold -> show(iconCold)
            else -> {
                iconHot.gone()
                iconCold.gone()
            }
        }
    }

    private fun checkAlco(isAlco: Boolean) {
        if (isAlco) {
            iconAlcohol.visible()
        } else {
            iconAlcohol.gone()
        }
    }
}