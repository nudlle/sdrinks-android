package ru.nudlik.activity

import android.app.Activity
import android.content.Intent
import android.graphics.Bitmap
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import ru.nudlik.MainActivity
import ru.nudlik.domain.Recipe
import ru.nudlik.R
import ru.nudlik.service.rest.RestClient
import android.graphics.PorterDuff
import android.graphics.drawable.Drawable
import android.graphics.drawable.LayerDrawable
import android.net.Uri
import android.view.View
import android.widget.*
import androidx.coordinatorlayout.widget.CoordinatorLayout
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentStatePagerAdapter
import androidx.palette.graphics.Palette
import androidx.viewpager.widget.ViewPager
import com.google.android.material.appbar.SubtitleCollapsingToolbar
import com.google.android.material.floatingactionbutton.ExtendedFloatingActionButton
import com.google.android.material.floatingactionbutton.FloatingActionButton
import com.squareup.picasso.Picasso
import com.squareup.picasso.Target
import ru.nudlik.domain.User
import ru.nudlik.domain.UserComment
import ru.nudlik.extentions.*
import ru.nudlik.fragment.RecipeCommentsFragment
import ru.nudlik.fragment.RecipePreviewContentFragment
import ru.nudlik.fragment.dialog.FeedbackDialogFragment
import ru.nudlik.fragment.snackbar.SnackbarMaker
import ru.nudlik.fragment.toast.ToastMaker
import ru.nudlik.interfaces.DialogButtonsListener
import ru.nudlik.interfaces.RequestNextPageListener
import ru.nudlik.listener.ItemClickListener
import ru.nudlik.provider.utils.ProviderHelper
import ru.nudlik.response.ServerResponseCode
import ru.nudlik.service.rest.RestUtilService
import ru.nudlik.singleton.CurrentActiveRecipe
import ru.nudlik.singleton.UserSingleton
import ru.nudlik.widget.PulseLayout
import java.lang.Exception


class RecipeViewActivity : AppCompatActivity() {
    private lateinit var recipe: Recipe
    private lateinit var restClient: RestUtilService
    private lateinit var imageView: ImageView
    private lateinit var ratingBar: RatingBar
    private lateinit var ratesHolder: LinearLayout
    private lateinit var iconsHolder: LinearLayout
    private lateinit var collapsingToolbar: SubtitleCollapsingToolbar
    private lateinit var actionFabsHolder: LinearLayout
    private lateinit var rateCount: TextView
    private lateinit var iconsHelper: IconsHelper
    private lateinit var likeFab: FloatingActionButton
    private lateinit var shareFab: FloatingActionButton
    private lateinit var feedbackFab: FloatingActionButton
    private lateinit var savePulse: PulseLayout
    private lateinit var feedbackPulse: PulseLayout
    private lateinit var sharePulse: PulseLayout
    private lateinit var likePulse: PulseLayout
    private lateinit var mainContainer: CoordinatorLayout
    private lateinit var viewPager: ViewPager
    private var commentsFragment: RecipeCommentsFragment? = null
    private var creation: Boolean = true
    private var user: User? = null
    private var postComment: Boolean = false
    private var recipePos: Int? = null
    private var recipeExtPos: Int? = null
    
    private val commentPosActionListener = object : DialogButtonsListener {
        override fun positiveClick(ext: Any?) {
            feedbackPulse.gone(1000)
            feedbackPulse.stop()
            val comment = ext as UserComment
            recipe.rate += comment.rate!!
            recipe.commentCount++
            calcRates()
            postComment = true
            commentsFragment?.addItem(comment)
            SnackbarMaker.showShortSnackbar(mainContainer, R.string.recipe_comment_success_added, R.string.ok)
        }
    }

    private val loadNextCommentsRequest = object : RequestNextPageListener {
        override fun loadNextPage(page: Int, size: Int, callback: RestUtilService.ResponseCallback) {
            restClient.getCommentsByRecipeId(recipe.recipeId!!, page, size, callback)
        }
    }

    private val loadCommentsCallback = object : RestUtilService.ResponseCallback {
        override fun <T> success(data: T?) {
            if (data is List<*>) {
                val l = data.filterIsInstance<UserComment>()
                if (l.isNotEmpty()) {
                    commentsFragment?.addItems(l)
                    commentsFragment?.checkEmptyData()
                }
            }
        }
    }

    private fun loadComments() {
        restClient.getCommentsByRecipeId(recipe.recipeId!!, 0, 36, loadCommentsCallback)
    }

    private val targetImage = object : Target {
        override fun onPrepareLoad(placeHolderDrawable: Drawable?) {}
        override fun onBitmapFailed(e: Exception?, errorDrawable: Drawable?) {}
        override fun onBitmapLoaded(bitmap: Bitmap?, from: Picasso.LoadedFrom?) {
            bitmap?.apply {
                imageView.setImageBitmap(this)
                Palette.from(bitmap).generate { p ->
                    val lightVibrantSwatch = p?.lightVibrantSwatch
                    val vibrantSwatch = p?.vibrantSwatch
                    val darkVibrantSwatch = p?.darkVibrantSwatch
                    val lightMutedSwatch = p?.lightMutedSwatch
                    val mutedSwatch = p?.mutedSwatch
                    val darkMutedSwatch = p?.darkMutedSwatch

                    val current: Palette.Swatch? = when {
                        darkMutedSwatch != null -> darkMutedSwatch
                        mutedSwatch != null -> mutedSwatch
                        darkVibrantSwatch != null -> darkVibrantSwatch
                        lightMutedSwatch != null -> lightMutedSwatch
                        vibrantSwatch != null -> vibrantSwatch
                        else -> lightVibrantSwatch
                    }

                    current?.apply {
                        window?.statusBarColor = rgb
                        var lighter = lightenColor(hsl, 0.1f)
                        collapsingToolbar.setBackgroundColor(lighter)
                        collapsingToolbar.setContentScrimColor(lighter)
                        lighter = lightenColor(hsl, 0.65f)
                        mainContainer.animateBackgroundColor(lighter)
                    }
                }
            }
        }
    }

    private fun checkShowFabs() {
        if ((user != null && user!!.likedRecipes.contains(recipe.recipeId)) ||
            ProviderHelper.isLikedRecipe(recipe.recipeId!!, contentResolver)) {
            likePulse.gone()
            likePulse.stop()
        }
        if (user == null || recipe.userId == user!!.userId!!) {
            feedbackPulse.gone()
            feedbackPulse.stop()
        } else {
            restClient.isCommentPosted(recipe.recipeId!!, user!!.userId!!, object : RestUtilService.ResponseCallback {
                override fun <T> success(data: T?) {
                    if (data == ServerResponseCode.COMMENT_ALREADY_POSTED) {
                        feedbackPulse.gone()
                        feedbackPulse.stop()
                    }
                }
            })
        }
    }

    private fun incView() {
        if (recipe.userId != user?.userId && !creation) {
            restClient.incView(recipe.recipeId!!, object : RestUtilService.ResponseCallback {
                override fun <T> success(data: T?) {
                    recipe.viewed++
                }
            })
        }
    }

    private fun incAdClicked() {
        if (recipe.userId != user?.userId && !creation) {
            restClient.incClick(recipe.ad!!.adId!!, object : RestUtilService.ResponseCallback {
                override fun <T> success(data: T?) {
                    recipe.ad!!.clicked++
                }
            })
        }
    }

    private fun calcRates() {
        ratingBar.rating = (recipe.rate / recipe.commentCount).toFloat()
        rateCount.apply {
            text = if (recipe.rate == 0.0) "(нет отзывов)" else "(${recipe.commentCount})"
        }
    }

    private val adClickListener = object : ItemClickListener {
        override fun onItemClick(v: View?, position: Int, ext: Any?) {
            incAdClicked()
            val intent = Intent(Intent.ACTION_VIEW, Uri.parse(ext as String))
            val chooser = Intent.createChooser(intent, null)
            startActivity(chooser)
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_recipe_preview)

        user = UserSingleton.getUser()

        recipe = CurrentActiveRecipe.recipe!!
        creation = intent?.extras?.getBoolean(MainActivity.RECIPE_CREATION_FLAG)!!
        recipePos = intent?.extras?.getInt(MainActivity.RECIPE_POSITION_FLAG)
        recipeExtPos = intent?.extras?.getInt(MainActivity.RECIPE_POSITION_EXT_FLAG)
        val toolbar = findViewById<Toolbar>(R.id.toolbar_recipe_preview)
        setSupportActionBar(toolbar)
        collapsingToolbar = findViewById(R.id.collapsing_toolbar_recipe_preview)
        collapsingToolbar.title = recipe.name
        collapsingToolbar.subtitle = recipe.secondaryName ?: ""

        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setDisplayShowHomeEnabled(true)

        mainContainer = findViewById(R.id.recipe_preview_parent_container)

        val author = "опубликовано ${recipe.userEmail?.substring(0, recipe.userEmail?.indexOf('@')?.plus(1) ?: 0)}"
        findViewById<TextView>(R.id.recipe_author_email)?.apply {
            text = author
        }

        ratesHolder = findViewById(R.id.recipe_rates_holder)
        ratesHolder.setOnClickListener {
            if (!creation) {
                viewPager.currentItem = 1
            }
        }
        iconsHelper = IconsHelper(this@RecipeViewActivity, recipe)
        ratingBar = findViewById(R.id.recipe_rates)
        val stars = ratingBar.progressDrawable as LayerDrawable
        stars.getDrawable(0).setColorFilter(getColor(R.color.colorAccent), PorterDuff.Mode.SRC_ATOP)
        stars.getDrawable(1).setColorFilter(getColor(R.color.colorAccent), PorterDuff.Mode.SRC_ATOP)
        stars.getDrawable(2).setColorFilter(getColor(R.color.colorAccent), PorterDuff.Mode.SRC_ATOP)
        rateCount = findViewById(R.id.recipe_rating_count)

        iconsHolder = findViewById(R.id.recipe_icons_holder)

        imageView = findViewById(R.id.toolbar_recipe_preview_image)
        imageView.apply {
            loadUri("${RestClient.URL}/resources/recipes/${recipe.recipeId}/${recipe.imageUrls.large}", targetImage)
        }

        val save = findViewById<ExtendedFloatingActionButton>(R.id.confirm_new_recipe)
        save.apply {
            setOnClickListener {
                restClient.activateRecipe(recipe.recipeId!!, recipe.userId!!, object : RestUtilService.ResponseCallback {
                    override fun <T> success(data: T?) {
                        setResult(Activity.RESULT_OK)
                        finish()
                    }

                    override fun errorCode(responseCode: ServerResponseCode?) {
                        ToastMaker.showShortToast(this@RecipeViewActivity,
                            restClient.getStringResponseRes(responseCode))
                    }
                })
            }
        }

        actionFabsHolder = findViewById(R.id.recipe_action_fabs_holder)
        likeFab = findViewById(R.id.like_recipe_fab)
        likeFab.setOnClickListener {
            //TODO update recipe like count if in own fragment
            ProviderHelper.addLikedRecipe(recipe.recipeId!!, contentResolver)
            SnackbarMaker.showShortSnackbar(mainContainer, R.string.recipe_add_to_liked, R.string.ok)
            likePulse.gone(1000)
            likePulse.stop()
            if (user != null) {
                restClient.addLikeRecipe(user!!.userId!!, recipe.recipeId!!, object : RestUtilService.ResponseCallback {
                    override fun <T> success(data: T?) {
                        user!!.likedRecipes.add(recipe.recipeId!!)
                    }
                })
            }
        }
        shareFab = findViewById(R.id.share_recipe_fab)
        shareFab.setOnClickListener {
            val intent = Intent(Intent.ACTION_SEND)
            intent.type = "text/plain"
            intent.putExtra(Intent.EXTRA_TEXT,
                "Попробуйте приготовить https://somehost.ru/recipes/${recipe.recipeId}")
            startActivity(Intent.createChooser(intent, "Поделиться"))
        }
        feedbackFab = findViewById(R.id.feedback_recipe_fab)
        feedbackFab.setOnClickListener {
            val feedbackDialog = FeedbackDialogFragment.createInstance(recipe.recipeId!!, user!!.userId!!,
                user!!.email!!, restClient)
            feedbackDialog.buttonsClickListener = commentPosActionListener
            feedbackDialog.show(supportFragmentManager, "FeedbackDialogFrag")
        }

        restClient = RestUtilService(this@RecipeViewActivity)

        savePulse = findViewById(R.id.save_action_pulse_layout)

        feedbackPulse = findViewById(R.id.feedback_action_pulse_layout)
        sharePulse = findViewById(R.id.share_action_pulse_layout)
        likePulse = findViewById(R.id.like_action_pulse_layout)

        val sections = SectionPagerAdapter(supportFragmentManager)
        val recipeContent = RecipePreviewContentFragment.createInstance(recipe)
        recipeContent.adClickListener = adClickListener
        sections.addFragment(recipeContent)
        viewPager = findViewById(R.id.recipe_preview_view_pager)


        if (!creation) {
            save.gone()
            calcRates()
            commentsFragment = RecipeCommentsFragment.createInstance(recipe.recipeId!!, loadNextCommentsRequest)
            sections.addFragment(commentsFragment!!)
            viewPager.offscreenPageLimit = 2
            loadComments()
        } else {
            actionFabsHolder.gone()
            viewPager.offscreenPageLimit = 1
        }

        viewPager.adapter = sections
        checkShowFabs()
        incView()
    }

    override fun onResume() {
        super.onResume()
        if (!creation) {
            feedbackPulse.start()
            sharePulse.start()
            likePulse.start()
        } else {
            savePulse.start()
        }
    }

    override fun onPause() {
        super.onPause()
        feedbackPulse.stop()
        sharePulse.stop()
        likePulse.stop()
        savePulse.stop()
    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }

    override fun onBackPressed() {
        if (creation) {
            setResult(ACTIVATE_RESULT_CANCELED)
            finish()
//            AlertDialogBuilder.createInfoDialogNPActions(this@RecipePreviewActivity, "Вы уверены?",
//                "Все данные будут потеряны, прекратить добавление рецепта?",
//                DialogInterface.OnClickListener { _, _ ->
//                    restClient.deleteRecipe(recipe.recipeId!!, user!!.userId!!, object : RestUtilService.ResponseCallback {
//                        override fun <T> success(data: T?) {
//                            setResult(ACTIVATE_RESULT_CANCELED)
//                            finish()
//                        }
//                        override fun errorCode(responseCode: ServerResponseCode?) {
//                            ToastMaker.showShortToast(this@RecipePreviewActivity, responseCode?.name)
//                        }
//                        override fun exception(e: Throwable?) {
//                            ToastMaker.showShortToast(this@RecipePreviewActivity, e?.message)
//                        }
//                    })
//
//                }).show()
        } else {
            if (viewPager.currentItem == 1) {
                viewPager.currentItem = 0
            } else {
                if (postComment) {
                    val intent = Intent()
                    CurrentActiveRecipe.recipe = recipe
                    intent.putExtra(MainActivity.RECIPE_POSITION_FLAG, recipePos)
                    intent.putExtra(MainActivity.RECIPE_POSITION_EXT_FLAG, recipeExtPos)
                    setResult(POST_COMMENT_RESULT, intent)
                }
                finish()
            }
        }
    }

    override fun onDestroy() {
        restClient.destroy()
        super.onDestroy()
    }

    companion object {
        const val POST_COMMENT_RESULT = 5511
        const val ACTIVATE_RESULT_CANCELED = 3301
    }

    class SectionPagerAdapter(fragmentManager: FragmentManager) : FragmentStatePagerAdapter(fragmentManager, BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT) {
        private val fragments = ArrayList<Fragment>(3)
        fun addFragment(fragment: Fragment) {
            fragments.add(fragment)
        }
        override fun getItem(position: Int): Fragment = fragments[position]
        override fun getCount(): Int = fragments.size
    }
}