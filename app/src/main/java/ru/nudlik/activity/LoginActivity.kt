package ru.nudlik.activity

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.util.Patterns
import android.view.View
import android.widget.LinearLayout
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.core.widget.ContentLoadingProgressBar
import com.google.android.material.button.MaterialButton
import com.google.android.material.textfield.TextInputLayout
import ru.nudlik.MainActivity
import ru.nudlik.R
import ru.nudlik.domain.User
import ru.nudlik.extentions.gone
import ru.nudlik.extentions.invisible
import ru.nudlik.extentions.visible
import ru.nudlik.listener.EditTextErrorCleaner
import ru.nudlik.provider.utils.ProviderHelper
import ru.nudlik.response.ServerResponseCode
import ru.nudlik.security.SessionToken
import ru.nudlik.security.UserRequest
import ru.nudlik.service.rest.RestUtilService
import ru.nudlik.singleton.SingletonInitializer
import ru.nudlik.singleton.UserSingleton

class LoginActivity : AppCompatActivity() {
    private var state: State = State.LOGIN
    private lateinit var email: TextInputLayout
    private lateinit var confirmPwd: TextInputLayout
    private lateinit var pwd: TextInputLayout
    private lateinit var emailCheck: TextInputLayout
    private lateinit var loginButtonsHolder: LinearLayout
    private lateinit var registerButton: MaterialButton
    private lateinit var loginButton: MaterialButton
    private lateinit var recoverButton: MaterialButton
    private lateinit var confirmButton: MaterialButton
    private lateinit var loginText: TextView
    private lateinit var registerText: TextView
    private lateinit var recoverText: TextView
    private lateinit var progressBar: ContentLoadingProgressBar
    private lateinit var editTextHolder: LinearLayout
    private val restClient: RestUtilService = RestUtilService(this@LoginActivity)

    private fun showLoading(show: Boolean) {
        if (show) {
            editTextHolder.visibility = View.GONE
            progressBar.visibility = View.VISIBLE
            progressBar.show()
        } else {
            progressBar.hide()
            progressBar.visibility = View.GONE
            editTextHolder.visibility = View.VISIBLE
        }
    }
    private fun actionState(newState: State) {
        when (state) {
            State.LOGIN -> {
                when (newState) {
                    State.RECOVER -> {
                        state = newState
                        recoverButton.backgroundTintList = getColorStateList(R.color.colorAccent)
                        loginButton.backgroundTintList = getColorStateList(android.R.color.transparent)
                        recoverText.visible(200)
                        registerButton.gone(200)
                        loginText.gone(200)
                        pwd.invisible(200)
                        pwd.error = null
                        pwd.editText?.setText("")
                        confirmPwd.error = null
                        confirmPwd.editText?.setText("")
                    }
                    State.REGISTER -> {
                        state = newState
                        registerButton.backgroundTintList = getColorStateList( R.color.colorAccent)
                        registerText.visible(200)
                        confirmPwd.visible(200)
                        loginText.gone(200)
                        loginButtonsHolder.gone(200)
                        pwd.error = null
                        pwd.editText?.setText("")
                    }
                    else -> {
                        makeLoginAction()
                    }
                }
            }
            State.RECOVER -> {
                when (newState) {
                    State.RECOVER -> {
                        makeRecoverAction()
                    }
                    else -> {
                        state = newState
                        loginButton.backgroundTintList = getColorStateList(R.color.colorAccent)
                        recoverButton.backgroundTintList = getColorStateList(android.R.color.transparent)
                        recoverText.gone(200)
                        loginText.visible(200)
                        pwd.visible(200)
                        registerButton.visible(200)
                    }
                }
            }
            State.REGISTER -> {
                makeRegisterAction()
            }
            State.REGISTER_CHECK_EMAIL -> {
                if (valid()) {
                    val user = User()
                    user.email = email.editText?.text.toString()
                    user.password = pwd.editText?.text.toString()
                    val userRequest = UserRequest()
                    userRequest.user = user
                    userRequest.timestamp = System.currentTimeMillis()
                    showLoading(true)
                    confirmButton.gone()
                    restClient.registerUser(email.editText?.text.toString(),
                        emailCheck.editText?.text.toString(), userRequest, object: RestUtilService.ResponseCallback {
                            override fun <T> success(data: T?) {
                                if (data is User) {
                                    loginUser(data as User)
                                }
                            }
                            override fun errorCode(responseCode: ServerResponseCode?) {
                                emailCheck.error = resources.getString(restClient.getStringResponseRes(responseCode))
                                showLoading(false)
                                confirmButton.visible()
                            }

                            override fun exception(e: Throwable?) {
                                super.exception(e)
                                showLoading(false)
                                confirmButton.visible()
                            }
                        })
                }
            }
            State.RECOVER_CHECK_EMAIL -> {
                confirmRecoverAction()
            }
        }
    }

    private fun validateEmail(): Boolean {
        return when {
            email.editText?.text!!.isEmpty() -> {
                email.error = resources.getString(R.string.not_be_empty)
                false
            }
            email.editText?.text!!.length > 30 -> {
                email.error = resources.getString(R.string.max_length_error)
                false
            }
            !Patterns.EMAIL_ADDRESS.matcher(email.editText!!.text.toString()).matches() -> {
                email.error = resources.getString(R.string.invalid_email_error)
                false
            }
            else -> {
                true
            }
        }
    }
    private fun validatePwd(): Boolean {
        return when {
            pwd.editText?.text!!.isEmpty() -> {
                pwd.error = resources.getString(R.string.not_be_empty)
                false
            }
            pwd.editText?.text!!.length > 20 -> {
                pwd.error = resources.getString(R.string.max_length_error)
                false
            }
            else -> {
                true
            }
        }
    }
    private fun validateConfirmPwd(): Boolean {
        return when {
            confirmPwd.editText?.text!!.isEmpty() -> {
                confirmPwd.error = resources.getString(R.string.not_be_empty)
                false
            }
            confirmPwd.editText?.text!!.length > 20 -> {
                confirmPwd.error = resources.getString(R.string.max_length_error)
                false
            }
            confirmPwd.editText?.text.toString() != pwd.editText?.text.toString() -> {
                confirmPwd.error = resources.getString(R.string.password_not_match_error)
                false
            }
            else -> {
                true
            }
        }
    }

    private fun validateCheckEmailHash(): Boolean {
        return when {
            emailCheck.editText?.text!!.isEmpty() -> {
                emailCheck.error = resources.getString(R.string.not_be_empty)
                false
            }
            else -> {
                true
            }
        }
    }

    private fun valid(): Boolean {
        return when (state) {
            State.LOGIN -> {
                validateEmail() && validatePwd()
            }
            State.RECOVER -> {
                validateEmail()
            }
            State.RECOVER_CHECK_EMAIL -> {
                validateCheckEmailHash() && validatePwd() && validateConfirmPwd()
            }
            State.REGISTER -> {
                validateEmail() && validatePwd() && validateConfirmPwd()
            }
            State.REGISTER_CHECK_EMAIL -> {
                validateCheckEmailHash()
            }
        }
    }

    private fun makeLoginAction() {
        if (valid()) {
            loginButtonsHolder.gone()
            showLoading(true)
            val user = User()
            user.email = email.editText?.text.toString()
            user.password = pwd.editText?.text.toString()
            val userRequest = UserRequest()
            userRequest.user = user
            userRequest.timestamp = System.currentTimeMillis()
            restClient.loginWithPassword(userRequest, object : RestUtilService.ResponseCallback {
                override fun <T> success(data: T?) {
                    if (data is User) {
                        loginUser(data as User)
                    }
                }

                override fun errorCode(responseCode: ServerResponseCode?) {
                    showLoading(false)
                    loginButtonsHolder.visible()
                    if (responseCode == ServerResponseCode.LOGIN_FAILED) {
                        pwd.error = resources.getString(restClient.getStringResponseRes(responseCode))
                    } else {
                        email.error = resources.getString(restClient.getStringResponseRes(responseCode))
                    }
                }

                override fun exception(e: Throwable?) {
                    super.exception(e)
                    showLoading(false)
                    loginButtonsHolder.visible()
                }
            })
        }
    }

    private fun makeRecoverAction() {
        if (valid()) {
            loginButtonsHolder.gone()
            showLoading(true)
            restClient.checkEmailOnRecoverRequest(email.editText?.text.toString(), object : RestUtilService.ResponseCallback {
                override fun <T> success(data: T?) {
                    state = State.RECOVER_CHECK_EMAIL
                    showLoading(false)
                    emailCheck.visible(200)
                    pwd.visible(200)
                    confirmPwd.visible(200)
                    confirmButton.visible(200)
                    email.gone()
                }

                override fun errorCode(responseCode: ServerResponseCode?) {
                    showLoading(false)
                    loginButtonsHolder.visible()
                    email.error = resources.getString(restClient.getStringResponseRes(responseCode))
                }

                override fun exception(e: Throwable?) {
                    super.exception(e)
                    showLoading(false)
                    loginButtonsHolder.visible()
                }
            })
        }
    }

    private fun makeRegisterAction() {
        if (valid()) {
            showLoading(true)
            registerButton.gone(200)
            restClient.checkEmailOnRegisterRequest(email.editText?.text.toString(),
                object: RestUtilService.ResponseCallback {
                    override fun <T> success(data: T?) {
                        showLoading(false)
                        state = State.REGISTER_CHECK_EMAIL
                        confirmButton.visible(200)
                        emailCheck.visible(200)
                        email.gone()
                        pwd.gone()
                        confirmPwd.gone()
                    }

                    override fun errorCode(responseCode: ServerResponseCode?) {
                        showLoading(false)
                        registerButton.visible(200)
                        email.error = resources.getString(restClient.getStringResponseRes(responseCode))
                        state = State.REGISTER
                    }

                    override fun exception(e: Throwable?) {
                        super.exception(e)
                        showLoading(false)
                        registerButton.visible(200)
                        state = State.REGISTER
                    }
                })
        }
    }

    private fun confirmRecoverAction() {
        if (valid()) {
            showLoading(true)
            confirmButton.gone()
            val user = User()
            user.email = email.editText?.text.toString()
            user.password = confirmPwd.editText?.text.toString()
            val userRequest = UserRequest()
            userRequest.user = user
            userRequest.timestamp = System.currentTimeMillis()
            restClient.recoverWithEmail(emailCheck.editText?.text.toString(), userRequest,
                object : RestUtilService.ResponseCallback {
                    override fun <T> success(data: T?) {
                        loginUser(data as User)
                    }

                    override fun errorCode(responseCode: ServerResponseCode?) {
                        showLoading(false)
                        confirmButton.visible(200)
                        emailCheck.visible(200)
                        pwd.visible(200)
                        confirmPwd.visible(200)
                        emailCheck.error = resources.getString(restClient.getStringResponseRes(responseCode))
                    }

                    override fun exception(e: Throwable?) {
                        super.exception(e)
                        showLoading(false)
                        confirmButton.visible(200)
                        emailCheck.visible(200)
                        pwd.visible(200)
                        confirmPwd.visible(200)
                    }

                })
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)

        val toolbar = findViewById<Toolbar>(R.id.login_toolbar)
        setSupportActionBar(toolbar)

        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setDisplayShowHomeEnabled(true)
        supportActionBar?.setDisplayShowTitleEnabled(false)

        email = findViewById(R.id.email_input_layout)
        email.editText?.addTextChangedListener(EditTextErrorCleaner(email))
        pwd = findViewById(R.id.password_input_layout)
        pwd.editText?.addTextChangedListener(EditTextErrorCleaner(pwd))
        confirmPwd = findViewById(R.id.confirm_password_input_layout)
        confirmPwd.editText?.addTextChangedListener(EditTextErrorCleaner(confirmPwd))
        emailCheck = findViewById(R.id.email_check_input_layout)
        emailCheck.editText?.addTextChangedListener(EditTextErrorCleaner(emailCheck))

        loginButtonsHolder = findViewById(R.id.login_buttons_holder)

        loginText = findViewById(R.id.login_title)
        registerText = findViewById(R.id.register_title)
        recoverText = findViewById(R.id.recover_title)

        loginButton = findViewById(R.id.login_button)
        loginButton.backgroundTintList = getColorStateList(R.color.colorAccent)
        loginButton.setOnClickListener {
            actionState(State.LOGIN)
        }
        recoverButton = findViewById(R.id.recover_button)
        recoverButton.setOnClickListener {
            actionState(State.RECOVER)
        }
        registerButton = findViewById(R.id.register_button)
        registerButton.setOnClickListener {
            actionState(State.REGISTER)
        }

        confirmButton = findViewById(R.id.confirm_recover_button)
        confirmButton.setOnClickListener {
            actionState(state)
        }

        progressBar = findViewById(R.id.login_progress_bar)
        editTextHolder = findViewById(R.id.edittext_holder)
    }

    private fun loginUser(user: User) {
        val userLogin = User()
        userLogin.userId = user.userId
        userLogin.password = pwd.editText?.text.toString()
        val userRequest = UserRequest(userLogin, System.currentTimeMillis())
        restClient.loginUser(userRequest, object : RestUtilService.ResponseCallback {
            override fun <T> success(data: T?) {
                val st = data as SessionToken
                SingletonInitializer.addCredentials(st.id!!, st.token!!)
                saveUser(user)
            }
        })
    }

    private fun saveUser(user: User) {
        user.password = pwd.editText?.text.toString()
        UserSingleton.initUser(user)
        UserSingleton.storeUserInDb(contentResolver)
        setResult(Activity.RESULT_OK)
        finish()
    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }

    override fun onBackPressed() {
        when (state) {
            State.REGISTER -> {
                state = State.LOGIN
                registerButton.backgroundTintList = getColorStateList(android.R.color.transparent)
                registerText.gone(200)
                confirmPwd.gone(200)
                loginText.visible(200)
                loginButtonsHolder.visible(200)
                pwd.error = null
                pwd.editText?.setText("")
                confirmPwd.error = null
                confirmPwd.editText?.setText("")
            }
            State.RECOVER -> {
                state = State.LOGIN
                recoverButton.backgroundTintList = getColorStateList(android.R.color.transparent)
                loginButton.backgroundTintList = getColorStateList(R.color.colorAccent)
                recoverText.gone(200)
                loginText.visible(200)
                pwd.visible(200)
                registerButton.visible(200)
            }
            State.REGISTER_CHECK_EMAIL -> {
                state = State.REGISTER
                emailCheck.editText?.setText("")
                emailCheck.error = null
                emailCheck.gone(200)
                confirmButton.gone(200)
                email.visible(200)
                pwd.visible(200)
                confirmPwd.visible(200)
                registerButton.visible(200)
            }
            State.RECOVER_CHECK_EMAIL -> {
                state = State.RECOVER
                email.visible(200)
                loginButtonsHolder.visible(200)
                emailCheck.editText?.setText("")
                emailCheck.error = null
                emailCheck.gone()
                pwd.editText?.setText("")
                pwd.error = null
                pwd.invisible()
                confirmPwd.editText?.setText("")
                confirmPwd.error = null
                confirmPwd.gone()
                confirmButton.gone()

            }
            else -> {
                setResult(Activity.RESULT_CANCELED)
                super.onBackPressed()
            }
        }
    }

    enum class State {
        LOGIN, RECOVER, RECOVER_CHECK_EMAIL, REGISTER, REGISTER_CHECK_EMAIL
    }
}