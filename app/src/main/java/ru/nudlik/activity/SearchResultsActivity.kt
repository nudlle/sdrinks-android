package ru.nudlik.activity

import android.app.SearchManager
import android.content.Intent
import android.os.Bundle
import android.view.MotionEvent
import android.view.View
import android.widget.FrameLayout
import android.widget.RelativeLayout
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.constraintlayout.widget.ConstraintLayout
import com.google.android.material.appbar.CollapsingToolbarLayout
import ru.nudlik.MainActivity
import ru.nudlik.domain.Recipe
import ru.nudlik.domain.RecipesToSearch
import ru.nudlik.fragment.toast.ToastMaker
import ru.nudlik.response.ServerResponseCode
import ru.nudlik.service.rest.RestUtilService
import ru.nudlik.singleton.SearchAddOnsSingleton
import ru.nudlik.R
import ru.nudlik.extentions.gone
import ru.nudlik.extentions.visible
import ru.nudlik.fragment.RecipeSearchFragment
import ru.nudlik.interfaces.RequestNextPageListener
import ru.nudlik.listener.ItemClickListener
import ru.nudlik.singleton.CurrentActiveRecipe
import ru.nudlik.utils.SplashAnimUtil

class SearchResultsActivity : AppCompatActivity() {
    private lateinit var restUtilService: RestUtilService
    private lateinit var searchFragment: RecipeSearchFragment
    private var searchRequest: RecipesToSearch? = null
    private lateinit var fragmentContainer: FrameLayout
    private lateinit var parentContainer: ConstraintLayout
    private lateinit var progressHolder: RelativeLayout

    private val loadRequest = object : RequestNextPageListener {
        override fun loadNextPage(page: Int, size: Int, callback: RestUtilService.ResponseCallback) {
            restUtilService.searchRecipes(searchRequest!!, page, size, callback)
        }
    }

    private val recipeItemClickListener = object : ItemClickListener {
        override fun onItemClick(v: View?, position: Int, ext: Any?) {
            showRecipeActivity(ext as Recipe, position)
        }
    }

    private fun showRecipeActivity(recipe: Recipe?, position: Int) {
        val intent = Intent(this@SearchResultsActivity, RecipeViewActivity::class.java)
        intent.putExtra(MainActivity.RECIPE_CREATION_FLAG, false)
        CurrentActiveRecipe.recipe = recipe
        intent.putExtra(MainActivity.RECIPE_POSITION_FLAG, position)
        SplashAnimUtil.viewLoading(false, fragmentContainer, parentContainer, progressHolder)
        startActivityForResult(intent, MainActivity.VIEW_RECIPE_REQUEST)
    }

    override fun dispatchTouchEvent(ev: MotionEvent?): Boolean {
        if (ev != null && ev.action == MotionEvent.ACTION_DOWN) {
            SplashAnimUtil.x = ev.x.toInt()
            SplashAnimUtil.y = ev.y.toInt() - 100
        }
        return super.dispatchTouchEvent(ev)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(R.layout.activity_search)

        val toolbar = findViewById<Toolbar>(R.id.toolbar_search)
        toolbar.setTitle(R.string.label_search_result)
        setSupportActionBar(toolbar)
        val collapsingToolbar = findViewById<CollapsingToolbarLayout>(R.id.collapsing_toolbar_search)
        collapsingToolbar.isTitleEnabled = false

        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setDisplayShowHomeEnabled(true)

        restUtilService = RestUtilService(this@SearchResultsActivity)

        fragmentContainer = findViewById(R.id.search_fragment_container)
        progressHolder = findViewById(R.id.search_progress_holder)
        parentContainer = findViewById(R.id.search_parent_container)
        searchFragment = RecipeSearchFragment.createInstance(recipeItemClickListener, loadRequest)
        supportFragmentManager.beginTransaction().replace(R.id.search_fragment_container,
            searchFragment, RecipeSearchFragment.TAG).commit()
        showLoading(true)

        handleIntent(intent)
    }

    override fun onNewIntent(intent: Intent?) {
        super.onNewIntent(intent)
        handleIntent(intent)
    }

    private fun showLoading(loading: Boolean) {
        if (loading) {
            fragmentContainer.gone()
            progressHolder.visible()
        } else {
            progressHolder.gone()
            fragmentContainer.visible()
        }
    }

    private fun handleIntent(intent: Intent?) {
        if (Intent.ACTION_SEARCH == intent?.action) {
            val query = intent.getStringExtra(SearchManager.QUERY)
            searchRequest = RecipesToSearch()
            val index =  SearchAddOnsSingleton.getTextIndex()
            index.append(query)
            searchRequest!!.search = index.toString()

            restUtilService.searchRecipes(searchRequest!!, 0, 30, object : RestUtilService.ResponseCallback {
                override fun <T> success(data: T?) {
                    if (data is List<*>) {
                        val ll = data.filterIsInstance(Recipe::class.java)
                        searchFragment.addNewData(ll)
                        searchFragment.checkEmptyData()
                        showLoading(false)
                    }
                }

                override fun errorCode(responseCode: ServerResponseCode?) {
                    showLoading(false)
                    ToastMaker.showShortToast(this@SearchResultsActivity, restUtilService.getStringResponseRes(responseCode))
                }

                override fun exception(e: Throwable?) {
                    showLoading(false)
                    ToastMaker.showShortToast(this@SearchResultsActivity, e?.message)
                }
            })
        }
    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        SplashAnimUtil.viewLoading(true, fragmentContainer, parentContainer, progressHolder)
        when (requestCode) {
            MainActivity.VIEW_RECIPE_REQUEST -> {
                if (resultCode == RecipeViewActivity.POST_COMMENT_RESULT) {
                    val recipe = CurrentActiveRecipe.recipe
                    val pos = data?.extras?.getInt(MainActivity.RECIPE_POSITION_FLAG) ?: -1
                    if (pos >= 0 && recipe != null) {
                        searchFragment.updateAt(recipe as Recipe, pos)
                    }
                }
            }
        }
    }

    override fun onDestroy() {
        restUtilService.destroy()
        super.onDestroy()
    }
}