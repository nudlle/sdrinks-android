package ru.nudlik.extentions

import android.animation.Animator
import android.animation.ArgbEvaluator
import android.animation.ValueAnimator
import android.app.Activity
import android.content.Context
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.AnimationUtils
import android.widget.ImageView
import androidx.core.graphics.ColorUtils
import androidx.recyclerview.widget.RecyclerView
import com.squareup.picasso.Picasso
import com.squareup.picasso.Target
import ru.nudlik.R
import ru.nudlik.domain.Subscription
import java.io.File
import java.text.SimpleDateFormat
import java.util.*
import kotlin.math.max
import kotlin.math.min

fun ViewGroup.inflate(layoutRes: Int): View {
    return LayoutInflater.from(context).inflate(layoutRes, this, false)
}
fun ImageView.loadRes(imageRes: Int) {
    Picasso.get()
        .load(imageRes)
        .into(this)
}
fun ImageView.loadVectorDrawable(imageRes: Int) {
    Picasso.get()
        .load(imageRes)
        .placeholder(imageRes)
        .into(this)
}
fun ImageView.loadUri(uri: String?) {
    Picasso.get().load(uri)
        .error(R.drawable.ic_error_outline_black_24dp)
        .into(this)
}
fun ImageView.loadUri(uri: String?, target: Target) {
    Picasso.get().load(uri)
        .error(R.drawable.ic_error_outline_black_24dp)
        .into(target)
}
fun ImageView.loadPath(path: File) {
    Picasso.get().load(path)
        .error(R.drawable.ic_error_outline_black_24dp)
        .into(this)
}
fun ImageView.reloadPath(path: File) {
    Picasso.get().invalidate(path)
    Picasso.get().load(path)
        .error(R.drawable.ic_error_outline_black_24dp)
        .into(this)
}
fun ImageView.reloadUri(uri: String) {
    Picasso.get().invalidate(uri)
    Picasso.get().load(uri)
        .error(R.drawable.ic_error_outline_black_24dp)
        .into(this)
}
fun ImageView.reloadUri(uri: String, target: Target) {
    Picasso.get().invalidate(uri)
    Picasso.get().load(uri)
        .error(R.drawable.ic_error_outline_black_24dp)
        .into(target)
}
fun Date.toTextDate(pattern: String): String {
    val df = SimpleDateFormat(pattern, Locale.getDefault())
    return df.format(this)
}
fun Subscription.isValid(): Boolean {
    return (endDate?.after(Date()) ?: false) && activated
}
fun String.toDate(pattern: String): Date {
    return SimpleDateFormat(pattern, Locale.getDefault()).parse(this)
}
fun View.visible(animDuration: Long = 0) {
    if (animDuration > 0) {
        animate().alpha(1f).setDuration(animDuration).setListener(object : Animator.AnimatorListener {
            override fun onAnimationRepeat(animation: Animator?) {}
            override fun onAnimationEnd(animation: Animator?) {
                visibility = View.VISIBLE
                animate().setListener(null)
            }
            override fun onAnimationStart(animation: Animator?) {}

            override fun onAnimationCancel(animation: Animator?) {
                animate().setListener(null)
            }
        }).start()
    } else {
        visibility = View.VISIBLE
    }
}
fun View.gone(animDuration: Long = 0) {
    if (animDuration > 0) {
        animate().alpha(0f).setDuration(animDuration).setListener(object : Animator.AnimatorListener {
            override fun onAnimationRepeat(animation: Animator?) {}
            override fun onAnimationEnd(animation: Animator?) {
                visibility = View.GONE
                animate().setListener(null)
            }
            override fun onAnimationStart(animation: Animator?) {}

            override fun onAnimationCancel(animation: Animator?) {
                animate().setListener(null)
            }
        }).start()
    } else {
        visibility = View.GONE
    }
}
fun View.invisible(animDuration: Long = 0) {
    if (animDuration > 0) {
        animate().alpha(0f).setDuration(animDuration).setListener(object : Animator.AnimatorListener {
            override fun onAnimationRepeat(animation: Animator?) {}
            override fun onAnimationEnd(animation: Animator?) {
                visibility = View.INVISIBLE
                animate().setListener(null)
            }
            override fun onAnimationStart(animation: Animator?) {}

            override fun onAnimationCancel(animation: Animator?) {
                animate().setListener(null)
            }
        }).start()
    } else {
        visibility = View.INVISIBLE
    }
}
fun View.animateBackgroundColor(newColor: Int, duration: Long = 500, startDelay: Long = 150) {
    var colorFrom = Color.TRANSPARENT
    if (background is ColorDrawable) {
        colorFrom = (background as ColorDrawable).color
    }
    val colorAnimation = ValueAnimator.ofObject(ArgbEvaluator(), colorFrom, newColor)
    colorAnimation.duration = duration
    colorAnimation.startDelay = startDelay
    colorAnimation.addUpdateListener {
        setBackgroundColor(it.animatedValue as Int)
    }
    colorAnimation.start()
}
fun RecyclerView.runLayoutAnimation(resAnim: Int) {
    layoutAnimation = AnimationUtils.loadLayoutAnimation(context, resAnim)
    adapter?.notifyItemChanged(0)
    scheduleLayoutAnimation()
}
fun View.isVisible(): Boolean {
    return visibility == View.VISIBLE
}
fun Context.finish() {
    if (this is Activity) {
        finish()
    }
}
fun lightenColor(hsl: FloatArray, value: Float) : Int {
    hsl[2] += value
    hsl[2] = max(0f, min(hsl[2], 1f))
    return ColorUtils.HSLToColor(hsl)
}
fun darkenColor(hsl: FloatArray, value: Float) : Int {
    hsl[2] -= value
    hsl[2] = max(0f, min(hsl[2], 1f))
    return ColorUtils.HSLToColor(hsl)
}