package ru.nudlik.service.rest

import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import okhttp3.MultipartBody
import retrofit2.http.*
import ru.nudlik.domain.*
import ru.nudlik.response.*
import ru.nudlik.security.UserRequest

interface RestClient {
    @GET("recipes/all/{page}/{size}")
    fun getRecipes(@Path("page") page: Int,
                   @Path("size") size: Int): Single<List<RecipeResponse>>
    @GET("recipes/by-id/{recipeId}")
    fun getRecipeById(@Path("recipeId") recipeId: String): Single<RecipeResponse>
    @GET("recipes/by-type/{type}/{page}/{size}")
    fun getRecipesByType(@Path("type") recipeType: DrinkType,
                         @Path("page") page: Int,
                         @Path("size") size: Int): Single<List<RecipeResponse>>
    @GET("recipes/by-user-email/{email}/{page}/{size}")
    fun getRecipesByUserEmail(@Path("email") email: String,
                         @Path("page") page: Int,
                         @Path("size") size: Int): Single<List<RecipeResponse>>
    @PUT("recipes/se/{page}/{size}")
    fun searchRecipes(@Body searchRecipes: RecipesToSearch,
                      @Path("page") page: Int,
                      @Path("size") size: Int): Single<List<RecipeResponse>>
    @POST("recipes/create")
    fun createRecipe(@Body recipe: Recipe): Single<RecipeResponse>
    @POST("recipes/activate/{recipeId}/{userId}")
    fun activateRecipe(@Path("recipeId") recipeId: String, @Path("userId") userId: String): Single<ServerResponseCode>
    @DELETE("recipes/delete/{recipeId}/{userId}")
    fun deleteRecipe(@Path("recipeId") recipeId: String, @Path("userId") userId: String): Single<ServerResponseCode>
    @PUT("recipes/inc/view/{recipeId}")
    fun incRecipeViewed(@Path("recipeId") recipeId: String): Single<ServerResponseCode>
    @PUT("recipes/inc/click/{adId}")
    fun incRecipeAdClicked(@Path("adId") adId: String): Single<ServerResponseCode>
    @POST("users/liked-recipe/{userId}/{recipeId}")
    fun addLikedRecipe(@Path("userId") userId: String,
                         @Path("recipeId") recipeId: String): Single<ServerResponseCode>
    @DELETE("users/liked-recipe/{userId}/{recipeId}")
    fun deleteLikedRecipe(@Path("userId") userId: String,
                             @Path("recipeId") recipeId: String): Single<ServerResponseCode>

    @POST("auth/login")
    fun login(@Body userRequest: UserRequest): Single<SessionTokenResponse>
    @POST("auth/logout/{userId}")
    fun logout(@Path("userId") userId: String): Single<ServerResponseCode>
    @POST("auth/register/{email}/{emailHash}")
    fun register(@Path("email") email: String,
                 @Path("emailHash") emailHash: String,
                 @Body user: UserRequest): Single<UserResponse>
    @GET("auth/check-email/{email}")
    fun checkEmail(@Path("email") email: String): Single<ServerResponseCode>
    @POST("auth/recover")
    fun getUserByPassword(@Body user: UserRequest): Single<UserResponse>
    @GET("auth/recover-request/{email}")
    fun recoverEmailHashRequest(@Path("email") email: String): Single<ServerResponseCode>
    @POST("auth/recover/{hashEmail}")
    fun recoverByEmail(@Path("hashEmail") hashEmail: String,
                       @Body user: UserRequest): Single<UserResponse>
    @POST("comments/create")
    fun createComment(@Body userComment: UserComment): Single<CommentResponse>
    @GET("comments/{recipeId}/{page}/{size}")
    fun getCommentsOfRecipe(@Path("recipeId") recipeId: String,
                            @Path("page") page: Int,
                            @Path("size") size: Int): Single<List<CommentResponse>>
    @GET("comments/check/{recipeId}/{userId}")
    fun isCommentAlreadyPosted(@Path("recipeId") recipeId: String,
                               @Path("userId") userId: String): Single<ServerResponseCode>
    @Multipart
    @POST("uploads/recipe-image/{recipeId}")
    fun uploadRecipeImage(@Path("recipeId") recipeId: String,
                          @Part file: MultipartBody.Part): Single<ImageUrlsResponse>
    @Multipart
    @POST("uploads/recipe-step/{recipeId}/{stepId}")
    fun uploadRecipeStepImage(@Path("recipeId") recipeId: String,
                              @Path("stepId") stepId: String,
                              @Part file: MultipartBody.Part): Single<StringResponse>
    @Multipart
    @POST("uploads/recipe-ad/{recipeId}/{adId}")
    fun uploadRecipeAdImage(@Path("recipeId") recipeId: String,
                            @Path("adId") adId: String,
                            @Part file: MultipartBody.Part): Single<StringResponse>
    @Multipart
    @POST("uploads/user-avatar/{userId}")
    fun uploadUserAvatar(@Path("userId") userId: String,
                         @Part file: MultipartBody.Part): Single<StringResponse>
    @POST("user-messages/create")
    fun postAppErrorMessage(@Body userMessage: UserMessage): Single<UserMessageResponse>
    @POST("subscriptions/create-sub")
    fun createSub(@Body sub: Subscription): Single<SubscriptionResponse>
    @GET("users/user/{userId}")
    fun getUser(@Path("userId") userId: String): Single<UserResponse>

    companion object Factory {
//        const val URL = "https://192.168.0.101"
        const val URL = "https://192.168.43.74:8443"

        fun getRecipesImpl(restClient: RestClient?, page: Int, size: Int): Single<List<RecipeResponse>>? {
            return restClient?.getRecipes(page, size)
                ?.observeOn(AndroidSchedulers.mainThread())
                ?.subscribeOn(Schedulers.io())
        }

        fun getRecipeByIdImpl(restClient: RestClient?, recipeId: String): Single<RecipeResponse>? {
            return restClient?.getRecipeById(recipeId)
                ?.observeOn(AndroidSchedulers.mainThread())
                ?.subscribeOn(Schedulers.io())
        }

        fun getRecipesByTypeImpl(restClient: RestClient?, recipeType: DrinkType, page: Int, size: Int): Single<List<RecipeResponse>>? {
            return restClient?.getRecipesByType(recipeType, page, size)
                ?.observeOn(AndroidSchedulers.mainThread())
                ?.subscribeOn(Schedulers.io())
        }

        fun getRecipesByUserEmailImpl(restClient: RestClient?, email: String, page: Int,
                                      size: Int): Single<List<RecipeResponse>>? {
            return restClient?.getRecipesByUserEmail(email, page, size)
                ?.observeOn(AndroidSchedulers.mainThread())
                ?.subscribeOn(Schedulers.io())
        }

        fun searchRecipesImpl(restClient: RestClient?, searchRecipes: RecipesToSearch, page: Int, size: Int): Single<List<RecipeResponse>>? {
            return restClient?.searchRecipes(searchRecipes, page, size)
                ?.observeOn(AndroidSchedulers.mainThread())
                ?.subscribeOn(Schedulers.io())
        }

        fun createRecipeImpl(restClient: RestClient?, recipe: Recipe): Single<RecipeResponse>? {
            return restClient?.createRecipe(recipe)
                ?.observeOn(AndroidSchedulers.mainThread())
                ?.subscribeOn(Schedulers.io())
        }

        fun activateRecipeImpl(restClient: RestClient?, recipeId: String, userId: String): Single<ServerResponseCode>? {
            return restClient?.activateRecipe(recipeId, userId)
                ?.observeOn(AndroidSchedulers.mainThread())
                ?.subscribeOn(Schedulers.io())
        }

        fun incRecipeViewedImpl(restClient: RestClient?, recipeId: String): Single<ServerResponseCode>? {
            return restClient?.incRecipeViewed(recipeId)
                ?.observeOn(AndroidSchedulers.mainThread())
                ?.subscribeOn(Schedulers.io())
        }

        fun incRecipeAdClickedImpl(restClient: RestClient?, adId: String): Single<ServerResponseCode>? {
            return restClient?.incRecipeAdClicked(adId)
                ?.observeOn(AndroidSchedulers.mainThread())
                ?.subscribeOn(Schedulers.io())
        }

        fun deleteRecipeImpl(restClient: RestClient?, recipeId: String, userId: String): Single<ServerResponseCode>? {
            return restClient?.deleteRecipe(recipeId, userId)
                ?.observeOn(AndroidSchedulers.mainThread())
                ?.subscribeOn(Schedulers.io())
        }
        fun loginImpl(restClient: RestClient?, userRequest: UserRequest): Single<SessionTokenResponse>? {
            return restClient?.login(userRequest)
                ?.observeOn(AndroidSchedulers.mainThread())
                ?.subscribeOn(Schedulers.io())
        }

        fun logoutImpl(restClient: RestClient?, userId: String): Single<ServerResponseCode>? {
            return restClient?.logout(userId)
                ?.observeOn(AndroidSchedulers.mainThread())
                ?.subscribeOn(Schedulers.io())
        }

        fun registerImpl(restClient: RestClient?, email: String, emailHash: String, user: UserRequest): Single<UserResponse>? {
            return restClient?.register(email, emailHash, user)
                ?.observeOn(AndroidSchedulers.mainThread())
                ?.subscribeOn(Schedulers.io())
        }

        fun checkEmailImpl(restClient: RestClient?, email: String): Single<ServerResponseCode>? {
            return restClient?.checkEmail(email)
                ?.observeOn(AndroidSchedulers.mainThread())
                ?.subscribeOn(Schedulers.io())
        }

        fun loginByPasswordImpl(restClient: RestClient?, user: UserRequest): Single<UserResponse>? {
            return restClient?.getUserByPassword(user)
                ?.observeOn(AndroidSchedulers.mainThread())
                ?.subscribeOn(Schedulers.io())
        }

        fun recoverEmailHashRequestImpl(restClient: RestClient?, email: String): Single<ServerResponseCode>? {
            return restClient?.recoverEmailHashRequest(email)
                ?.observeOn(AndroidSchedulers.mainThread())
                ?.subscribeOn(Schedulers.io())
        }

        fun recoverByEmailImpl(restClient: RestClient?, hashEmail: String, user: UserRequest): Single<UserResponse>? {
            return restClient?.recoverByEmail(hashEmail, user)
                ?.observeOn(AndroidSchedulers.mainThread())
                ?.subscribeOn(Schedulers.io())
        }

        fun createCommentImpl(restClient: RestClient?, userComment: UserComment): Single<CommentResponse>? {
            return restClient?.createComment(userComment)
                ?.observeOn(AndroidSchedulers.mainThread())
                ?.subscribeOn(Schedulers.io())
        }

        fun getCommentsOfRecipeImpl(restClient: RestClient?, recipeId: String, page: Int, size: Int): Single<List<CommentResponse>>? {
            return restClient?.getCommentsOfRecipe(recipeId, page, size)
                ?.observeOn(AndroidSchedulers.mainThread())
                ?.subscribeOn(Schedulers.io())
        }

        fun isCommentAlreadyPostedImpl(restClient: RestClient?, recipeId: String, userId: String): Single<ServerResponseCode>? {
            return restClient?.isCommentAlreadyPosted(recipeId, userId)
                ?.observeOn(AndroidSchedulers.mainThread())
                ?.subscribeOn(Schedulers.io())
        }
        fun uploadRecipeImageImpl(restClient: RestClient?, recipeId: String, file: MultipartBody.Part): Single<ImageUrlsResponse>? {
            return restClient?.uploadRecipeImage(recipeId, file)
                ?.observeOn(AndroidSchedulers.mainThread())
                ?.subscribeOn(Schedulers.io())
        }
        fun uploadRecipeStepImageImpl(restClient: RestClient?, recipeId: String, stepId: String,
                                      file: MultipartBody.Part): Single<StringResponse>? {
            return restClient?.uploadRecipeStepImage(recipeId, stepId, file)
                ?.observeOn(AndroidSchedulers.mainThread())
                ?.subscribeOn(Schedulers.io())
        }

        fun uploadRecipeAdImageImpl(restClient: RestClient?, recipeId: String, adId: String,
                                    file: MultipartBody.Part): Single<StringResponse>? {
            return restClient?.uploadRecipeAdImage(recipeId, adId, file)
                ?.observeOn(AndroidSchedulers.mainThread())
                ?.subscribeOn(Schedulers.io())
        }
        fun uploadUserAvatarImpl(restClient: RestClient?, userId: String, file: MultipartBody.Part): Single<StringResponse>? {
            return restClient?.uploadUserAvatar(userId, file)
                ?.observeOn(AndroidSchedulers.mainThread())
                ?.subscribeOn(Schedulers.io())
        }
        fun addLikedRecipeImpl(restClient: RestClient?, userId: String,
                               recipeId: String): Single<ServerResponseCode>? {
            return restClient?.addLikedRecipe(userId, recipeId)
                ?.observeOn(AndroidSchedulers.mainThread())
                ?.subscribeOn(Schedulers.io())
        }
        fun deleteLikedRecipeImpl(restClient: RestClient?, userId: String,
                              recipeId: String): Single<ServerResponseCode>? {
            return restClient?.deleteLikedRecipe(userId, recipeId)
                ?.observeOn(AndroidSchedulers.mainThread())
                ?.subscribeOn(Schedulers.io())
        }
        fun postAppErrorMessageImpl(restClient: RestClient?,
                                    userMessage: UserMessage): Single<UserMessageResponse>? {
            return restClient?.postAppErrorMessage(userMessage)
                ?.observeOn(AndroidSchedulers.mainThread())
                ?.subscribeOn(Schedulers.io())
        }
        fun createSubImpl(restClient: RestClient?, sub: Subscription): Single<SubscriptionResponse>? {
            return restClient?.createSub(sub)
                ?.observeOn(AndroidSchedulers.mainThread())
                ?.subscribeOn(Schedulers.io())
        }
        fun getUser(restClient: RestClient?, userId: String): Single<UserResponse>? {
            return restClient?.getUser(userId)
                ?.observeOn(AndroidSchedulers.mainThread())
                ?.subscribeOn(Schedulers.io())
        }
    }
}