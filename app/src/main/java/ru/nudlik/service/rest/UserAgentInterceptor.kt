package ru.nudlik.service.rest

import okhttp3.Interceptor
import okhttp3.Response

class UserAgentInterceptor(private val agent: String) : Interceptor {
    override fun intercept(chain: Interceptor.Chain): Response {
        val request = chain.request()
        val userAgent = request.newBuilder().header("User-Agent", agent).build()
        return chain.proceed(userAgent)
    }
}