package ru.nudlik.service.rest

import android.content.Context
import android.content.DialogInterface
import io.reactivex.disposables.CompositeDisposable
import okhttp3.MultipartBody
import ru.nudlik.R
import ru.nudlik.domain.*
import ru.nudlik.response.DataResponse
import ru.nudlik.response.ServerResponseCode
import ru.nudlik.security.UserRequest
import ru.nudlik.singleton.RestClientSingleton

class RestUtilService(ctx: Context) {
    private val compositeDisposable = CompositeDisposable()
    val restClient = RestClientSingleton.getRestClientInstance()
    private val context: Context = ctx
    var showExceptionDialog = true

    private fun <T> consume(callback: ResponseCallback) = { response: DataResponse<T>?, e: Throwable? ->
        if (response != null) {
            if (response.code == ServerResponseCode.OK_RESPONSE) {
                callback.success(response.data)
            } else {
                callback.errorCode(response.code)
            }
        } else {
            if (showExceptionDialog) {
                ErrorHandler.processErrorImpl(
                    context,
                    e,
                    DialogInterface.OnClickListener { dialog, _ ->
                        dialog.dismiss()
                    })
            }
            callback.exception(e)
        }
    }

    private fun <T> consumeList(callback: ResponseCallback) = { list: List<DataResponse<T>>?, e: Throwable? ->
        if (list != null) {
            val data = ArrayList<T>(list.size)
            list.filter { it.code == ServerResponseCode.OK_RESPONSE }
                .map { it.data }
                .forEach { it?.let { nonNull -> data.add(nonNull) } }
            callback.success(data)
        } else {
            if (showExceptionDialog) {
                ErrorHandler.processErrorImpl(
                    context,
                    e,
                    DialogInterface.OnClickListener { dialog, _ ->
                        dialog.dismiss()
                    })
            }
            callback.exception(e)
        }
    }

    private fun consumeCode(callback: ResponseCallback) = { responseCode: ServerResponseCode?, e: Throwable? ->
        if (responseCode != null) {
            if (responseCode == ServerResponseCode.OK_RESPONSE) {
                callback.success(responseCode)
            } else {
                callback.errorCode(responseCode)
            }
        } else {
            if (showExceptionDialog) {
                ErrorHandler.processErrorImpl(
                    context,
                    e,
                    DialogInterface.OnClickListener { dialog, _ ->
                        dialog.dismiss()
                    })
            }
            callback.exception(e)
        }
    }

    fun getRecipes(page: Int, size: Int, callback: ResponseCallback) {
        compositeDisposable.add(
            RestClient.getRecipesImpl(restClient, page, size)
                ?.subscribe(consumeList(callback))
        )
    }

    fun getRecipeById(recipeId: String, callback: ResponseCallback) {
        compositeDisposable.add(
            RestClient.getRecipeByIdImpl(restClient, recipeId)
                ?.subscribe(consume(callback))
        )
    }

    fun getRecipesByType(recipeType: DrinkType, page: Int, size: Int, callback: ResponseCallback) {
        compositeDisposable.add(
            RestClient.getRecipesByTypeImpl(restClient, recipeType, page, size)
                ?.subscribe(consumeList(callback))
        )
    }

    fun getRecipesByEmail(email: String, page: Int, size: Int, callback: ResponseCallback) {
        compositeDisposable.add(
            RestClient.getRecipesByUserEmailImpl(restClient, email, page, size)
                ?.subscribe(consumeList(callback))
        )
    }

    fun searchRecipes(search: RecipesToSearch, page: Int, size: Int, callback: ResponseCallback) {
        compositeDisposable.add(
            RestClient.searchRecipesImpl(restClient, search, page, size)
                ?.subscribe(consumeList(callback))
        )
    }

    fun postRecipe(recipe: Recipe, callback: ResponseCallback) {
        compositeDisposable.add(
            RestClient.createRecipeImpl(restClient, recipe)
                ?.subscribe(consume(callback))
        )
    }

    fun activateRecipe(recipeId: String, userId: String, callback: ResponseCallback) {
        compositeDisposable.add(
            RestClient.activateRecipeImpl(restClient, recipeId, userId)
                ?.subscribe(consumeCode(callback))
        )
    }

    fun incView(recipeId: String, callback: ResponseCallback) {
        compositeDisposable.add(
            RestClient.incRecipeViewedImpl(restClient, recipeId)
                ?.subscribe(consumeCode(callback))
        )
    }

    fun incClick(adId: String, callback: ResponseCallback) {
        compositeDisposable.add(
            RestClient.incRecipeAdClickedImpl(restClient, adId)
                ?.subscribe(consumeCode(callback))
        )
    }

    fun deleteRecipe(recipeId: String, userId: String, callback: ResponseCallback) {
        compositeDisposable.add(
            RestClient.deleteRecipeImpl(restClient, recipeId, userId)
                ?.subscribe(consumeCode(callback))
        )
    }

    fun registerUser(email: String, emailHash: String, user: UserRequest, callback: ResponseCallback) {
        compositeDisposable.add(
            RestClient.registerImpl(restClient, email, emailHash, user)
                ?.subscribe(consume(callback))
        )
    }

    fun loginUser(user: UserRequest, callback: ResponseCallback) {
        compositeDisposable.add(
            RestClient.loginImpl(restClient, user)
                ?.subscribe(consume(callback))
        )
    }

    fun logoutUser(userId: String, callback: ResponseCallback) {
        compositeDisposable.add(
            RestClient.logoutImpl(restClient, userId)
                ?.subscribe(consumeCode(callback))
        )
    }

    fun loginWithPassword(user: UserRequest, callback: ResponseCallback) {
        compositeDisposable.add(
            RestClient.loginByPasswordImpl(restClient, user)
                ?.subscribe(consume(callback))
        )
    }

    fun checkEmailOnRegisterRequest(email: String, callback: ResponseCallback) {
        compositeDisposable.add(
            RestClient.checkEmailImpl(restClient, email)
                ?.subscribe(consumeCode(callback))
        )
    }

    fun checkEmailOnRecoverRequest(email: String, callback: ResponseCallback) {
        compositeDisposable.add(
            RestClient.recoverEmailHashRequestImpl(restClient, email)
                ?.subscribe(consumeCode(callback))
        )
    }

    fun recoverWithEmail(emailHash: String, user: UserRequest, callback: ResponseCallback) {
        compositeDisposable.add(
            RestClient.recoverByEmailImpl(restClient, emailHash, user)
                ?.subscribe(consume(callback))
        )
    }

    fun postComment(userComment: UserComment, callback: ResponseCallback) {
        compositeDisposable.add(
            RestClient.createCommentImpl(restClient, userComment)
                ?.subscribe(consume(callback))
        )
    }

    fun getCommentsByRecipeId(recipeId: String, page: Int, size: Int, callback: ResponseCallback) {
        compositeDisposable.add(
            RestClient.getCommentsOfRecipeImpl(restClient, recipeId, page, size)
                ?.subscribe(consumeList(callback))
        )
    }

    fun isCommentPosted(recipeId: String, userId: String, callback: ResponseCallback) {
        compositeDisposable.add(
            RestClient.isCommentAlreadyPostedImpl(restClient, recipeId, userId)
                ?.subscribe(consumeCode(callback))
        )
    }

    fun postRecipeImage(recipeId: String, file: MultipartBody.Part, callback: ResponseCallback) {
        compositeDisposable.add(
            RestClient.uploadRecipeImageImpl(restClient, recipeId, file)
                ?.subscribe(consume(callback))
        )
    }

    fun postRecipeStepImage(recipeId: String, stepId: String, file: MultipartBody.Part, callback: ResponseCallback) {
        compositeDisposable.add(
            RestClient.uploadRecipeStepImageImpl(restClient, recipeId, stepId, file)
                ?.subscribe(consume(callback))
        )
    }

    fun postRecipeAdImage(recipeId: String, adId: String, file: MultipartBody.Part, callback: ResponseCallback) {
        compositeDisposable.add(
            RestClient.uploadRecipeAdImageImpl(restClient, recipeId, adId, file)
                ?.subscribe(consume(callback))
        )
    }

    fun postUserAvatar(userId: String, file: MultipartBody.Part, callback: ResponseCallback) {
        compositeDisposable.add(
            RestClient.uploadUserAvatarImpl(restClient, userId, file)
                ?.subscribe(consume(callback))
        )
    }

    fun addLikeRecipe(userId: String, recipeId: String, callback: ResponseCallback = object : ResponseCallback {}) {
        compositeDisposable.add(
            RestClient.addLikedRecipeImpl(restClient, userId, recipeId)
                ?.subscribe(consumeCode(callback))
        )
    }

    fun deleteLikeRecipe(userId: String, recipeId: String, callback: ResponseCallback) {
        compositeDisposable.add(
            RestClient.deleteLikedRecipeImpl(restClient, userId, recipeId)
                ?.subscribe(consumeCode(callback))
        )
    }

    fun postAppError(userMessage: UserMessage, callback: ResponseCallback) {
        compositeDisposable.add(
            RestClient.postAppErrorMessageImpl(restClient, userMessage)
                ?.subscribe(consume(callback))
        )
    }

    fun createSub(subscription: Subscription, callback: ResponseCallback) {
        compositeDisposable.add(
            RestClient.createSubImpl(restClient, subscription)
                ?.subscribe(consume(callback))
        )
    }

    fun getUser(userId: String, callback: ResponseCallback) {
        compositeDisposable.add(
            RestClient.getUser(restClient, userId)
                ?.subscribe(consume(callback))
        )
    }

    fun destroy() {
        compositeDisposable.clear()
    }

    fun getStringResponseRes(responseCode: ServerResponseCode?): Int {
        return when (responseCode) {
            ServerResponseCode.UNIQUE_EMAIL_EXCEPTION -> {
                R.string.email_unique_validation_title
            }
            ServerResponseCode.COMMENT_NOT_FOUND -> {
                R.string.comment_not_found_title
            }
            ServerResponseCode.VALIDATION_EXCEPTION -> {
                R.string.validation_exception_title
            }
            ServerResponseCode.USER_NOT_FOUND -> {
                R.string.user_not_found_title
            }
            ServerResponseCode.LOGIN_FAILED -> {
                R.string.login_failed_title
            }
            ServerResponseCode.FILE_IS_TOO_BIG -> {
                R.string.invalid_file_size_title
            }
            ServerResponseCode.FILE_UPLOAD_FAILED -> {
                R.string.upload_failed_title
            }
            ServerResponseCode.IMAGE_RESIZE_FAILED -> {
                R.string.image_resize_failed_title
            }
            ServerResponseCode.RECIPE_NOT_FOUND -> {
                R.string.recipe_not_found_title
            }
            ServerResponseCode.RESOURCE_NOT_FOUND -> {
                R.string.resource_not_found_title
            }
            ServerResponseCode.UNKNOWN_EXCEPTION -> {
                R.string.unknown_exception_title
            }
            ServerResponseCode.CHECK_EMAIL_FAILED -> {
                R.string.check_email_failed_title
            }
            ServerResponseCode.USER_MESSAGE_NOT_FOUND -> {
                R.string.app_error_not_found
            }
            ServerResponseCode.SUBSCRIPTION_NOT_FOUND -> {
                R.string.app_error_subscription_not_found
            }
            ServerResponseCode.SUBSCRIPTION_NOT_VALID -> {
                R.string.app_error_subscription_not_valid
            }
            else -> {
                R.string.unknown_exception_title
            }
        }
    }

    interface ResponseCallback {
        fun <T> success(data: T?) {}
        fun errorCode(responseCode: ServerResponseCode?) {}
        fun exception(e: Throwable?) {
        }
    }
}