package ru.nudlik.service.rest

import okhttp3.Credentials
import okhttp3.Interceptor
import okhttp3.Response

class BasicAuthInterceptor : Interceptor {
    private var credentials: String? = null

    override fun intercept(chain: Interceptor.Chain): Response {
        val request = chain.request()
        return if (credentials != null) {
            val authRequest = request.newBuilder().header("Authorization", credentials!!).build()
            chain.proceed(authRequest)
        } else {
            chain.proceed(request)
        }
    }

    fun addCredentials(user: String, password: String) {
        credentials = Credentials.basic(user, password)
    }

    fun deleteCredintials() {
        credentials = null
    }
}