package ru.nudlik.service.rest

import android.app.Activity
import android.content.Context
import android.content.DialogInterface
import retrofit2.HttpException
import ru.nudlik.R
import ru.nudlik.fragment.dialog.AlertDialogBuilder
import ru.nudlik.response.ServerResponseCode
import java.net.ConnectException
import java.net.SocketTimeoutException
import kotlin.system.exitProcess

interface ErrorHandler {
    companion object {
        fun processResponse(context: Context?, responseCode: ServerResponseCode?, okButton: DialogInterface.OnClickListener) {
            if (context !is Activity) return
            AlertDialogBuilder.createInfoDialogPosAct(context, R.string.unexpected_response,
                "Response Code: ${responseCode?.name}", okButton).show()
        }
        fun processResponse(context: Context?, responseCode: ServerResponseCode?) {
            if (context !is Activity) return
            AlertDialogBuilder.createInfoDialogPosAct(context, R.string.unexpected_response,
                "Response Code: ${responseCode?.name}", DialogInterface.OnClickListener { _,_ ->
                    exitProcess(0)
                }).show()
        }
        fun processError(context: Context?, e: Throwable?) {
            if (context !is Activity) return
            AlertDialogBuilder.createCrashAppDialog(context, R.string.unexpected_error, "Error: ${e?.message}. Application will be closed!").show()
        }
        private fun processError(context: Context?, e: Throwable?, okButton: DialogInterface.OnClickListener) {
            AlertDialogBuilder.createInfoDialogPosAct(context, R.string.unexpected_error, "Error: ${e?.message}!", okButton).show()
        }

        fun processErrorImpl(context: Context?, e: Throwable?, okButton: DialogInterface.OnClickListener) {
            when (e) {
                is SocketTimeoutException -> AlertDialogBuilder.
                        createInfoDialogPosAct(context, R.string.timeout_error_title, R.string.timeout_error_body,
                            okButton).show()
                is ConnectException -> AlertDialogBuilder.
                        createInfoDialogPosAct(context, R.string.connect_error_title, R.string.connect_error_body,
                            okButton).show()
                is HttpException -> {
                    when (e.code()) {
                        401 -> AlertDialogBuilder.createInfoDialogPosAct(context, R.string.auth_error_title,
                            R.string.auth_error_body, okButton).show()
                        404 -> AlertDialogBuilder.createInfoDialogPosAct(context, R.string.resource_not_found_title,
                            R.string.resource_not_found_body, okButton).show()
                        500 -> AlertDialogBuilder
                            .createInfoDialogPosAct(context, R.string.server_internal_error_title,
                            R.string.server_internal_error_body, okButton).show()
                        else -> processError(context, e, okButton)
                    }
                }
                else -> processError(context, e, okButton)
            }
        }

        fun processResponseImpl(context: Context?, responseCode: ServerResponseCode?, okButton: DialogInterface.OnClickListener) {
            if (context !is Activity) return
            when (responseCode) {
                ServerResponseCode.LOGIN_FAILED -> {
                    AlertDialogBuilder.createInfoDialogPosAct(context, R.string.login_failed_title, R.string.login_failed_body,
                        okButton).show()
                }
                ServerResponseCode.USER_NOT_FOUND -> {
                    AlertDialogBuilder.createInfoDialogPosAct(context, R.string.user_not_found_title,
                        R.string.user_not_found_body, okButton).show()
                }
                ServerResponseCode.VALIDATION_EXCEPTION -> {
                    AlertDialogBuilder.createInfoDialogPosAct(context, R.string.validation_exception_title,
                        R.string.validation_exception_body, okButton).show()
                }
                ServerResponseCode.UNIQUE_EMAIL_EXCEPTION -> {
                    AlertDialogBuilder.createInfoDialogPosAct(context, R.string.email_unique_validation_title,
                        R.string.email_unique_validation_body, okButton).show()
                }
                ServerResponseCode.COMMENT_NOT_FOUND -> {
                    AlertDialogBuilder.createInfoDialogPosAct(context, R.string.comment_not_found_title,
                        R.string.comment_not_found_body, okButton).show()
                }
                ServerResponseCode.RECIPE_NOT_FOUND -> {
                    AlertDialogBuilder.createInfoDialogPosAct(context, R.string.recipe_not_found_title,
                        R.string.recipe_not_found_body, okButton).show()
                }
                ServerResponseCode.FILE_UPLOAD_FAILED -> {
                    AlertDialogBuilder.createInfoDialogPosAct(context, R.string.upload_failed_title,
                        R.string.upload_failed_body, okButton).show()
                }
                ServerResponseCode.RESOURCE_NOT_FOUND -> {
                    AlertDialogBuilder.createInfoDialogPosAct(context, R.string.resource_not_found_title,
                        R.string.resource_not_found_body, okButton).show()
                }
                ServerResponseCode.IMAGE_RESIZE_FAILED -> {
                    AlertDialogBuilder.createInfoDialogPosAct(context, R.string.image_resize_failed_title,
                        R.string.image_resize_failed_body, okButton).show()
                }
                ServerResponseCode.FILE_IS_TOO_BIG -> {
                    AlertDialogBuilder.createInfoDialogPosAct(context, R.string.invalid_file_size_title,
                        R.string.invalid_file_size_body, okButton).show()
                }
                ServerResponseCode.UNKNOWN_EXCEPTION -> {
                    AlertDialogBuilder.createInfoDialogPosAct(context, R.string.unknown_exception_title,
                        R.string.unknown_exception_body, okButton).show()
                }
                else -> {
                    processResponse(context, responseCode, okButton)
                }
            }
        }
    }
}