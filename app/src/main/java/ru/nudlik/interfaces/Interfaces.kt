package ru.nudlik.interfaces

import androidx.recyclerview.widget.RecyclerView
import ru.nudlik.service.rest.RestUtilService

interface BSListener<T> {
    fun add(t: T)
    fun edit(t: T)
    fun delete(t: T)
}

interface RequestNextPageListener {
    fun loadNextPage(page: Int, size: Int, callback: RestUtilService.ResponseCallback)
}

interface LoadNextPageCallback {
    fun loadFinished()
}

interface RequestImage {
    fun requestImage(requestCode: Int, filename: String, viewHolder: RecyclerView.ViewHolder)
}

interface PostImage {
    fun postImage(path: String)
}

interface SwipeListItemControllerAction {
    fun onLeftClicked(position: Int)
    fun onRightClicked(position: Int)
}

interface DialogButtonsListener {
    fun positiveClick(ext: Any? = null)
    fun negativeClick(ext: Any? = null) {}
}