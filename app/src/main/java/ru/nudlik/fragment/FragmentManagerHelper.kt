package ru.nudlik.fragment

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import ru.nudlik.R
import ru.nudlik.adapter.recipe.items.RecipeItem
import ru.nudlik.domain.Recipe
import ru.nudlik.fragment.nav.AddedRecipesFragment
import ru.nudlik.fragment.nav.LikedRecipesFragment
import ru.nudlik.interfaces.RequestNextPageListener
import ru.nudlik.listener.ItemClickListener

class FragmentManagerHelper(private val supportFragmentManager: FragmentManager) {

    private fun replaceFragment(frag: Fragment, tag: String, addToBackStack: Boolean) {
        val tr = supportFragmentManager.beginTransaction()
        tr.replace(R.id.main_fragment_container, frag, tag)
        if (addToBackStack) {
            if (isCurrentNavFragShowing()) {
                supportFragmentManager.popBackStack()
            }
            tr.addToBackStack(null)
        }
        tr.commit()
    }

    fun showRecipesFragment(list: List<RecipeItem>, loadRequest: RequestNextPageListener, itemClickListener: ItemClickListener?) {
        val g = findFragment(RECIPES_FRAG_TAG)
        if (g == null) {
            val frag = RecipesFragment.createInstance(list, loadRequest, itemClickListener)
            replaceFragment(frag, RECIPES_FRAG_TAG, false)
        }
    }

    fun showLikedRecipesFragment(list: List<String>, itemClickListener: ItemClickListener?, deleteItemClickListener: ItemClickListener?) {
        val g = findFragment(LIKED_RECIPES_FRAG_TAG)
        if (g == null) {
            val frag = LikedRecipesFragment.createInstance(list, itemClickListener, deleteItemClickListener)
            replaceFragment(frag, LIKED_RECIPES_FRAG_TAG, true)
        }
    }

    fun showAddedRecipesFragment(list: List<String>, itemClickListener: ItemClickListener?, deleteItemClickListener: ItemClickListener?) {
        val g = findFragment(ADDED_RECIPES_FRAG_TAG)
        if (g == null) {
            val frag = AddedRecipesFragment.createInstance(list, itemClickListener, deleteItemClickListener)
            replaceFragment(frag, ADDED_RECIPES_FRAG_TAG, true)
        }
    }

    fun filterFromSearch(text: String?) {
        var g = findFragment(LIKED_RECIPES_FRAG_TAG)
        if (g != null) {
            (g as LikedRecipesFragment).filter(text)
        }
        g = findFragment(ADDED_RECIPES_FRAG_TAG)
        if (g != null) {
            (g as AddedRecipesFragment).filter(text)
        }
    }

    fun addRecipes(list: List<RecipeItem>) {
        var g = findFragment(RECIPES_FRAG_TAG)
        if (g != null) {
            g = g as RecipesFragment
            g.addItems(list)
            g.checkEmptyData()
        }
    }

    fun clearRecipes() {
        var g = findFragment(RECIPES_FRAG_TAG)
        if (g != null) {
            g = g as RecipesFragment
            g.clearItems()
            g.checkEmptyData()
        }
    }

    fun isCurrentNavFragShowing() : Boolean {
        return findFragment(LIKED_RECIPES_FRAG_TAG) != null ||
                findFragment(ADDED_RECIPES_FRAG_TAG) != null
    }

    fun getCurrentActiveFragmentTag(): String {
        return when {
            findFragment(LIKED_RECIPES_FRAG_TAG) != null -> LIKED_RECIPES_FRAG_TAG
            findFragment(ADDED_RECIPES_FRAG_TAG) != null -> ADDED_RECIPES_FRAG_TAG
            else -> RECIPES_FRAG_TAG
        }
    }

    private fun findFragment(tag: String): Fragment? {
        return supportFragmentManager.findFragmentByTag(tag)
    }

    private fun updateFragment(oldFrag: Fragment, newFrag: Fragment, tag: String, addToBackStack: Boolean = true) {
        val tr = supportFragmentManager.beginTransaction()
        tr.remove(oldFrag).commit()
        replaceFragment(newFrag, tag, addToBackStack)
    }

    fun updateLikedRecipe(position: Int) {
        val g = findFragment(LIKED_RECIPES_FRAG_TAG)
        if (g != null) {
            val gg = g as LikedRecipesFragment
            gg.updateAt(position)
        }
    }

    fun reloadLikedRecipesFragment(list: List<String>, itemClickListener: ItemClickListener?, deleteItemClickListener: ItemClickListener?) {
        val frag = findFragment(LIKED_RECIPES_FRAG_TAG)
        if (frag != null) {
            val newFrag = LikedRecipesFragment.createInstance(list, itemClickListener, deleteItemClickListener, false)
            updateFragment(frag, newFrag, LIKED_RECIPES_FRAG_TAG)
        }
    }

    fun reloadAddedRecipesFragment(list: List<String>, itemClickListener: ItemClickListener?, deleteItemClickListener: ItemClickListener?) {
        val frag = findFragment(ADDED_RECIPES_FRAG_TAG)
        if (frag != null) {
            val newFrag = AddedRecipesFragment.createInstance(list, itemClickListener, deleteItemClickListener, false)
            updateFragment(frag, newFrag, ADDED_RECIPES_FRAG_TAG)
        }
    }

    fun reloadMainFragment(list: List<RecipeItem>, loadRequest: RequestNextPageListener,
                           itemClickListener: ItemClickListener?) {
        val frag = findFragment(RECIPES_FRAG_TAG)
        if (frag != null) {
            val newFrag = RecipesFragment.createInstance(list, loadRequest, itemClickListener)
            updateFragment(frag, newFrag, RECIPES_FRAG_TAG, false)
        }
    }

    companion object {
        const val RECIPES_FRAG_TAG = "recipes_frag_tag"
        const val ADDED_RECIPES_FRAG_TAG = "added_recipe_frag_tag"
        const val LIKED_RECIPES_FRAG_TAG = "liked_recipe_frag_tag"
    }
}