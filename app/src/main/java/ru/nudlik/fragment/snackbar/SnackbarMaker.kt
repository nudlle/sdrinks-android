package ru.nudlik.fragment.snackbar

import android.view.View
import com.google.android.material.snackbar.Snackbar

interface SnackbarMaker {
    companion object {
        private fun createSnackbar(viewParent: View, textRes: Int, duration: Int): Snackbar {
            return Snackbar.make(viewParent, textRes, duration)
        }
        private fun createSnackbar(viewParent: View, text: String, duration: Int): Snackbar {
            return Snackbar.make(viewParent, text, duration)
        }
        fun showLongSnackbar(viewParent: View, textRes: Int) {
            createSnackbar(viewParent, textRes, Snackbar.LENGTH_LONG).show()
        }
        fun showLongSnackbar(viewParent: View, text: String) {
            createSnackbar(viewParent, text, Snackbar.LENGTH_LONG).show()
        }
        fun showLongSnackbar(viewParent: View, textRes: Int, actionRes: Int) {
            val snack = createSnackbar(viewParent, textRes, Snackbar.LENGTH_LONG)
            snack.setAction(actionRes) { snack.dismiss() }
            snack.show()
        }
        fun showLongSnackbar(viewParent: View, textRes: Int, action: String) {
            val snack = createSnackbar(viewParent, textRes, Snackbar.LENGTH_LONG)
            snack.setAction(action) { snack.dismiss() }
            snack.show()
        }
        fun showLongSnackbar(viewParent: View, textRes: Int, actionRes: Int, listener: View.OnClickListener) {
            val snack = createSnackbar(viewParent, textRes, Snackbar.LENGTH_LONG)
            snack.setAction(actionRes, listener)
            snack.show()
        }
        fun showLongSnackbar(viewParent: View, textRes: Int, action: String, listener: View.OnClickListener) {
            val snack = createSnackbar(viewParent, textRes, Snackbar.LENGTH_LONG)
            snack.setAction(action, listener)
            snack.show()
        }
        fun showShortSnackbar(viewParent: View, textRes: Int) {
            createSnackbar(viewParent, textRes, Snackbar.LENGTH_SHORT).show()
        }
        fun showShortSnackbar(viewParent: View, text: String) {
            createSnackbar(viewParent, text, Snackbar.LENGTH_SHORT).show()
        }
        fun showShortSnackbar(viewParent: View, textRes: Int, actionRes: Int) {
            val snack = createSnackbar(viewParent, textRes, Snackbar.LENGTH_SHORT)
            snack.setAction(actionRes) { snack.dismiss() }
            snack.show()
        }
        fun showShortSnackbar(viewParent: View, textRes: Int, action: String) {
            val snack = createSnackbar(viewParent, textRes, Snackbar.LENGTH_SHORT)
            snack.setAction(action) { snack.dismiss() }
            snack.show()
        }
        fun showShortSnackbar(viewParent: View, textRes: Int, actionRes: Int, listener: View.OnClickListener) {
            val snack = createSnackbar(viewParent, textRes, Snackbar.LENGTH_SHORT)
            snack.setAction(actionRes, listener)
            snack.show()
        }
        fun showShortSnackbar(viewParent: View, textRes: Int, action: String, listener: View.OnClickListener) {
            val snack = createSnackbar(viewParent, textRes, Snackbar.LENGTH_SHORT)
            snack.setAction(action, listener)
            snack.show()
        }
    }
}