package ru.nudlik.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.RelativeLayout
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import ru.nudlik.R
import ru.nudlik.adapter.recipe.items.RecipeItem
import ru.nudlik.adapter.recipe.RecipeItemAdapter
import ru.nudlik.extentions.gone
import ru.nudlik.extentions.inflate
import ru.nudlik.extentions.visible
import ru.nudlik.interfaces.LoadNextPageCallback
import ru.nudlik.interfaces.RequestNextPageListener
import ru.nudlik.listener.ItemClickListener


class RecipesFragment : BaseFragment() {
    private lateinit var recyclerView: RecyclerView
    private var adapter: RecipeItemAdapter? = null
    private lateinit var noContent: RelativeLayout
    private val list = ArrayList<RecipeItem?>()
    private var loading: Boolean = false
    private var loadListener: RequestNextPageListener? = null
    private var itemClickListener: ItemClickListener? = null

    fun loadMore() {
        list.add(null)
        adapter?.notifyItemInserted(list.size - 1)
    }

    fun addItems(newData: List<RecipeItem>) {
        list.addAll(newData)
        adapter?.notifyDataSetChanged()
    }

    fun clearItems() {
        list.clear()
        adapter?.notifyDataSetChanged()
    }

    fun addItemClickListener(listener: ItemClickListener?) {
        this.itemClickListener = listener
    }

    private fun setLoadListener(listener: RequestNextPageListener?) {
        loadListener = listener
    }

    private val loadCallback = object : LoadNextPageCallback {
        override fun loadFinished() {
            loading = false
        }
    }

    fun checkEmptyData() {
        if (adapter?.itemCount == 0) noContent.visible(100) else noContent.gone(100)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = container?.inflate(R.layout.fragment_main)
        return if (view != null) {
            recyclerView = view.findViewById(R.id.recipe_recycler_view)
            noContent = view.findViewById(R.id.no_content_holder)

            adapter = RecipeItemAdapter(list, itemClickListener)
            adapter?.setRequestNextPageListener(loadListener)
            adapter?.setLoadCallback(loadCallback)

            recyclerView.setHasFixedSize(false)
            recyclerView.layoutManager = LinearLayoutManager(context)
            recyclerView.adapter = adapter

            recyclerView.addOnScrollListener(object : RecyclerView.OnScrollListener() {
                override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                    super.onScrolled(recyclerView, dx, dy)

                    val llm = recyclerView.layoutManager as LinearLayoutManager
                    val totalItemCount = llm.itemCount
                    val lastVisibleItem = llm.findLastCompletelyVisibleItemPosition()
                    if (!loading && totalItemCount <= (lastVisibleItem + 5)) {
                        if (adapter!!.doLoad()) {
                            loadMore()
                            loading = true
                        }
                    }
                }
            })

            view
        } else {
            super.onCreateView(inflater, container, savedInstanceState)
        }
    }

    override fun onResume() {
        super.onResume()
        noContent.postDelayed( { checkEmptyData() }, 2000 )
    }

    companion object {
        fun createInstance(list: List<RecipeItem>, listener: RequestNextPageListener?,
                           itemClickListener: ItemClickListener?): RecipesFragment {
            val frag = RecipesFragment()
            frag.addItems(list)
            frag.setLoadListener(listener)
            frag.addItemClickListener(itemClickListener)
            return frag
        }
    }
}