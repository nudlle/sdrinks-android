package ru.nudlik.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import ru.nudlik.R
import ru.nudlik.adapter.recipe.RecipeIngredientsAdapter
import ru.nudlik.adapter.recipe.RecipeStepAdapter
import ru.nudlik.domain.Recipe
import ru.nudlik.extentions.runLayoutAnimation
import ru.nudlik.listener.ItemClickListener

class RecipePreviewContentFragment : BaseFragment() {
    private lateinit var recipe: Recipe
    private lateinit var recipeStepsRecyclerView: RecyclerView
    private lateinit var recipeStepsAdapter: RecipeStepAdapter
    var adClickListener: ItemClickListener? = null

    private fun createStepsAdapter() {
        recipeStepsAdapter = RecipeStepAdapter(null, recipe.ad, false)
        recipeStepsAdapter.getList().clear()
        recipeStepsAdapter.adClickListener = adClickListener

        recipe.recipeSteps.sortBy { it.number }
        for (step in recipe.recipeSteps) {
            recipeStepsAdapter.getList().add(step)
            if (recipe.ad != null && recipe.ad?.stepPosition == step.number) {
                recipeStepsAdapter.getList().add(null)
            }
        }
    }

    private fun createRecipeStepsRecyclerView(v: View) {
        recipeStepsRecyclerView = v.findViewById(R.id.recipe_steps_list_preview)
        recipeStepsRecyclerView.adapter = recipeStepsAdapter
        recipeStepsRecyclerView.layoutManager = LinearLayoutManager(v.context, RecyclerView.VERTICAL, false)
        recipeStepsRecyclerView.isNestedScrollingEnabled = false
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.content_recipe_preview, container, false)

        val recipeIngredientsAdapter = RecipeIngredientsAdapter(null, false)
        for (ing in recipe.ingredients) {
            recipeIngredientsAdapter.getList().add(ing)
        }

        val recipeIngredientsRecyclerView = view.findViewById<RecyclerView>(R.id.recipe_ingredients_list_preview)
        recipeIngredientsRecyclerView.adapter = recipeIngredientsAdapter
        recipeIngredientsRecyclerView.layoutManager = LinearLayoutManager(view.context, RecyclerView.VERTICAL, false)
        recipeIngredientsRecyclerView.isNestedScrollingEnabled = false

        createStepsAdapter()
        createRecipeStepsRecyclerView(view)

        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        recipeStepsRecyclerView.runLayoutAnimation(R.anim.layout_animation_from_bottom)
    }

    companion object {
        fun createInstance(recipe: Recipe): RecipePreviewContentFragment {
            val frag = RecipePreviewContentFragment()
            frag.recipe = recipe
            return frag
        }
    }
}