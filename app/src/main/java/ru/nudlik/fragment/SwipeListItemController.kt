package ru.nudlik.fragment

import android.annotation.SuppressLint
import android.content.Context
import android.graphics.*
import android.graphics.drawable.BitmapDrawable
import android.graphics.drawable.VectorDrawable
import android.view.MotionEvent
import androidx.appcompat.content.res.AppCompatResources
import androidx.core.graphics.drawable.toBitmap
import androidx.recyclerview.widget.ItemTouchHelper
import androidx.recyclerview.widget.RecyclerView
import ru.nudlik.interfaces.SwipeListItemControllerAction
import java.lang.IllegalArgumentException
import kotlin.math.max
import kotlin.math.min


class SwipeListItemController(private val swipeFlags: Int) : ItemTouchHelper.Callback() {
    private var swipeBack: Boolean = false
    private var buttonState: ButtonState = ButtonState.GONE
    private var currentItemViewHolder: RecyclerView.ViewHolder? = null
    private var buttonInstance: RectF? = null
    var buttonsActions: SwipeListItemControllerAction? = null
    var buttonWidth = 300f
    var buttonWidthPadding = 0
    var cornerRadius = 16f
    var textSize = 60f
    var leftResIcon: Int? = null
    var rightResIcon: Int? = null
    var leftResText: Int? = null
    var rightResText: Int? = null
    var leftBackgroundColor = Color.GRAY
    var rightBackgroundColor = Color.GRAY
    var textColor = Color.WHITE

    override fun getMovementFlags(recyclerView: RecyclerView, viewHolder: RecyclerView.ViewHolder): Int {
        return makeMovementFlags(0, swipeFlags)
    }

    override fun onMove(
        recyclerView: RecyclerView,
        viewHolder: RecyclerView.ViewHolder,
        target: RecyclerView.ViewHolder
    ): Boolean = false

    override fun onSwiped(viewHolder: RecyclerView.ViewHolder, direction: Int) {

    }

    override fun convertToAbsoluteDirection(flags: Int, layoutDirection: Int): Int {
        if (swipeBack) {
            swipeBack = buttonState != ButtonState.GONE
            return 0
        }
        return super.convertToAbsoluteDirection(flags, layoutDirection)
    }

    override fun onChildDraw(
        c: Canvas,
        recyclerView: RecyclerView,
        viewHolder: RecyclerView.ViewHolder,
        dX: Float,
        dY: Float,
        actionState: Int,
        isCurrentlyActive: Boolean
    ) {
        var dXX = dX
        if (actionState == ItemTouchHelper.ACTION_STATE_SWIPE) {
            if (buttonState != ButtonState.GONE) {
                if (buttonState == ButtonState.VISIBLE_LEFT) dXX = max(dXX, buttonWidth)
                if (buttonState == ButtonState.VISIBLE_RIGHT) dXX = min(dXX, -buttonWidth)
                super.onChildDraw(c, recyclerView, viewHolder, dXX, dY, actionState, isCurrentlyActive)
            } else {
                setTouchListener(c, recyclerView, viewHolder, dX, dY, actionState, isCurrentlyActive)
            }

        }
        if (buttonState == ButtonState.GONE) {
            super.onChildDraw(c, recyclerView, viewHolder, dXX, dY, actionState, isCurrentlyActive)
        }
        currentItemViewHolder = viewHolder
    }

    fun onDraw(c: Canvas) {
        currentItemViewHolder?.apply {
            drawButtons(c, this)
        }
    }

    @SuppressLint("ClickableViewAccessibility")
    private fun setTouchListener(c: Canvas, recyclerView: RecyclerView,
                                 holder: RecyclerView.ViewHolder,
                                 dX: Float, dY: Float,
                                 actionState: Int, isCurrentlyActive: Boolean) {
        recyclerView.setOnTouchListener { _, p1 ->
            swipeBack = if (p1?.action == MotionEvent.ACTION_CANCEL ||
                p1?.action == MotionEvent.ACTION_UP) {
                true
            } else {
                true
            }
            if (swipeBack) {
                if (dX < -buttonWidth) buttonState = ButtonState.VISIBLE_RIGHT
                else if (dX > buttonWidth) buttonState = ButtonState.VISIBLE_LEFT
                if (buttonState != ButtonState.GONE) {
                    setTouchDownListener(c, recyclerView, holder, dX, dY, actionState, isCurrentlyActive)
                    setItemsClickable(recyclerView, false)
                }
            }
            false
        }
    }

    @SuppressLint("ClickableViewAccessibility")
    private fun setTouchDownListener(c: Canvas, recyclerView: RecyclerView,
                                     holder: RecyclerView.ViewHolder,
                                     dX: Float, dY: Float,
                                     actionState: Int, isCurrentlyActive: Boolean) {
        recyclerView.setOnTouchListener { _, event ->
            if (event.action == MotionEvent.ACTION_DOWN) {
                setTouchUpListener(c, recyclerView, holder, dX, dY, actionState, isCurrentlyActive)
            }
            false
        }
    }

    @SuppressLint("ClickableViewAccessibility")
    private fun setTouchUpListener(c: Canvas, recyclerView: RecyclerView,
                                   holder: RecyclerView.ViewHolder,
                                   dX: Float, dY: Float,
                                   actionState: Int, isCurrentlyActive: Boolean) {
        recyclerView.setOnTouchListener { _, event ->
            if (event.action == MotionEvent.ACTION_UP) {
                super.onChildDraw(c, recyclerView, holder, 0f, dY, actionState, isCurrentlyActive)
                recyclerView.setOnTouchListener { _, _ -> false }
                setItemsClickable(recyclerView, true)
                swipeBack = false
                if (buttonsActions != null && buttonInstance != null
                    && buttonInstance!!.contains(event.x, event.y)) {
                    if (buttonState == ButtonState.VISIBLE_LEFT) {
                        buttonsActions?.onLeftClicked(holder.adapterPosition)
                    }
                    else if (buttonState == ButtonState.VISIBLE_RIGHT) {
                        buttonsActions?.onRightClicked(holder.adapterPosition)
                    }
                }
                buttonState = ButtonState.GONE
                currentItemViewHolder = null
            }
            false
        }
    }

    private fun drawButtons(c: Canvas, viewHolder: RecyclerView.ViewHolder) {
        val buttonWidthWithoutPadding = buttonWidth - buttonWidthPadding
        val corners = cornerRadius
        val itemView = viewHolder.itemView
        val context = itemView.context
        val p = Paint()
        val leftButton = RectF(
            itemView.left.toFloat(),
            itemView.top.toFloat(),
            itemView.left + buttonWidthWithoutPadding,
            itemView.bottom.toFloat()
        )

        p.color = leftBackgroundColor
        c.drawRoundRect(leftButton, corners, corners, p)
        if (leftResIcon != null) {

            val b = context.getDrawable(leftResIcon!!)?.toBitmap()
            b?.apply {
                drawBitmap(this, c, leftButton, p)
            }

        } else {
            if (leftResText != null) {
                val t = context.resources.getString(leftResText!!)
                drawText(t, c, leftButton, p)
            } else {
                drawText("DUMP", c, leftButton, p)
            }
        }
        val rightButton = RectF(
            itemView.right - buttonWidthWithoutPadding,
            itemView.top.toFloat(),
            itemView.right.toFloat(),
            itemView.bottom.toFloat()
        )
        p.color = rightBackgroundColor
        c.drawRoundRect(rightButton, corners, corners, p)
        if (rightResIcon != null) {
            val b = context.getDrawable(rightResIcon!!)?.toBitmap()
            b?.apply {
                drawBitmap(this, c, rightButton, p)
            }

        } else {
            if (leftResText != null) {
                val t = context.resources.getString(rightResText!!)
                drawText(t, c, rightButton, p)
            } else {
                drawText("DUMP", c, rightButton, p)
            }
        }

        buttonInstance = null
        if (buttonState === ButtonState.VISIBLE_LEFT) {
            buttonInstance = leftButton
        } else if (buttonState === ButtonState.VISIBLE_RIGHT) {
            buttonInstance = rightButton
        }
    }

    private fun drawBitmap(bitmap: Bitmap, c: Canvas, button: RectF, p: Paint) {
        p.isAntiAlias = true
        val bitmapHeight = bitmap.height
        val bitmapWidth = bitmap.width
        c.drawBitmap(bitmap, button.centerX() - bitmapWidth / 2, button.centerY() - bitmapHeight / 2, p)
    }

    private fun getBitmap(context: Context, resId: Int) : Bitmap {
        return when (val drawable = context.resources.getDrawable(resId, context.theme)) {
            is BitmapDrawable -> drawable.bitmap
            is VectorDrawable -> getBitmap(drawable)
            else -> throw IllegalArgumentException("unsupported drawable type")
        }
    }

    private fun getBitmap(vd: VectorDrawable) : Bitmap {
        val b = Bitmap.createBitmap(vd.intrinsicWidth * 2, vd.intrinsicHeight * 2, Bitmap.Config.ARGB_8888)
        val c = Canvas(b)
        vd.setBounds(0, 0, c.width, c.height)
        vd.draw(c)
        return b
    }

    private fun drawText(text: String, c: Canvas, button: RectF, p: Paint) {
        val textSize = this.textSize
        p.color = textColor
        p.isAntiAlias = true
        p.textSize = textSize

        val textWidth = p.measureText(text)
        c.drawText(text, button.centerX() - textWidth / 2, button.centerY() + textSize / 2, p)
    }

    private fun setItemsClickable(recyclerView: RecyclerView, isClickable: Boolean) {
        for (i in 0 until recyclerView.childCount) {
            recyclerView.getChildAt(i).isClickable = isClickable
        }
    }

    enum class ButtonState {
        GONE, VISIBLE_LEFT, VISIBLE_RIGHT
    }
}