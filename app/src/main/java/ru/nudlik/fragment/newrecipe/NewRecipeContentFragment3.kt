package ru.nudlik.fragment.newrecipe

import android.Manifest
import android.content.pm.PackageManager
import android.os.Bundle
import android.util.SparseArray
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.app.ActivityCompat
import androidx.core.util.forEach
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import ru.nudlik.R
import ru.nudlik.activity.BaseActivity
import ru.nudlik.activity.NewRecipeActivity
import ru.nudlik.activity.ucrop.UCropHelper
import ru.nudlik.adapter.recipe.RecipeStepAdapter
import ru.nudlik.domain.RecipeStepCook
import ru.nudlik.extentions.inflate
import ru.nudlik.fragment.BaseFragment
import ru.nudlik.fragment.toast.ToastMaker
import ru.nudlik.interfaces.RequestImage
import java.io.File

class NewRecipeContentFragment3 : BaseFragment() {
    private lateinit var stepsList: RecyclerView
    private lateinit var stepsAdapter: RecipeStepAdapter
    private var resourceToLoadImage: RecyclerView.ViewHolder? = null
    var stepFilename: String? = null
    private val imagesPathHash = SparseArray<String>()

    private val requestImageListener = object : RequestImage {
        override fun requestImage(requestCode: Int, filename: String, viewHolder: RecyclerView.ViewHolder) {
            NewRecipeActivity.rCode = NewRecipeActivity.REQUEST_STEP_IMAGE_MODE
            stepFilename = filename
            resourceToLoadImage = viewHolder
            pickFromGallery()
        }
    }

    fun pickFromGallery() {
        if (ActivityCompat.checkSelfPermission(
                activity!!,
                Manifest.permission.READ_EXTERNAL_STORAGE
            ) != PackageManager.PERMISSION_GRANTED
        ) {
            requestPermission(
                Manifest.permission.READ_EXTERNAL_STORAGE,
                getString(R.string.permission_read_storage_rationale),
                BaseActivity.REQUEST_STORAGE_READ_ACCESS_PERMISSION
            )
        } else {
            UCropHelper.pickFromGallery(activity!!, NewRecipeActivity.REQUEST_STEP_IMAGE_MODE)
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = container?.inflate(R.layout.add_recipe_fragment_content3) ?: return super.onCreateView(inflater, container, savedInstanceState)

        stepsList = view.findViewById(R.id.recipe_steps_list)
        stepsAdapter = RecipeStepAdapter(requestImageListener, null, true)
        stepsList.adapter = stepsAdapter
        stepsList.layoutManager = LinearLayoutManager(activity!!, RecyclerView.VERTICAL, false)

        return view
    }

    private fun removeCashData() {
        imagesPathHash.forEach { _, value ->
            val file = File(value)
            file.delete()
        }
        imagesPathHash.clear()
        if (resourceToLoadImage != null) {
            (resourceToLoadImage as RecipeStepAdapter.RecipeStepViewHolder).clearCash()
        }
    }

    override fun onDestroy() {
        removeCashData()
        super.onDestroy()
    }

    fun getRecipeSteps(): MutableList<RecipeStepCook> {
        imagesPathHash.clear()
        val list = ArrayList<RecipeStepCook>(stepsAdapter.list.size - 1)
        for (i in 0 until stepsAdapter.list.size - 1) {
            val step = stepsAdapter.list[i]
            val ss = RecipeStepCook(step)
            imagesPathHash.put(ss.number, ss.imageUrl!!)
            ss.imageUrl = null
            list.add(ss)
        }


        return list
    }

    fun validate(): Boolean {
        return when {
            stepsAdapter.list.size == 1 -> {
                ToastMaker.showShortWarningToastGravityCenter(requireContext(), R.string.add_recipe_steps_warning)
                false
            }
            else -> true
        }
    }

    fun addResource(file: File) {
        (resourceToLoadImage as RecipeStepAdapter.RecipeStepViewHolder).addResource(file)
    }

    fun getStepImagesHash(): SparseArray<String> = imagesPathHash

    companion object {
        fun createInstance(): NewRecipeContentFragment3 = NewRecipeContentFragment3()
    }
}