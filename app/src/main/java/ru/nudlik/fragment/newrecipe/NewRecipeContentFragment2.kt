package ru.nudlik.fragment.newrecipe

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import ru.nudlik.R
import ru.nudlik.adapter.recipe.RecipeIngredientsAdapter
import ru.nudlik.extentions.inflate
import ru.nudlik.fragment.BaseFragment
import ru.nudlik.fragment.toast.ToastMaker

class NewRecipeContentFragment2 : BaseFragment() {
    private lateinit var ingredientsList: RecyclerView
    private lateinit var ingredientsAdapter: RecipeIngredientsAdapter

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = container?.inflate(R.layout.add_recipe_fragment_content2) ?: return super.onCreateView(inflater, container, savedInstanceState)

        ingredientsList = view.findViewById(R.id.recipe_ingredients_list)
        ingredientsAdapter = RecipeIngredientsAdapter(null, true)
        ingredientsList.adapter = ingredientsAdapter
        ingredientsList.layoutManager = LinearLayoutManager(activity!!, RecyclerView.VERTICAL, false)

        return view
    }

    fun getRecipeIngredients(): MutableList<String> {
        val list = ArrayList<String>(ingredientsAdapter.list.size - 1)
        for (ing in 0 until ingredientsAdapter.list.size - 1) {
            list.add(ingredientsAdapter.list[ing]!!)
        }
        return list
    }

    fun validate(): Boolean {
        return when {
            ingredientsAdapter.list.size == 1 -> {
                ToastMaker.showShortWarningToastGravityCenter(requireContext(), "Добавьте ингредиенты")
                false
            }
            else -> true
        }
    }

    companion object {
        fun createInstance(): NewRecipeContentFragment2 = NewRecipeContentFragment2()
    }
}