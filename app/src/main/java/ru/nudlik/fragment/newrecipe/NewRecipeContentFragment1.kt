package ru.nudlik.fragment.newrecipe

import android.Manifest
import android.content.pm.PackageManager
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.CompoundButton
import android.widget.ImageView
import android.widget.RadioGroup
import androidx.cardview.widget.CardView
import androidx.core.app.ActivityCompat
import com.google.android.material.checkbox.MaterialCheckBox
import com.google.android.material.textfield.TextInputLayout
import ru.nudlik.extentions.inflate
import ru.nudlik.R
import ru.nudlik.activity.BaseActivity
import ru.nudlik.activity.NewRecipeActivity
import ru.nudlik.activity.ucrop.UCropHelper
import ru.nudlik.domain.DrinkState
import ru.nudlik.domain.DrinkTaste
import ru.nudlik.domain.DrinkType
import ru.nudlik.extentions.gone
import ru.nudlik.extentions.loadPath
import ru.nudlik.extentions.reloadPath
import ru.nudlik.fragment.BaseFragment
import ru.nudlik.fragment.toast.ToastMaker
import ru.nudlik.listener.EditTextErrorCleaner
import ru.nudlik.widget.RadioGroup2Columns
import java.io.File

class NewRecipeContentFragment1 : BaseFragment() {
    private lateinit var recipeImageCard: CardView
    private lateinit var recipeMainImagePlaceholder: ImageView
    private lateinit var recipeMainImage: ImageView
    private lateinit var recipeTitle: TextInputLayout
    private lateinit var recipeSecondaryTitle: TextInputLayout
    private lateinit var recipeStateRadioGroup: RadioGroup
    private lateinit var recipeAlcoStateRadioGroup: RadioGroup
    private lateinit var recipeTypeRadioGroup: RadioGroup2Columns
    private val checkBoxes: Array<MaterialCheckBox?> = arrayOfNulls(7)
    val recipeDrinkTastes: MutableList<DrinkTaste> = ArrayList()
    var recipeDrinkState: DrinkState = DrinkState.Unspecified
    var recipeDrinkType: DrinkType = DrinkType.Lemonade
    var mainImageFile: File? = null
    var alcohol: Boolean = false

    private fun addRemoveDrinkTaste(add: Boolean, taste: DrinkTaste) {
        if (add) {
            recipeDrinkTastes.add(taste)
        } else {
            recipeDrinkTastes.remove(taste)
        }
    }

    private val checkUncheckTaste = CompoundButton.OnCheckedChangeListener { c, isChecked ->
        when (c.id) {
            R.id.recipe_taste_berries -> {
                addRemoveDrinkTaste(isChecked, DrinkTaste.Berries)
            }
            R.id.recipe_taste_fruit -> {
                addRemoveDrinkTaste(isChecked, DrinkTaste.Fruit)
            }
            R.id.recipe_taste_herb -> {
                addRemoveDrinkTaste(isChecked, DrinkTaste.Herb)
            }
            R.id.recipe_taste_sourish -> {
                addRemoveDrinkTaste(isChecked, DrinkTaste.Sour)
            }
            R.id.recipe_taste_spice -> {
                addRemoveDrinkTaste(isChecked, DrinkTaste.Spicy)
            }
            R.id.recipe_taste_tasty -> {
                addRemoveDrinkTaste(isChecked, DrinkTaste.Tasty)
            }
            R.id.recipe_taste_vegetable -> {
                addRemoveDrinkTaste(isChecked, DrinkTaste.Vegetable)
            }
        }
    }

    private val recipeTypeCheckedListener = RadioGroup2Columns.OnCheckedChangeListener { _, id ->
        when (id) {
            R.id.recipe_type_liqueur -> {
                recipeDrinkType = DrinkType.Liqueur
            }
            R.id.recipe_type_tea -> {
                recipeDrinkType = DrinkType.Tea
            }
            R.id.recipe_type_shot -> {
                recipeDrinkType = DrinkType.Shot
            }
            R.id.recipe_type_milkshake -> {
                recipeDrinkType = DrinkType.Milkshake
            }
            R.id.recipe_type_lemonade -> {
                recipeDrinkType = DrinkType.Lemonade
            }
            R.id.recipe_type_coffee -> {
                recipeDrinkType = DrinkType.Coffee
            }
            R.id.recipe_type_cocktail -> {
                recipeDrinkType = DrinkType.Cocktail
            }
        }
    }

    private val checkRecipeState = RadioGroup.OnCheckedChangeListener { _, id ->
        when (id) {
            R.id.recipe_cond_cold -> {
                recipeDrinkState = DrinkState.Cold
            }
            R.id.recipe_cond_hot -> {
                recipeDrinkState = DrinkState.Hot
            }
            R.id.recipe_cond_not_spec -> {
                recipeDrinkState = DrinkState.Unspecified
            }
        }
    }

    private val checkAlcoState = RadioGroup.OnCheckedChangeListener { _, id ->
        when (id) {
            R.id.recipe_non_alco -> {
                alcohol = false
            }
            R.id.recipe_alco -> {
                alcohol = true
            }
        }
    }

    fun pickFromGallery() {
        if (ActivityCompat.checkSelfPermission(
                activity!!,
                Manifest.permission.READ_EXTERNAL_STORAGE
            ) != PackageManager.PERMISSION_GRANTED
        ) {
            requestPermission(
                Manifest.permission.READ_EXTERNAL_STORAGE,
                getString(R.string.permission_read_storage_rationale),
                BaseActivity.REQUEST_STORAGE_READ_ACCESS_PERMISSION
            )
        } else {
            UCropHelper.pickFromGallery(activity!!, NewRecipeActivity.REQUEST_MAIN_IMAGE_MODE)
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = container?.inflate(R.layout.add_recipe_fragment_content1) ?: return super.onCreateView(inflater, container, savedInstanceState)

        recipeImageCard = view.findViewById(R.id.cardview_recipe_image)
        recipeMainImage = view.findViewById(R.id.recipe_image)
        recipeMainImage.setOnClickListener {
            NewRecipeActivity.rCode = NewRecipeActivity.REQUEST_MAIN_IMAGE_MODE
            pickFromGallery()
        }
        recipeMainImagePlaceholder = view.findViewById(R.id.recipe_image_placeholder)
        recipeTitle = view.findViewById(R.id.recipe_title_input_layout)
        recipeTitle.editText?.addTextChangedListener(EditTextErrorCleaner(recipeTitle))
        recipeSecondaryTitle = view.findViewById(R.id.secondary_recipe_title_input_layout)
        recipeSecondaryTitle.editText?.addTextChangedListener(EditTextErrorCleaner(recipeSecondaryTitle))
        recipeStateRadioGroup = view.findViewById(R.id.recipe_condition_group)
        recipeStateRadioGroup.setOnCheckedChangeListener(checkRecipeState)
        recipeAlcoStateRadioGroup = view.findViewById(R.id.recipe_has_alco_group)
        recipeAlcoStateRadioGroup.setOnCheckedChangeListener(checkAlcoState)
        recipeTypeRadioGroup = view.findViewById(R.id.recipe_type_group)
        recipeTypeRadioGroup.setOnCheckedChangeListener(recipeTypeCheckedListener)

        checkBoxes[0] = view.findViewById(R.id.recipe_taste_vegetable)
        checkBoxes[1] = view.findViewById(R.id.recipe_taste_tasty)
        checkBoxes[2] = view.findViewById(R.id.recipe_taste_spice)
        checkBoxes[3] = view.findViewById(R.id.recipe_taste_sourish)
        checkBoxes[4] = view.findViewById(R.id.recipe_taste_herb)
        checkBoxes[5] = view.findViewById(R.id.recipe_taste_fruit)
        checkBoxes[6] = view.findViewById(R.id.recipe_taste_berries)

        for (ch in checkBoxes) {
            ch?.setOnCheckedChangeListener(checkUncheckTaste)
        }

        return view
    }

    fun validate(): Boolean {
        return when {
            mainImageFile == null -> {
                ToastMaker.showShortWarningToastGravityCenter(requireContext(), R.string.choose_image_warning)
                false
            }
            recipeTitle.editText?.text!!.isEmpty() -> {
                recipeTitle.error = getString(R.string.not_be_empty)
                false
            }
            recipeTitle.editText?.text!!.length > 30 -> {
                recipeTitle.error = getString(R.string.max_length_error)
                false
            }
            recipeSecondaryTitle.editText?.text!!.length > 30 -> {
                recipeSecondaryTitle.error = getString(R.string.max_length_error)
                false
            }
            recipeDrinkTastes.isEmpty() -> {
                ToastMaker.showShortWarningToastGravityCenter(requireContext(), R.string.choose_recipe_taste_warning)
                false
            }
            else -> true
        }
    }

    private fun removeCashData() {
        mainImageFile?.delete()
    }

    fun loadImage(file: File) {
        if (mainImageFile == null) {
            mainImageFile = file
            recipeMainImagePlaceholder.gone()
            recipeMainImage.loadPath(mainImageFile!!)
        } else {
            mainImageFile = file
            recipeMainImage.reloadPath(mainImageFile!!)
        }

    }

    fun getName(): String = recipeTitle.editText!!.text.toString()
    fun getSecondaryName(): String = recipeSecondaryTitle.editText!!.text.toString()

    override fun onDestroy() {
        removeCashData()
        super.onDestroy()
    }

    companion object {
        fun createInstance() : NewRecipeContentFragment1 = NewRecipeContentFragment1()
    }
}