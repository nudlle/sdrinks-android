package ru.nudlik.fragment.newrecipe

import android.Manifest
import android.content.pm.PackageManager
import android.os.Bundle
import android.util.Patterns
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import androidx.cardview.widget.CardView
import androidx.core.app.ActivityCompat
import com.google.android.material.button.MaterialButton
import com.google.android.material.button.MaterialButtonToggleGroup
import com.google.android.material.textfield.TextInputLayout
import ru.nudlik.MainActivity
import ru.nudlik.fragment.BaseFragment
import ru.nudlik.R
import ru.nudlik.activity.BaseActivity
import ru.nudlik.activity.NewRecipeActivity
import ru.nudlik.activity.ucrop.UCropHelper
import ru.nudlik.domain.Ad
import ru.nudlik.extentions.gone
import ru.nudlik.extentions.loadPath
import ru.nudlik.extentions.reloadPath
import ru.nudlik.extentions.visible
import ru.nudlik.fragment.toast.ToastMaker
import ru.nudlik.listener.EditTextErrorCleaner
import ru.nudlik.listener.FinishWithGoto
import java.io.File

class NewRecipeContentFragment4 : BaseFragment() {
    private lateinit var recipeAdImageCard: CardView
    private lateinit var recipeAdImagePlaceholder: ImageView
    private lateinit var recipeAdImage: ImageView
    private lateinit var recipeAdLink: TextInputLayout
    private lateinit var recipeAdDescription: TextInputLayout
    private var granted: Boolean = false
    var includeAd: Boolean = true
    var adImageFile: File? = null
    var gotoBuySubscription: FinishWithGoto? = null

    fun validate(): Boolean {
        return when {
            adImageFile == null -> {
                ToastMaker.showShortWarningToastGravityCenter(requireContext(), R.string.choose_image_warning)
                false
            }
            recipeAdLink.editText?.text!!.isEmpty() -> {
                recipeAdLink.error = getString(R.string.not_be_empty)
                false
            }
            !Patterns.WEB_URL.matcher(recipeAdLink.editText?.text!!).matches() -> {
                recipeAdLink.error = getString(R.string.url_link_not_matches)
                false
            }
            recipeAdLink.editText?.text!!.length > 500 -> {
                recipeAdLink.error = getString(R.string.max_length_error)
                false
            }
            recipeAdDescription.editText?.text!!.length > 1000 -> {
                recipeAdDescription.error = getString(R.string.max_length_error)
                false
            }
            else -> true
        }
    }

    fun pickFromGallery() {
        if (ActivityCompat.checkSelfPermission(
                activity!!,
                Manifest.permission.READ_EXTERNAL_STORAGE
            ) != PackageManager.PERMISSION_GRANTED
        ) {
            requestPermission(
                Manifest.permission.READ_EXTERNAL_STORAGE,
                getString(R.string.permission_read_storage_rationale),
                BaseActivity.REQUEST_STORAGE_READ_ACCESS_PERMISSION
            )
        } else {
            UCropHelper.pickFromGallery(activity!!, NewRecipeActivity.REQUEST_AD_IMAGE_MODE)
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.add_recipe_fragment_content4, container, false)
        recipeAdImageCard = view.findViewById(R.id.cardview_recipe_ad_image)
        recipeAdImage = view.findViewById(R.id.recipe_ad_image)
        recipeAdImage.setOnClickListener {
            NewRecipeActivity.rCode = NewRecipeActivity.REQUEST_AD_IMAGE_MODE
            pickFromGallery()
        }
        recipeAdImagePlaceholder = view.findViewById(R.id.recipe_ad_image_placeholder)
        recipeAdLink = view.findViewById(R.id.recipe_ad_external_link_input_layout)
        recipeAdLink.editText?.addTextChangedListener(EditTextErrorCleaner(recipeAdLink))
        recipeAdDescription = view.findViewById(R.id.recipe_ad_description_input_layout)
        recipeAdDescription.editText?.addTextChangedListener(EditTextErrorCleaner(recipeAdDescription))

        view.findViewById<MaterialButtonToggleGroup>(R.id.recipe_ad_include).apply {
            addOnButtonCheckedListener { _, _, isChecked ->
                includeAd = isChecked
                view.findViewById<MaterialButton>(R.id.recipe_ad_add_button).apply {
                    setText(if (isChecked) R.string.recipe_ad_include_off else R.string.recipe_ad_include_on)
                }
            }
        }

        if (granted) {
            view.findViewById<LinearLayout>(R.id.buy_subscription_holder).gone()
        } else {
            recipeAdImageCard.gone()
            recipeAdLink.gone()
            recipeAdDescription.gone()
            view.findViewById<MaterialButtonToggleGroup>(R.id.recipe_ad_include).gone()
            view.findViewById<MaterialButton>(R.id.buy_subscription).apply {
                setOnClickListener {
                    gotoBuySubscription?.goto(MainActivity.GOTO_PROFILE)
                }
            }
        }
        return view
    }

    private fun removeCashData() {
        adImageFile?.delete()
    }

    override fun onDestroy() {
        removeCashData()
        super.onDestroy()
    }

    fun loadImage(file: File) {
        if (adImageFile == null) {
            adImageFile = file
            recipeAdImagePlaceholder.gone()
            recipeAdImage.loadPath(adImageFile!!)
        } else {
            adImageFile = file
            recipeAdImage.reloadPath(adImageFile!!)
        }
    }

    fun updateSubscription(granted: Boolean) {
        if (granted) {
            view?.findViewById<LinearLayout>(R.id.buy_subscription_holder)?.gone()
            recipeAdImageCard.visible()
            recipeAdLink.visible()
            recipeAdDescription.visible()
            view?.findViewById<MaterialButtonToggleGroup>(R.id.recipe_ad_include)?.visible()
        }
    }

    fun getRecipeAd(): Ad {
        val ad = Ad()
        ad.externalLink = recipeAdLink.editText?.text?.toString()
        ad.description = recipeAdDescription.editText?.text?.toString()
        return ad
    }

    companion object {
        fun createInstance(granted: Boolean): NewRecipeContentFragment4 {
            val frag = NewRecipeContentFragment4()
            frag.granted = granted
            return frag
        }
    }
}