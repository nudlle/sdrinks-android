package ru.nudlik.fragment.nav

import ru.nudlik.listener.ItemClickListener

class AddedRecipesFragment : BaseRecyclerViewFragment() {
    companion object {
        fun createInstance(list: List<String>,
                           itemClickListener: ItemClickListener?,
                           deleteClickListener: ItemClickListener?,
                           showProgress: Boolean = true): AddedRecipesFragment {
            val frag = AddedRecipesFragment()
            frag.own = true
            frag.showProgress = showProgress
            frag.addItems(list)
            frag.itemClickListener = itemClickListener
            frag.deleteClickListener = deleteClickListener
            frag.showAlertDialog = true
            return frag
        }
    }
}