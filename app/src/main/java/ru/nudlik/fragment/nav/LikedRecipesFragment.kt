package ru.nudlik.fragment.nav

import ru.nudlik.listener.ItemClickListener

class LikedRecipesFragment : BaseRecyclerViewFragment() {
    companion object {
        fun createInstance(list: List<String>,
                           itemClickListener: ItemClickListener?,
                           deleteClickListener: ItemClickListener?,
                           showProgress: Boolean = true): LikedRecipesFragment {
            val frag = LikedRecipesFragment()
            frag.addItems(list)
            frag.showProgress = showProgress
            frag.itemClickListener = itemClickListener
            frag.deleteClickListener = deleteClickListener
            return frag
        }
    }
}