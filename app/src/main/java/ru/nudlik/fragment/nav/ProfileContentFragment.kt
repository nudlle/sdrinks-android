package ru.nudlik.fragment.nav

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.RelativeLayout
import android.widget.TextView
import androidx.cardview.widget.CardView
import androidx.fragment.app.Fragment
import ru.nudlik.R
import com.google.android.material.button.MaterialButton
import ru.nudlik.MainActivity
import ru.nudlik.domain.Subscription
import ru.nudlik.domain.User
import ru.nudlik.extentions.*
import ru.nudlik.listener.BuySubscriptionListener
import ru.nudlik.listener.FinishWithGoto

class ProfileContentFragment : Fragment() {
    private lateinit var user: User
    private lateinit var parentContainer: RelativeLayout
    private lateinit var subscriptionBuyCard: CardView
    private lateinit var currentSubscriptionCard: CardView
    private lateinit var currentSubscriptionEndDate: TextView
    private var buySubscriptionListener: BuySubscriptionListener? = null
    private var gotoListener: FinishWithGoto? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = container?.inflate(R.layout.fragment_profile)
        return if (view != null) {
            parentContainer = view.findViewById(R.id.fragment_user_profile_parent_container)

            view.findViewById<TextView>(R.id.posted_recipes_count).apply {
                text = "${user.ownRecipes.size}"
            }
            view.findViewById<TextView>(R.id.liked_recipes_count).apply {
                text = "${user.likedRecipes.size}"
            }
            view.findViewById<LinearLayout>(R.id.profile_goto_liked_recipes).apply {
                setOnClickListener {
                    gotoListener?.goto(MainActivity.GOTO_LIKED_RECIPES)
                }
            }
            view.findViewById<LinearLayout>(R.id.profile_goto_own_recipes).apply {
                setOnClickListener {
                    gotoListener?.goto(MainActivity.GOTO_OWN_RECIPES)
                }
            }

            subscriptionBuyCard = view.findViewById(R.id.profile_buy_subscription)
            currentSubscriptionCard = view.findViewById(R.id.profile_current_subscription)
            currentSubscriptionEndDate = view.findViewById(R.id.profile_current_subscription_end_date)

            if (user.userSubscription != null && user.userSubscription!!.isValid()) {
                subscriptionBuyCard.gone()
                currentSubscriptionEndDate = view.findViewById(R.id.profile_current_subscription_end_date)
                currentSubscriptionEndDate.text = user.userSubscription?.endDate?.toTextDate("d MMM yyyy")
            } else {
                currentSubscriptionCard.gone()
                view.findViewById<MaterialButton>(R.id.profile_buy_subscription_button)?.apply {
                    setOnClickListener {
                        buySubscriptionListener?.buySub(150.0)
                    }
                }
            }

            view
        } else {
            super.onCreateView(inflater, container, savedInstanceState)
        }
    }

    fun subscriptionBuyed(sub: Subscription) {
        user.userSubscription = sub
        subscriptionBuyCard.gone()
        currentSubscriptionCard.visible()
        currentSubscriptionEndDate.text = user.userSubscription?.endDate?.toTextDate("d MMM yyyy")
    }

    companion object {
        fun createInstance(user: User, goto: FinishWithGoto?, bsl: BuySubscriptionListener?): ProfileContentFragment {
            val fragment = ProfileContentFragment()
            fragment.user = user
            fragment.gotoListener = goto
            fragment.buySubscriptionListener = bsl
            return fragment
        }
    }
}