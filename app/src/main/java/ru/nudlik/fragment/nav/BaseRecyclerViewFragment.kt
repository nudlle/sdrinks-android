package ru.nudlik.fragment.nav

import android.content.DialogInterface
import android.graphics.Canvas
import android.graphics.Color
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ProgressBar
import android.widget.RelativeLayout
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.ItemTouchHelper
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import ru.nudlik.R
import ru.nudlik.adapter.recipe.RecipesListAdapter
import ru.nudlik.domain.Recipe
import ru.nudlik.extentions.gone
import ru.nudlik.extentions.inflate
import ru.nudlik.extentions.runLayoutAnimation
import ru.nudlik.extentions.visible
import ru.nudlik.fragment.SwipeListItemController
import ru.nudlik.fragment.dialog.AlertDialogBuilder
import ru.nudlik.interfaces.SwipeListItemControllerAction
import ru.nudlik.listener.ItemClickListener
import ru.nudlik.response.ServerResponseCode
import ru.nudlik.service.rest.RestUtilService

abstract class BaseRecyclerViewFragment : Fragment() {
    private lateinit var recyclerView: RecyclerView
    private lateinit var noContent: RelativeLayout
    private lateinit var adapter: RecipesListAdapter
    private lateinit var restUtilService: RestUtilService
    private lateinit var progressBar: ProgressBar
    private val list = ArrayList<String>()
    private val recipesList = ArrayList<Recipe>()
    private var itemsCount: Int = 0
    protected var itemClickListener: ItemClickListener? = null
    protected var deleteClickListener: ItemClickListener? = null
    protected var showAlertDialog: Boolean = false
    protected var showProgress: Boolean = true
    protected var own: Boolean = false

    private fun showLoading(isShow: Boolean) {
        if (isShow && showProgress) {
            progressBar.visible(100)
        } else {
            progressBar.gone(100)
        }
    }

    private fun checkEndLoadingData() {
        if (itemsCount == 0) {
            showLoading(false)
            recyclerView.runLayoutAnimation(R.anim.layout_animation_from_bottom)
            checkEmptyData()
        }
    }

    private fun removeRecipe(id: String, count: Int) {
        deleteClickListener?.onItemClick(null, count, id)
    }

    private fun loadRecipes() {
        showLoading(true)
        itemsCount = list.size
        checkEndLoadingData()
        for ((i, id) in list.withIndex()) {
            restUtilService.getRecipeById(id, object : RestUtilService.ResponseCallback {
                override fun <T> success(data: T?) {
                    recipesList.add(data as Recipe)
                    itemsCount--
                    checkEndLoadingData()
                }
                override fun errorCode(responseCode: ServerResponseCode?) {
                    removeRecipe(id, i)
                    itemsCount--
                    checkEndLoadingData()
                }
                override fun exception(e: Throwable?) {
                    removeRecipe(id, i)
                    itemsCount--
                    checkEndLoadingData()
                }
            })
        }
    }

    private val filterListener = object : FilterChangesListener {
        override fun onPublishFilteredResult() {
            checkEmptyData()
        }
    }

    private val deleteItem = object : SwipeListItemControllerAction {
        override fun onLeftClicked(position: Int) {}

        override fun onRightClicked(position: Int) {
            if (showAlertDialog) {
                AlertDialogBuilder.createInfoDialogNPActions(requireActivity(), "Вы уверены?",
                    "Рецепт будет удалён", DialogInterface.OnClickListener { _, _ ->
                        val recipeId = adapter.removeAt(position)
                        checkEmptyData()
                        deleteClickListener?.onItemClick(null , position, recipeId)
                    }).show()
            } else {
                val recipeId = adapter.removeAt(position)
                checkEmptyData()
                deleteClickListener?.onItemClick(null , position, recipeId)
            }
        }
    }

    fun addItems(newData: List<String>) {
        this.list.addAll(newData)
    }

    fun updateAt(position: Int) {
        adapter.notifyItemChanged(position)
    }

    fun filter(text: String?) {
        adapter.filter.filter(text)
        checkEmptyData()
    }

    private fun checkEmptyData() {
        if (adapter.itemCount == 0) noContent.visible(100) else noContent.gone(100)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = container?.inflate(R.layout.fragment_recipes_list)
        return if (view != null) {
            restUtilService = RestUtilService(view.context)
            restUtilService.showExceptionDialog = false
            progressBar = view.findViewById(R.id.loading_progress)
            noContent = view.findViewById(R.id.no_content_holder)
            recyclerView = view.findViewById(R.id.recipe_recycler_view)
            adapter = RecipesListAdapter(recipesList, recipesList, itemClickListener, own)
            adapter.filterCallback = filterListener
            recyclerView.layoutManager = LinearLayoutManager(context)
            recyclerView.adapter = adapter
            //recyclerView.addItemDecoration(DividerItemDecoration(view.context, DividerItemDecoration.VERTICAL))

            val swipeListItemController = SwipeListItemController(ItemTouchHelper.LEFT)
            swipeListItemController.buttonsActions = deleteItem
            swipeListItemController.rightResIcon = R.drawable.ic_clear_black_48dp
            swipeListItemController.rightBackgroundColor = Color.rgb(255, 102, 102)
            swipeListItemController.cornerRadius = 0f

            val touchHelper = ItemTouchHelper(swipeListItemController)
            touchHelper.attachToRecyclerView(recyclerView)

            recyclerView.addItemDecoration(object : RecyclerView.ItemDecoration() {
                override fun onDraw(c: Canvas, parent: RecyclerView, state: RecyclerView.State) {
                    swipeListItemController.onDraw(c)
                }
            })

            loadRecipes()

            view
        } else {
            super.onCreateView(inflater, container, savedInstanceState)
        }
    }

    override fun onDestroy() {
        restUtilService.destroy()
        super.onDestroy()
    }

    interface FilterChangesListener {
        fun onPublishFilteredResult()
    }
}