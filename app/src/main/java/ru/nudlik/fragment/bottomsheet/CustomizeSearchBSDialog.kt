package ru.nudlik.fragment.bottomsheet

import android.app.Dialog
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.CompoundButton
import android.widget.RadioGroup
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import com.google.android.material.checkbox.MaterialCheckBox
import kotlinx.android.synthetic.main.bs_customize_search_content.view.*
import ru.nudlik.R
import ru.nudlik.domain.DrinkState
import ru.nudlik.domain.DrinkTaste
import ru.nudlik.domain.DrinkType
import ru.nudlik.singleton.SearchAddOnsSingleton
import ru.nudlik.widget.RadioGroup2Columns

class CustomizeSearchBSDialog : BottomSheetDialogFragment() {
    private val recipeTastes = arrayOfNulls<MaterialCheckBox?>(7)
    private lateinit var recipeType: RadioGroup2Columns
    private lateinit var recipeState: RadioGroup
    private lateinit var isAlcohol: RadioGroup

    private val tasteCheckedListener = CompoundButton.OnCheckedChangeListener { button, checked ->
        when (button.id) {
            R.id.search_recipe_taste_herb -> {
                SearchAddOnsSingleton.addDeleteTaste(DrinkTaste.Herb, checked)
            }
            R.id.search_recipe_taste_berries -> {
                SearchAddOnsSingleton.addDeleteTaste(DrinkTaste.Berries, checked)
            }
            R.id.search_recipe_taste_sourish -> {
                SearchAddOnsSingleton.addDeleteTaste(DrinkTaste.Sour, checked)
            }
            R.id.search_recipe_taste_vegetable -> {
                SearchAddOnsSingleton.addDeleteTaste(DrinkTaste.Vegetable, checked)
            }
            R.id.search_recipe_taste_fruit -> {
                SearchAddOnsSingleton.addDeleteTaste(DrinkTaste.Fruit, checked)
            }
            R.id.search_recipe_taste_spice -> {
                SearchAddOnsSingleton.addDeleteTaste(DrinkTaste.Spicy, checked)
            }
            R.id.search_recipe_taste_tasty -> {
                SearchAddOnsSingleton.addDeleteTaste(DrinkTaste.Tasty, checked)
            }
        }
    }

    private val typeCheckedListener = RadioGroup2Columns.OnCheckedChangeListener { _, id ->
        when (id) {
            R.id.search_recipe_type_lemonade -> SearchAddOnsSingleton.type = DrinkType.Lemonade
            R.id.search_recipe_type_tea -> SearchAddOnsSingleton.type = DrinkType.Tea
            R.id.search_recipe_type_coffee -> SearchAddOnsSingleton.type = DrinkType.Coffee
            R.id.search_recipe_type_milkshake -> SearchAddOnsSingleton.type = DrinkType.Milkshake
            R.id.search_recipe_type_cocktail -> SearchAddOnsSingleton.type = DrinkType.Cocktail
            R.id.search_recipe_type_shot -> SearchAddOnsSingleton.type = DrinkType.Shot
            R.id.search_recipe_type_liqueur -> SearchAddOnsSingleton.type = DrinkType.Liqueur
            R.id.search_recipe_type_not_spec -> SearchAddOnsSingleton.type = null
        }
    }

    private val stateCheckedListener = RadioGroup.OnCheckedChangeListener { _, id ->
        when (id) {
            R.id.search_recipe_cond_cold -> SearchAddOnsSingleton.state = DrinkState.Cold
            R.id.search_recipe_cond_hot -> SearchAddOnsSingleton.state = DrinkState.Hot
            R.id.search_recipe_cond_not_spec -> SearchAddOnsSingleton.state = null
        }
    }

    private val alcoholCheckedListener = RadioGroup.OnCheckedChangeListener { _, id ->
        when (id) {
            R.id.search_recipe_alco -> SearchAddOnsSingleton.alcohol = true
            R.id.search_recipe_non_alco -> SearchAddOnsSingleton.alcohol = false
        }
    }

    private fun initWidgets() {
        recipeTastes[0]?.isChecked = SearchAddOnsSingleton.tastes.contains(DrinkTaste.Tasty)
        recipeTastes[1]?.isChecked = SearchAddOnsSingleton.tastes.contains(DrinkTaste.Spicy)
        recipeTastes[2]?.isChecked = SearchAddOnsSingleton.tastes.contains(DrinkTaste.Fruit)
        recipeTastes[3]?.isChecked = SearchAddOnsSingleton.tastes.contains(DrinkTaste.Vegetable)
        recipeTastes[4]?.isChecked = SearchAddOnsSingleton.tastes.contains(DrinkTaste.Sour)
        recipeTastes[5]?.isChecked = SearchAddOnsSingleton.tastes.contains(DrinkTaste.Berries)
        recipeTastes[6]?.isChecked = SearchAddOnsSingleton.tastes.contains(DrinkTaste.Herb)

        when (SearchAddOnsSingleton.type) {
            DrinkType.Lemonade -> recipeType.checkFirstRG(R.id.search_recipe_type_lemonade)
            DrinkType.Tea -> recipeType.checkFirstRG(R.id.search_recipe_type_tea)
            DrinkType.Coffee -> recipeType.checkFirstRG(R.id.search_recipe_type_coffee)
            DrinkType.Milkshake -> recipeType.checkFirstRG(R.id.search_recipe_type_milkshake)
            DrinkType.Cocktail -> recipeType.checkSecondRG(R.id.search_recipe_type_cocktail)
            DrinkType.Shot -> recipeType.checkSecondRG(R.id.search_recipe_type_shot)
            DrinkType.Liqueur -> recipeType.checkSecondRG(R.id.search_recipe_type_liqueur)
            else -> recipeType.checkSecondRG(R.id.search_recipe_type_not_spec)
        }

        when (SearchAddOnsSingleton.state) {
            DrinkState.Hot -> recipeState.check(R.id.search_recipe_cond_hot)
            DrinkState.Cold -> recipeState.check(R.id.search_recipe_cond_cold)
            else -> recipeState.check(R.id.search_recipe_cond_not_spec)
        }

        when (SearchAddOnsSingleton.alcohol) {
            true -> isAlcohol.check(R.id.search_recipe_alco)
            false -> isAlcohol.check(R.id.search_recipe_non_alco)
            else -> isAlcohol.check(R.id.search_recipe_alco_not_spec)
        }
    }

    override fun getTheme(): Int = R.style.AppTheme_BottomSheetDialog_Custom

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.bs_customize_search_content, container, false)

        recipeTastes[0] = view.findViewById(R.id.search_recipe_taste_tasty)
        recipeTastes[1] = view.findViewById(R.id.search_recipe_taste_spice)
        recipeTastes[2] = view.findViewById(R.id.search_recipe_taste_fruit)
        recipeTastes[3] = view.findViewById(R.id.search_recipe_taste_vegetable)
        recipeTastes[4] = view.findViewById(R.id.search_recipe_taste_sourish)
        recipeTastes[5] = view.findViewById(R.id.search_recipe_taste_berries)
        recipeTastes[6] = view.findViewById(R.id.search_recipe_taste_herb)

        recipeType = view.findViewById(R.id.search_recipe_type_group)
        recipeState = view.findViewById(R.id.search_recipe_condition_group)
        isAlcohol = view.findViewById(R.id.search_recipe_has_alco_group)

        initWidgets()

        for (recipeTaste in recipeTastes) {
            recipeTaste?.setOnCheckedChangeListener(tasteCheckedListener)
        }

        recipeType.setOnCheckedChangeListener(typeCheckedListener)
        recipeState.setOnCheckedChangeListener(stateCheckedListener)
        isAlcohol.setOnCheckedChangeListener(alcoholCheckedListener)

        return view
    }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog = BottomSheetDialogCustom(requireContext(), theme)

    override fun onDestroy() {
        for (recipeTaste in recipeTastes) {
            recipeTaste?.setOnCheckedChangeListener(null)
        }
        recipeType.setOnCheckedChangeListener(null)
        recipeState.setOnCheckedChangeListener(null)
        isAlcohol.setOnCheckedChangeListener(null)
        super.onDestroy()
    }

    companion object {
        const val TAG = "customize_search_bs_dialog"
        fun createInstance(): CustomizeSearchBSDialog {
            val frag = CustomizeSearchBSDialog()
            return frag
        }
    }
}