package ru.nudlik.fragment.dialog

import android.app.AlertDialog
import android.content.Context
import android.content.DialogInterface
import ru.nudlik.R

interface AlertDialogBuilder {
    companion object Builder {
        fun createInfoDialog(context: Context?, title: Int, body: Int): AlertDialog {
            return AlertDialog.Builder(context)
                .setTitle(title)
                .setMessage(body)
                .setCancelable(false)
                .setPositiveButton(R.string.ok, null).create()
        }
        fun createInfoDialog(context: Context?, title: Int, body: String): AlertDialog {
            return AlertDialog.Builder(context)
                .setTitle(title)
                .setMessage(body)
                .setCancelable(false)
                .setPositiveButton(R.string.ok, null).create()
        }
        fun createInfoDialogPosAct(context: Context?, title: Int, body: Int,
                                   okButton: DialogInterface.OnClickListener): AlertDialog {
            return AlertDialog.Builder(context)
                .setTitle(title)
                .setMessage(body)
                .setCancelable(false)
                .setPositiveButton(R.string.ok, okButton).create()
        }
        fun createInfoDialogPosAct(context: Context?, title: Int, body: String,
                                   okButton: DialogInterface.OnClickListener): AlertDialog {
            return AlertDialog.Builder(context)
                .setTitle(title)
                .setMessage(body)
                .setCancelable(false)
                .setPositiveButton(R.string.ok, okButton).create()
        }
        fun createInfoDialogPosAct(context: Context?, title: String, body: String,
                                   okButton: DialogInterface.OnClickListener): AlertDialog {
            return AlertDialog.Builder(context)
                .setTitle(title)
                .setMessage(body)
                .setCancelable(false)
                .setPositiveButton(R.string.ok, okButton).create()
        }
        fun createInfoDialogNPActions(context: Context?, title: String, body: String,
                                      okButton: DialogInterface.OnClickListener): AlertDialog {
            return AlertDialog.Builder(context)
                .setTitle(title)
                .setMessage(body)
                .setCancelable(false)
                .setNegativeButton(R.string.cancel, null)
                .setPositiveButton(R.string.ok, okButton).create()
        }
        fun createInfoDialogNPActions(context: Context?, title: Int, body: Int,
                                      cancelButton: DialogInterface.OnClickListener,
                                      okButton: DialogInterface.OnClickListener): AlertDialog {
            return AlertDialog.Builder(context)
                .setTitle(title)
                .setMessage(body)
                .setCancelable(false)
                .setNegativeButton(R.string.cancel, cancelButton)
                .setPositiveButton(R.string.ok, okButton).create()
        }
        fun createCrashAppDialog(context: Context?, title: Int, body: Int): AlertDialog {
            return AlertDialog.Builder(context)
                .setTitle(title)
                .setMessage(body)
                .setCancelable(false)
                .setPositiveButton(R.string.ok) { _,_ ->
                    System.exit(0)
                }.create()
        }
        fun createCrashAppDialog(context: Context?, title: Int, body: String): AlertDialog {
            return AlertDialog.Builder(context)
                .setTitle(title)
                .setMessage(body)
                .setCancelable(false)
                .setPositiveButton(R.string.ok) { _,_ ->
                    System.exit(0)
                }.create()
        }
    }
}