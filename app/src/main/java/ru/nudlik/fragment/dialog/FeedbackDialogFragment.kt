package ru.nudlik.fragment.dialog

import android.app.AlertDialog
import android.app.Dialog
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.RatingBar
import androidx.annotation.Nullable
import androidx.fragment.app.DialogFragment
import com.google.android.material.textfield.TextInputLayout
import ru.nudlik.R
import ru.nudlik.domain.UserComment
import ru.nudlik.interfaces.DialogButtonsListener
import ru.nudlik.listener.EditTextErrorCleaner
import ru.nudlik.service.rest.RestUtilService

class FeedbackDialogFragment : DialogFragment() {
    private lateinit var recipeId: String
    private lateinit var userId: String
    private lateinit var userEmail: String
    private lateinit var ratingBar: RatingBar
    private lateinit var feedback: TextInputLayout
    private lateinit var restUtilService: RestUtilService
    var buttonsClickListener: DialogButtonsListener? = null

    private fun isValid(): Boolean {
        return when {
            feedback.editText?.text?.length!! == 0 -> {
                feedback.error = resources.getString(R.string.not_be_empty)
                false
            }
            feedback.editText?.text?.length!! > 1000 -> {
                feedback.error = resources.getString(R.string.max_length_error)
                false
            }
            else -> true
        }
    }

    private fun onCreateDialogView(inflater: LayoutInflater, @Nullable container: ViewGroup?, @Nullable savedInstanceState: Bundle?): View? =
        inflater.inflate(R.layout.feedback_dialog_fragment, container)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        ratingBar = view.findViewById(R.id.add_recipe_rate)
        feedback = view.findViewById(R.id.add_recipe_comment_input_layout)
        feedback.editText?.addTextChangedListener(EditTextErrorCleaner(feedback))
    }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val builder = AlertDialog.Builder(requireContext())
        builder.setPositiveButton("Отправить") { _, _ ->
            if (isValid()) {
                val comment = UserComment()
                comment.rate = ratingBar.rating.toDouble()
                comment.comment = feedback.editText?.text.toString()
                comment.recipeId = recipeId
                comment.userId = userId
                comment.userEmail = userEmail
                restUtilService.postComment(comment, object : RestUtilService.ResponseCallback {
                    override fun <T> success(data: T?) {
                        buttonsClickListener?.positiveClick(data as UserComment)
                    }
                })
            }
        }
        builder.setNegativeButton("Отмена", null)
        val view = onCreateDialogView(activity!!.layoutInflater, null, null)
        onViewCreated(view!!, null)
        builder.setView(view)
        return builder.create()
    }

    companion object {
        fun createInstance(recipeId: String, userId: String, userEmail: String, restUtilService: RestUtilService): FeedbackDialogFragment {
            val dialog = FeedbackDialogFragment()
            dialog.recipeId = recipeId
            dialog.userId = userId
            dialog.userEmail = userEmail
            dialog.restUtilService = restUtilService
            dialog.isCancelable = false
            return dialog
        }
    }
}