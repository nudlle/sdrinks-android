package ru.nudlik.fragment.dialog

import android.app.AlertDialog
import android.app.Dialog
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.Nullable
import androidx.fragment.app.DialogFragment
import com.google.android.material.textfield.TextInputLayout
import ru.nudlik.R
import ru.nudlik.domain.UserMessage
import ru.nudlik.interfaces.DialogButtonsListener
import ru.nudlik.listener.EditTextErrorCleaner
import ru.nudlik.service.rest.RestUtilService

class SendMessageDialogFragment : DialogFragment() {
    private lateinit var appErrorInputLayout: TextInputLayout
    private lateinit var userId: String
    private lateinit var appVersion: String
    private lateinit var restUtilService: RestUtilService
    var buttonsClickListener: DialogButtonsListener? = null

    private fun isValid(): Boolean {
        return when {
            appErrorInputLayout.editText?.text?.length!! == 0 -> {
                appErrorInputLayout.error = resources.getString(R.string.not_be_empty)
                false
            }
            appErrorInputLayout.editText?.text?.length!! > 1000 -> {
                appErrorInputLayout.error = resources.getString(R.string.max_length_error)
                false
            }
            else -> true
        }
    }

    private fun onCreateDialogView(inflater: LayoutInflater, @Nullable container: ViewGroup?, @Nullable savedInstanceState: Bundle?): View? =
        inflater.inflate(R.layout.app_error_dialog_fragment, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        appErrorInputLayout = view.findViewById(R.id.submit_app_error_input_layout)
        appErrorInputLayout.editText?.addTextChangedListener(EditTextErrorCleaner(appErrorInputLayout))
    }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val builder = AlertDialog.Builder(requireContext())
        builder.setTitle(R.string.user_message_dialog_title)
        builder.setPositiveButton("Отправить") { _, _ ->
            if (isValid()) {
                val appError = UserMessage()
                appError.appVersion = appVersion
                appError.message = appErrorInputLayout.editText?.text.toString()
                appError.userId = userId
                restUtilService.postAppError(appError, object : RestUtilService.ResponseCallback {
                    override fun <T> success(data: T?) {
                        buttonsClickListener?.positiveClick()
                    }
                })
            }
        }
        builder.setNegativeButton("Отмена", null)
        val view = onCreateDialogView(activity!!.layoutInflater, null, null)
        onViewCreated(view!!, null)
        builder.setView(view)
        return builder.create()
    }

    companion object {
        fun createInstance(userId: String, appVersion: String, restUtilService: RestUtilService): SendMessageDialogFragment {
            val dialog = SendMessageDialogFragment()
            dialog.userId = userId
            dialog.appVersion = appVersion
            dialog.restUtilService = restUtilService
            dialog.isCancelable = false
            return dialog
        }
    }
}