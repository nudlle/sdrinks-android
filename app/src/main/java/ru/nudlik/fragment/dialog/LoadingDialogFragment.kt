package ru.nudlik.fragment.dialog

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.fragment.app.DialogFragment
import ru.nudlik.R

class LoadingDialogFragment : DialogFragment() {
    private var content: TextView? = null
    private var contentMsg: String? = null
    private lateinit var title: String

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        retainInstance = false
        setStyle(STYLE_NORMAL, R.style.AppTheme_Dialog)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        dialog?.setTitle(title)
        val view = inflater.inflate(R.layout.loading_dialog_view, container, false)
        content = view.findViewById(R.id.loading_content_progress_text)
        content?.text = contentMsg
        return view
    }

    fun setContent(content: String) {
        contentMsg = content
        if (this.content != null) {
            this.content!!.text = contentMsg
        }

    }

    companion object {
        const val TAG = "loading_dialog_tag"
        fun createInstance(title: String): LoadingDialogFragment {
            val dialog = LoadingDialogFragment()
            dialog.title = title
            return dialog
        }
    }
}