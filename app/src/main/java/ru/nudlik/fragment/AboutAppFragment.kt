package ru.nudlik.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import ru.nudlik.R

class AboutAppFragment : BaseFragment() {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? =
        inflater.inflate(R.layout.fragment_about, container, false)

    companion object {
        const val TAG = "about_app_fragment_tag"
        fun createInstance(): AboutAppFragment {
            return AboutAppFragment()
        }
    }
}