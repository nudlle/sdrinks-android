package ru.nudlik.fragment.toast

import android.content.Context
import android.graphics.PorterDuff
import android.view.Gravity
import android.widget.Toast
import ru.nudlik.R

interface ToastMaker {
    companion object {
        private fun createToast(ctx: Context, msg: String?, duration: Int): Toast {
            return Toast.makeText(ctx, msg, duration)
        }
        private fun createToast(ctx: Context, msgRes: Int, duration: Int): Toast {
            return Toast.makeText(ctx, msgRes, duration)
        }
        fun showLongToast(ctx: Context, msg: String?) {
            createToast(ctx, msg, Toast.LENGTH_LONG).show()
        }
        fun showLongToast(ctx: Context, msgRes: Int) {
            createToast(ctx, msgRes, Toast.LENGTH_LONG).show()
        }
        fun showShortToast(ctx: Context, msg: String?) {
            createToast(ctx, msg, Toast.LENGTH_SHORT).show()
        }
        fun showShortToast(ctx: Context, msgRes: Int) {
            createToast(ctx, msgRes, Toast.LENGTH_SHORT).show()
        }
        fun showShortWarningToastGravityCenter(ctx: Context, msgRes: Int) {
            val toast = createToast(ctx, msgRes, Toast.LENGTH_SHORT)
            toast.setGravity(Gravity.CENTER, 0, 0)
            toast.view.background.setColorFilter(ctx.getColor(R.color.colorDanger), PorterDuff.Mode.SRC_IN)
            toast.show()
        }
        fun showShortWarningToastGravityCenter(ctx: Context, msg: String) {
            val toast = createToast(ctx, msg, Toast.LENGTH_SHORT)
            toast.setGravity(Gravity.CENTER, 0, 0)
            toast.view.background.setColorFilter(ctx.getColor(R.color.colorDanger), PorterDuff.Mode.SRC_IN)
            toast.show()
        }
    }
}