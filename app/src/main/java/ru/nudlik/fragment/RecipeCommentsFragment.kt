package ru.nudlik.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import ru.nudlik.R
import ru.nudlik.adapter.recipe.RecipeCommentsAdapter
import ru.nudlik.domain.UserComment
import ru.nudlik.extentions.gone
import ru.nudlik.extentions.visible
import ru.nudlik.interfaces.LoadNextPageCallback
import ru.nudlik.interfaces.RequestNextPageListener

class RecipeCommentsFragment : BaseFragment() {
    private lateinit var recipeId: String
    private lateinit var recyclerView: RecyclerView
    private lateinit var adapter: RecipeCommentsAdapter
    private lateinit var noContent: TextView
    private val comments = ArrayList<UserComment?>()
    private var loading: Boolean = false
    private var loadListener: RequestNextPageListener? = null

    fun loadMore() {
        comments.add(null)
        adapter.notifyItemInserted(comments.size - 1)
    }

    fun addItems(newData: List<UserComment>) {
        this.comments.addAll(newData)
        this.adapter.notifyDataSetChanged()
    }

    fun addItem(item: UserComment) {
        this.adapter.addItem(item)
    }

    private val loadCallback = object : LoadNextPageCallback {
        override fun loadFinished() {
            loading = false
        }
    }

    fun checkEmptyData() {
        if (adapter.itemCount == 0) noContent.visible(100) else noContent.gone(100)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment_comments_list, container, false)

        noContent = view.findViewById(R.id.recipe_comments_no_content)
        recyclerView = view.findViewById(R.id.recipe_comments_recycler_view)
        adapter = RecipeCommentsAdapter(comments)
        adapter.setRequestNextPageListener(loadListener)
        adapter.setLoadCallback(loadCallback)
        recyclerView.layoutManager = LinearLayoutManager(context)
        recyclerView.adapter = adapter

        recyclerView.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                super.onScrolled(recyclerView, dx, dy)

                val llm = recyclerView.layoutManager as LinearLayoutManager
                val totalItemCount = llm.itemCount
                val lastVisibleItem = llm.findLastCompletelyVisibleItemPosition()
                if (!loading && totalItemCount <= (lastVisibleItem + 5)) {
                    if (adapter.doLoad()) {
                        loadMore()
                        loading = true
                    }
                }
            }
        })

        checkEmptyData()

        return view
    }

    companion object {
        fun createInstance(recipeId: String, l: RequestNextPageListener?) : RecipeCommentsFragment {
            val frag = RecipeCommentsFragment()
            frag.recipeId = recipeId
            frag.loadListener = l
            return frag
        }
    }
}