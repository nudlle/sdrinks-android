package ru.nudlik.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import ru.nudlik.R
import ru.nudlik.adapter.recipe.SearchedRecipesListAdapter
import ru.nudlik.domain.Recipe
import ru.nudlik.extentions.gone
import ru.nudlik.extentions.runLayoutAnimation
import ru.nudlik.extentions.visible
import ru.nudlik.interfaces.LoadNextPageCallback
import ru.nudlik.interfaces.RequestNextPageListener
import ru.nudlik.listener.ItemClickListener

class RecipeSearchFragment : BaseFragment() {
    private lateinit var recyclerView: RecyclerView
    private lateinit var adapter: SearchedRecipesListAdapter
    private lateinit var noContent: TextView
    private var clickListener: ItemClickListener? = null
    private var requestNextPageListener: RequestNextPageListener? = null
    private val list: MutableList<Recipe?> = ArrayList()
    private var loading: Boolean = false
    
    fun loadMore() {
        list.add(null)
        adapter.notifyItemInserted(list.size - 1)
    }

    fun clearAll() {
        adapter.clearAdapter()
    }

    fun addNewData(list: List<Recipe>) {
        adapter.addItems(list)
    }

    fun updateAt(recipe: Recipe, pos: Int) {
        list[pos] = recipe
        adapter.notifyItemChanged(pos)
    }

    fun checkEmptyData() {
        if (adapter.itemCount == 0) noContent.visible(100) else noContent.gone(100)
    }

    private val loadCallback = object : LoadNextPageCallback {
        override fun loadFinished() {
            loading = false
        }
    }
    
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.searched_recipe_fragment, container, false)
        recyclerView = view.findViewById(R.id.searched_recipe_recycler_view)
        noContent = view.findViewById(R.id.searched_recipe_no_content)

        adapter = SearchedRecipesListAdapter(list, clickListener)
        adapter.setRequestNextPageListener(requestNextPageListener)
        adapter.setLoadCallback(loadCallback)

        recyclerView.setHasFixedSize(false)
        recyclerView.layoutManager = LinearLayoutManager(context)
        recyclerView.adapter = adapter

        recyclerView.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                super.onScrolled(recyclerView, dx, dy)

                val llm = recyclerView.layoutManager as LinearLayoutManager
                val totalItemCount = llm.itemCount
                val lastVisibleItem = llm.findLastCompletelyVisibleItemPosition()
                if (!loading && totalItemCount <= (lastVisibleItem + 5)) {
                    if (adapter.doLoad()) {
                        loadMore()
                        loading = true
                    }
                }
            }
        })
        checkEmptyData()
        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        recyclerView.runLayoutAnimation(R.anim.layout_animation_from_bottom)
    }

    companion object {
        const val TAG = "recipe_search_fragment"
        fun createInstance(clickListener: ItemClickListener?,
                           loadNextPageListener: RequestNextPageListener?): RecipeSearchFragment {
            val frag = RecipeSearchFragment()
            frag.clickListener = clickListener
            frag.requestNextPageListener = loadNextPageListener
            return frag
        }
    }
}