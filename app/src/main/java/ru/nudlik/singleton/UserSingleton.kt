package ru.nudlik.singleton

import android.content.ContentResolver
import ru.nudlik.domain.User
import ru.nudlik.provider.utils.ProviderHelper
import ru.nudlik.security.CryptoUtilImpl

object UserSingleton {
    private var user: User? = null
    private var changes = false;

    fun initUser(contentResolver: ContentResolver) {
        this.user = ProviderHelper.restoreUserData(contentResolver)
    }

    fun initUser(user: User) {
        this.user = user
    }

    fun changeUser(user: User) {
        userChanged()
        this.user = user
    }

    fun updateUserInDb(contentResolver: ContentResolver) {
        user?.apply {
            ProviderHelper.updateUserData(this, contentResolver)
        }
    }

    private fun userChanged() {
        this.changes = true;
    }

    fun isChanged(): Boolean {
        if (this.changes) {
            this.changes = false
            return true
        }
        return false
    }

    fun storeUserInDb(contentResolver: ContentResolver) {
        user?.apply {
            ProviderHelper.storeUserData(this, CryptoUtilImpl(), contentResolver)
        }
    }

    fun deleteUserInDb(contentResolver: ContentResolver) {
        user?.apply {
            ProviderHelper.deleteUser(this, contentResolver)
        }
        user = null
    }

    fun getUser(): User? = user
}