package ru.nudlik.singleton

import ru.nudlik.domain.Recipe

object CurrentActiveRecipe {
    var recipe: Recipe? = null
}