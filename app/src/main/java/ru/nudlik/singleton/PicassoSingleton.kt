package ru.nudlik.singleton

import android.content.Context
import com.squareup.picasso.OkHttp3Downloader
import com.squareup.picasso.Picasso

object PicassoSingleton {
    fun init(context: Context) {
        val httpClient = OkHttpClientSingleton.getHttpClient()
        val picassoBuilder = Picasso.Builder(context)
        val httpDownloader = OkHttp3Downloader(httpClient)
        picassoBuilder.downloader(httpDownloader)
        Picasso.setSingletonInstance(picassoBuilder.build())
    }
}