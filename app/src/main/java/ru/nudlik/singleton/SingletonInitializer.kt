package ru.nudlik.singleton

import android.content.Context
import okhttp3.Interceptor

object SingletonInitializer {
    private var isNotInit = true
    fun init(ctx: Context, vararg httpInterceptors: Interceptor = emptyArray()) {
        if (isNotInit) {
            OkHttpClientSingleton.create(ctx, *httpInterceptors)
            RestClientSingleton.init()
            PicassoSingleton.init(ctx)
            isNotInit = false
        }
    }
    fun addCredentials(name: String, pwd: String) {
        OkHttpClientSingleton.setCredentials(name, pwd)
    }
    fun deleteCredentials() {
        OkHttpClientSingleton.deleteCredentials()
    }
}