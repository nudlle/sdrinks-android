package ru.nudlik.singleton

import android.content.Context
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import ru.nudlik.R
import ru.nudlik.service.rest.BasicAuthInterceptor
import ru.nudlik.service.rest.UserAgentInterceptor
import java.security.KeyStore
import java.security.cert.CertificateFactory
import java.util.concurrent.TimeUnit
import javax.net.ssl.SSLContext
import javax.net.ssl.TrustManagerFactory
import javax.net.ssl.X509TrustManager

object OkHttpClientSingleton {
    private var httpClient: OkHttpClient? = null
    private val basicAuthInterceptor = BasicAuthInterceptor()

    fun create(ctx: Context, vararg interceptors: Interceptor = emptyArray()) {
        val logging = HttpLoggingInterceptor()
        logging.level = HttpLoggingInterceptor.Level.BODY
        val tmf = getTrustManagerFactory(ctx)
        val builder = OkHttpClient.Builder()
            .addInterceptor(logging)
            .addInterceptor(UserAgentInterceptor("mf1.0"))
            .connectTimeout(20, TimeUnit.SECONDS)
            .writeTimeout(20, TimeUnit.SECONDS)
            .readTimeout(30, TimeUnit.SECONDS)
            .sslSocketFactory(getSslConfig(tmf).socketFactory, tmf.trustManagers[0] as X509TrustManager)
            .hostnameVerifier { _, _ -> true }
        for (ppp in interceptors) {
            builder.addInterceptor(ppp)
        }
        builder.addInterceptor(basicAuthInterceptor)
        httpClient = builder.build()
    }

    fun setCredentials(name: String, pwd: String) {
        basicAuthInterceptor.addCredentials(name, pwd)
    }

    fun deleteCredentials() {
        basicAuthInterceptor.deleteCredintials()
    }

    private fun getTrustManagerFactory(ctx: Context): TrustManagerFactory {
        val factory = CertificateFactory.getInstance("X.509")
        val ca = factory.generateCertificate(ctx.resources.openRawResource(R.raw.sdrinks))
        val keystore = KeyStore.getInstance(KeyStore.getDefaultType())
        keystore.load(null, null)
        keystore.setCertificateEntry("sdrinks", ca)
        val tmf = TrustManagerFactory.getInstance(TrustManagerFactory.getDefaultAlgorithm())
        tmf.init(keystore)
        return tmf
    }
    private fun getSslConfig(tmf: TrustManagerFactory): SSLContext {
        val sslContext = SSLContext.getInstance("TLS")
        sslContext.init(null, tmf.trustManagers, null)
        return sslContext
    }

    fun getHttpClient(): OkHttpClient {
        return httpClient!!
    }
}