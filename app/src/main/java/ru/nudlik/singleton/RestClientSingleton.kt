package ru.nudlik.singleton

import com.google.gson.GsonBuilder
import com.google.gson.JsonDeserializationContext
import com.google.gson.JsonDeserializer
import com.google.gson.JsonElement
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import ru.nudlik.service.rest.RestClient
import java.lang.reflect.Type
import java.util.*

object RestClientSingleton {
    private var restClient: RestClient? = null

    fun init() {
        val httpClient = OkHttpClientSingleton.getHttpClient()

        val gson = GsonBuilder()
            .registerTypeAdapter(Date::class.java, JsonDateDeserializer()).create()
        val retrofit = Retrofit.Builder()
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .addConverterFactory(GsonConverterFactory.create(gson))
            .client(httpClient)
            .baseUrl(RestClient.URL)
            .build()
        restClient = retrofit.create(RestClient::class.java)
    }

    private class JsonDateDeserializer : JsonDeserializer<Date> {
        override fun deserialize(json: JsonElement?, typeOfT: Type?, context: JsonDeserializationContext?): Date {
            return Date(json?.asJsonPrimitive!!.asLong)
        }
    }

    fun getRestClientInstance(): RestClient {
        return restClient!!
    }
}