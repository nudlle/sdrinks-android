package ru.nudlik.singleton

import ru.nudlik.domain.DrinkState
import ru.nudlik.domain.DrinkTaste
import ru.nudlik.domain.DrinkType
import ru.nudlik.domain.RecipesToSearch

object SearchAddOnsSingleton {
    var state: DrinkState? = null
    var type: DrinkType? = null
    var tastes: MutableList<DrinkTaste> = ArrayList()
    var alcohol: Boolean? = null

    fun addDeleteTaste(taste: DrinkTaste, add: Boolean) {
        if (add && !tastes.contains(taste)) {
            tastes.add(taste)
        } else {
            tastes.remove(taste)
        }
    }

    fun getTextIndex() : StringBuilder {
        val builder = StringBuilder()
        state?.apply {
            builder.append(this).append(" ")
        }
        type?.apply {
            builder.append(this).append(" ")
        }
        for (taste in tastes) {
            builder.append(taste).append(" ")
        }
        alcohol?.apply {
            builder.append("Alco").append(" ")
        }
        return builder
    }

    fun clearSearchData() {
        state = null
        type = null
        tastes.clear()
    }

    fun getSearchBody() : RecipesToSearch {
        val recipesToSearch = RecipesToSearch()
        recipesToSearch.type = type
        recipesToSearch.state = state
        recipesToSearch.tastes = tastes
        recipesToSearch.alcohol = alcohol
        return recipesToSearch
    }
}