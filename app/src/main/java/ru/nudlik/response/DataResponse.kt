package ru.nudlik.response

import java.io.Serializable
import java.util.Objects

abstract class DataResponse<T> : Serializable {
    var data: T? = null
    var code: ServerResponseCode? = null

    constructor()

    @JvmOverloads
    constructor(data: T?, code: ServerResponseCode = ServerResponseCode.OK_RESPONSE) {
        this.data = data
        this.code = code
    }

    constructor(code: ServerResponseCode) : this(null, code)

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (other == null || javaClass != other.javaClass) return false
        val that = other as DataResponse<*>?
        return data == that!!.data && code == that.code
    }

    override fun hashCode(): Int {
        return Objects.hash(data, code)
    }

    override fun toString(): String {
        return "DataResponse{" +
                "data=" + data +
                ", code=" + code +
                '}'.toString()
    }

    companion object {
        private const val serialVersionUID = -821341241L
    }
}
