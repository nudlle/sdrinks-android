package ru.nudlik.response

class StringResponse : DataResponse<String> {
    constructor()

    constructor(data: String, code: ServerResponseCode) : super(data, code)

    constructor(data: String) : super(data)

    constructor(code: ServerResponseCode) : super(code)
}
