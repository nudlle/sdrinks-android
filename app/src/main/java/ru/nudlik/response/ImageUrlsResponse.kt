package ru.nudlik.response

import ru.nudlik.domain.ImageUrls

class ImageUrlsResponse : DataResponse<ImageUrls> {
    constructor()

    constructor(data: ImageUrls, code: ServerResponseCode) : super(data, code)

    constructor(data: ImageUrls) : super(data)

    constructor(code: ServerResponseCode) : super(code)
}
