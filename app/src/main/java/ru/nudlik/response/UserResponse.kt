package ru.nudlik.response

import ru.nudlik.domain.User

class UserResponse : DataResponse<User> {
    constructor()

    constructor(data: User, code: ServerResponseCode) : super(data, code)

    constructor(data: User) : super(data)

    constructor(code: ServerResponseCode) : super(code)
}
