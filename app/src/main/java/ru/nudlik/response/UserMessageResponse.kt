package ru.nudlik.response

import ru.nudlik.domain.UserMessage

class UserMessageResponse : DataResponse<UserMessage> {
    constructor()

    constructor(data: UserMessage, code: ServerResponseCode) : super(data, code)

    constructor(data: UserMessage) : super(data)

    constructor(code: ServerResponseCode) : super(code)
}
