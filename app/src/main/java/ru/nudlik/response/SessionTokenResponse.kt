package ru.nudlik.response

import ru.nudlik.security.SessionToken

class SessionTokenResponse : DataResponse<SessionToken> {
    constructor()

    constructor(data: SessionToken, code: ServerResponseCode) : super(data, code)

    constructor(data: SessionToken) : super(data)

    constructor(code: ServerResponseCode) : super(code)
}
