package ru.nudlik.response

import ru.nudlik.domain.Recipe

class RecipeResponse : DataResponse<Recipe> {
    constructor()

    constructor(data: Recipe, code: ServerResponseCode) : super(data, code)

    constructor(data: Recipe) : super(data)

    constructor(code: ServerResponseCode) : super(code)
}
