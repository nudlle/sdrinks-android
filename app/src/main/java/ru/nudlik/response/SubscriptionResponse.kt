package ru.nudlik.response

import ru.nudlik.domain.Subscription

class SubscriptionResponse : DataResponse<Subscription> {
    constructor()

    constructor(data: Subscription, code: ServerResponseCode) : super(data, code)

    constructor(data: Subscription) : super(data)

    constructor(code: ServerResponseCode) : super(code)
}
