package ru.nudlik.response

import ru.nudlik.domain.UserComment

class CommentResponse : DataResponse<UserComment> {
    constructor()

    constructor(data: UserComment, code: ServerResponseCode) : super(data, code)

    constructor(data: UserComment) : super(data)

    constructor(code: ServerResponseCode) : super(code)
}
