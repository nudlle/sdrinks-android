package ru.nudlik.widget

import android.content.Context
import android.util.AttributeSet
import androidx.appcompat.widget.SearchView

class CustomSearchView : SearchView {
    private var actionListener: OnCollapsedExpandedListener? = null
    constructor(context: Context) : super(context)

    constructor(context: Context, attrs: AttributeSet) : super(context, attrs)

    constructor(context: Context, attrs: AttributeSet, defStyleAttr: Int) : super(context, attrs, defStyleAttr)

    override fun onActionViewCollapsed() {
        actionListener?.onCollapsed()
        super.onActionViewCollapsed()
    }

    override fun onActionViewExpanded() {
        actionListener?.onExpanded()
        super.onActionViewExpanded()
    }

    fun setOnCollapsedExpandedListener(listener: OnCollapsedExpandedListener) {
        actionListener = listener
    }

    interface OnCollapsedExpandedListener {
        fun onCollapsed()
        fun onExpanded()
    }
}