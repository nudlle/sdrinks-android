package ru.nudlik.widget

import android.animation.Animator
import android.animation.AnimatorSet
import android.animation.ObjectAnimator
import android.content.Context
import android.graphics.Canvas
import android.graphics.Color
import android.graphics.Paint
import android.util.AttributeSet
import android.view.Gravity
import android.view.View
import android.view.ViewGroup
import android.view.animation.*
import android.widget.FrameLayout
import ru.nudlik.R
import java.util.ArrayList
import kotlin.math.min

class PulseView
@JvmOverloads constructor(context: Context, attrs: AttributeSet? = null, defStyleAttr: Int = 0) : View(context, attrs, defStyleAttr) {

    private var duration: Int = 0
    private var repeat: Int = 0
    private var color: Int = 0
    private var interpolator: Int = 0
    private var delay: Int = 0

    private var mAnimatorSet: AnimatorSet? = null
    private var paint: Paint? = null
    private var radius: Float = 0.toFloat()
    private var centerX: Float = 0.toFloat()
    private var centerY: Float = 0.toFloat()
    private var isStarted: Boolean = false

    init {
        val attr = context.theme?.obtainStyledAttributes(
            attrs, R.styleable.PulseLayout, 0, 0
        )

        delay = DEFAULT_DELAY
        duration = DEFAULT_DURATION
        repeat = DEFAULT_REPEAT
        color = DEFAULT_COLOR
        interpolator = DEFAULT_INTERPOLATOR

        if (attr != null) {
            try {
                delay = attr.getInteger(R.styleable.PulseLayout_delay, DEFAULT_DELAY)
                duration = attr.getInteger(R.styleable.PulseLayout_duration, DEFAULT_DURATION)
                repeat = attr.getInteger(R.styleable.PulseLayout_repeat, DEFAULT_REPEAT)
                color = attr.getColor(R.styleable.PulseLayout_color, DEFAULT_COLOR)
                interpolator = attr.getInteger(R.styleable.PulseLayout_interpolator, DEFAULT_INTERPOLATOR)
            } finally {
                attr.recycle()
            }
        }

        paint = Paint()
        paint!!.isAntiAlias = true
        paint!!.style = Paint.Style.FILL
        paint!!.color = color

        build()
    }

    @Synchronized
    fun start() {
        if (mAnimatorSet == null || isStarted) {
            return
        }

        mAnimatorSet!!.start()
    }

    @Synchronized
    fun stop() {
        if (mAnimatorSet == null || !isStarted) {
            return
        }

        mAnimatorSet!!.end()
    }

    @Synchronized
    fun isStarted(): Boolean {
        return mAnimatorSet != null && isStarted
    }

    fun getDuration(): Int {
        return duration
    }

    fun setDuration(millis: Int) {
        if (millis < 0) {
            throw IllegalArgumentException("Duration is negative")
        }

        if (millis != duration) {
            duration = millis
            reset()
            invalidate()
        }
    }

    fun getColor(): Int {
        return color
    }

    fun setColor(color: Int) {
        if (color != this.color) {
            this.color = color

            if (paint != null) {
                paint!!.color = color
            }
        }
    }

    fun getInterpolator(): Int {
        return interpolator
    }

    fun setInterpolator(type: Int) {
        if (type != interpolator) {
            interpolator = type
            reset()
            invalidate()
        }
    }

    fun getDelay(): Int {
        return delay
    }

    fun setDelay(delay: Int) {
        if (delay != this.delay) {
            this.delay = delay
            reset()
            invalidate()
        }
    }

    private fun clear() {
        stop()
    }

    override fun onMeasure(widthMeasureSpec: Int, heightMeasureSpec: Int) {
        val width = MeasureSpec.getSize(widthMeasureSpec) - paddingLeft - paddingRight
        val height = MeasureSpec.getSize(heightMeasureSpec) - paddingTop - paddingBottom

        centerX = width * 0.5f
        centerY = height * 0.5f
        radius = min(width, height) * 0.5f

        super.onMeasure(widthMeasureSpec, heightMeasureSpec)
    }

    override fun onDraw(canvas: Canvas) {
        if (paint == null) {
            paint = Paint()
        }
        canvas.drawCircle(centerX, centerY, radius, paint!!)
    }

    private fun build() {
        val layoutParams = FrameLayout.LayoutParams(
            ViewGroup.LayoutParams.MATCH_PARENT,
            ViewGroup.LayoutParams.MATCH_PARENT
        )
        layoutParams.gravity = Gravity.CENTER

        val repeatCount = if (repeat == INFINITE) ObjectAnimator.INFINITE else repeat

        val animators = ArrayList<Animator>()

        scaleX = 0f
        scaleY = 0f
        alpha = 1f

        this.layoutParams = layoutParams

        val scaleXAnimator = ObjectAnimator.ofFloat(this, "ScaleX", 0f, 1f)
        scaleXAnimator.repeatCount = repeatCount
        scaleXAnimator.repeatMode = ObjectAnimator.RESTART
        scaleXAnimator.startDelay = delay.toLong()
        animators.add(scaleXAnimator)

        val scaleYAnimator = ObjectAnimator.ofFloat(this, "ScaleY", 0f, 1f)
        scaleYAnimator.repeatCount = repeatCount
        scaleYAnimator.repeatMode = ObjectAnimator.RESTART
        scaleYAnimator.startDelay = delay.toLong()
        animators.add(scaleYAnimator)

        val alphaAnimator = ObjectAnimator.ofFloat(this, "Alpha", 1f, 0f)
        alphaAnimator.repeatCount = repeatCount
        alphaAnimator.repeatMode = ObjectAnimator.RESTART
        alphaAnimator.startDelay = delay.toLong()
        animators.add(alphaAnimator)


        mAnimatorSet = AnimatorSet()
        mAnimatorSet!!.playTogether(animators)
        mAnimatorSet!!.interpolator = createInterpolator(interpolator)
        mAnimatorSet!!.duration = duration.toLong()
        mAnimatorSet!!.addListener(mAnimatorListener)

        //invalidate();
    }

    private fun reset() {
        val isStarted = isStarted()

        clear()
        build()

        if (isStarted) {
            start()
        }
    }

    private fun createInterpolator(type: Int): Interpolator {
        return when (type) {
            INTERPOLATOR_ACCELERATE -> AccelerateInterpolator()
            INTERPOLATOR_DECELERATE -> DecelerateInterpolator()
            INTERPOLATOR_ACCELERATE_DECELERATE -> AccelerateDecelerateInterpolator()
            else -> LinearInterpolator()
        }
    }

    override fun onDetachedFromWindow() {
        super.onDetachedFromWindow()

        if (mAnimatorSet != null) {
            mAnimatorSet!!.cancel()
            mAnimatorSet = null
        }
    }

    private val mAnimatorListener = object : Animator.AnimatorListener {

        override fun onAnimationStart(animator: Animator) {
            isStarted = true
        }

        override fun onAnimationEnd(animator: Animator) {
            isStarted = false
        }

        override fun onAnimationCancel(animator: Animator) {
            isStarted = false
        }

        override fun onAnimationRepeat(animator: Animator) {}

    }

    companion object {
        private const val INFINITE = 0

        const val INTERPOLATOR_LINEAR = 0
        const val INTERPOLATOR_ACCELERATE = 1
        const val INTERPOLATOR_DECELERATE = 2
        const val INTERPOLATOR_ACCELERATE_DECELERATE = 3

        private const val DEFAULT_DELAY = 0
        private const val DEFAULT_COLOR = Color.WHITE
        private const val DEFAULT_DURATION = 5000
        private const val DEFAULT_REPEAT = INFINITE
        private const val DEFAULT_INTERPOLATOR = INTERPOLATOR_LINEAR
    }
}