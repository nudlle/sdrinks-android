package ru.nudlik.widget

import android.animation.Animator
import android.animation.AnimatorSet
import android.animation.ObjectAnimator
import android.content.Context
import android.content.res.TypedArray
import android.graphics.*
import android.util.AttributeSet
import android.view.Gravity
import android.view.View
import android.view.ViewGroup
import android.view.animation.AccelerateDecelerateInterpolator
import android.view.animation.AccelerateInterpolator
import android.view.animation.DecelerateInterpolator
import android.view.animation.Interpolator
import android.view.animation.LinearInterpolator

import java.util.ArrayList

import android.widget.FrameLayout
import ru.nudlik.R
import kotlin.math.min

class PulseLayout

@JvmOverloads constructor(context: Context, attrs: AttributeSet? = null, defStyleAttr: Int = 0) :
    FrameLayout(context, attrs, defStyleAttr) {

    private var duration: Int = 0
    private var repeat: Int = 0
    private var color: Int = 0
    private var interpolator: Int = 0
    private var delay: Int = 0

    private val mViews = ArrayList<View>()
    private var mAnimatorSet: AnimatorSet? = null
    private val paint: Paint?
    private var radius: Float = 0.toFloat()
    private var centerX: Float = 0.toFloat()
    private var centerY: Float = 0.toFloat()
    private var isStarted: Boolean = false

    private val mAnimatorListener = object : Animator.AnimatorListener {

        override fun onAnimationStart(animator: Animator) {
            isStarted = true
        }

        override fun onAnimationEnd(animator: Animator) {
            isStarted = false
        }

        override fun onAnimationCancel(animator: Animator) {
            isStarted = false
        }

        override fun onAnimationRepeat(animator: Animator) {}

    }

    init {

        val attr = context.theme.obtainStyledAttributes(
            attrs, R.styleable.PulseLayout, 0, 0
        )

        delay = DEFAULT_DELAY
        duration = DEFAULT_DURATION
        repeat = DEFAULT_REPEAT
        color = DEFAULT_COLOR
        interpolator = DEFAULT_INTERPOLATOR

        try {
            delay = attr.getInteger(R.styleable.PulseLayout_delay, DEFAULT_DELAY)
            duration = attr.getInteger(
                R.styleable.PulseLayout_duration,
                DEFAULT_DURATION
            )
            repeat = attr.getInteger(R.styleable.PulseLayout_repeat, DEFAULT_REPEAT)
            color = attr.getColor(R.styleable.PulseLayout_color, DEFAULT_COLOR)
            interpolator = attr.getInteger(
                R.styleable.PulseLayout_interpolator,
                DEFAULT_INTERPOLATOR
            )
        } finally {
            attr.recycle()
        }

        paint = Paint()
        paint.isAntiAlias = true
        paint.style = Paint.Style.FILL
        paint.color = color

        build()
    }

    @Synchronized
    fun start() {
        if (mAnimatorSet == null || isStarted) {
            return
        }

        mAnimatorSet!!.start()
    }

    @Synchronized
    fun stop() {
        if (mAnimatorSet == null || !isStarted) {
            return
        }

        mAnimatorSet!!.end()
    }

    @Synchronized
    fun isStarted(): Boolean {
        return mAnimatorSet != null && isStarted
    }

    fun getDuration(): Int {
        return duration
    }

    fun setDuration(millis: Int) {
        if (millis < 0) {
            throw IllegalArgumentException("Duration cannot be negative")
        }

        if (millis != duration) {
            duration = millis
            reset()
            invalidate()
        }
    }

    fun getColor(): Int {
        return color
    }

    fun setColor(color: Int) {
        if (color != this.color) {
            this.color = color

            if (paint != null) {
                paint.color = color
            }
        }
    }

    fun getInterpolator(): Int {
        return interpolator
    }

    fun setInterpolator(type: Int) {
        if (type != interpolator) {
            interpolator = type
            reset()
            invalidate()
        }
    }

    fun getDelay(): Int {
        return delay
    }

    fun setDelay(delay: Int) {
        if (delay != this.delay) {
            this.delay = delay
            reset()
            invalidate()
        }
    }

    public override fun onMeasure(widthMeasureSpec: Int, heightMeasureSpec: Int) {
        val width = MeasureSpec.getSize(widthMeasureSpec) - paddingLeft - paddingRight
        val height = MeasureSpec.getSize(heightMeasureSpec) - paddingTop - paddingBottom

        centerX = width * 0.5f
        centerY = height * 0.5f
        radius = min(width, height) * 0.5f

        super.onMeasure(widthMeasureSpec, heightMeasureSpec)
    }

    private fun clear() {
        stop()

        for (view in mViews) {
            removeView(view)
        }
        mViews.clear()
    }

    private fun build() {
        val layoutParams = LayoutParams(
            ViewGroup.LayoutParams.MATCH_PARENT,
            ViewGroup.LayoutParams.MATCH_PARENT
        )
        layoutParams.gravity = Gravity.CENTER

        val repeatCount = if (repeat == INFINITE) ObjectAnimator.INFINITE else repeat

        val animators = ArrayList<Animator>()

        val pulseView = PulseView(context)
        pulseView.scaleX = 0f
        pulseView.scaleY = 0f
        pulseView.alpha = 1f

        addView(pulseView, 0, layoutParams)
        mViews.add(pulseView)

        val scaleXAnimator = ObjectAnimator.ofFloat(pulseView, "ScaleX", 0f, 1f)
        scaleXAnimator.repeatCount = repeatCount
        scaleXAnimator.repeatMode = ObjectAnimator.RESTART
        animators.add(scaleXAnimator)

        val scaleYAnimator = ObjectAnimator.ofFloat(pulseView, "ScaleY", 0f, 1f)
        scaleYAnimator.repeatCount = repeatCount
        scaleYAnimator.repeatMode = ObjectAnimator.RESTART
        animators.add(scaleYAnimator)

        val alphaAnimator = ObjectAnimator.ofFloat(pulseView, "Alpha", 1f, 0f)
        alphaAnimator.repeatCount = repeatCount
        alphaAnimator.repeatMode = ObjectAnimator.RESTART
        animators.add(alphaAnimator)


        mAnimatorSet = AnimatorSet()
        mAnimatorSet!!.playTogether(animators)
        mAnimatorSet!!.interpolator = createInterpolator(interpolator)
        mAnimatorSet!!.duration = duration.toLong()
        mAnimatorSet!!.startDelay = delay.toLong()
        mAnimatorSet!!.addListener(mAnimatorListener)
    }

    private fun reset() {
        val isStarted = isStarted()

        clear()
        build()

        if (isStarted) {
            start()
        }
    }

    override fun onDetachedFromWindow() {
        super.onDetachedFromWindow()

        if (mAnimatorSet != null) {
            mAnimatorSet!!.cancel()
            mAnimatorSet = null
        }
    }

    private inner class PulseView(context: Context) : View(context) {

        override fun onDraw(canvas: Canvas) {
            if (width == height) {
                canvas.drawCircle(centerX, centerY, radius, paint!!)
            } else {
                canvas.drawRoundRect(0f, height.toFloat(), width.toFloat(), 0f, radius, radius, paint!!)
            }
        }
    }

    companion object {

        private const val INFINITE = 0

        const val INTERPOLATOR_LINEAR = 0
        const val INTERPOLATOR_ACCELERATE = 1
        const val INTERPOLATOR_DECELERATE = 2
        const val INTERPOLATOR_ACCELERATE_DECELERATE = 3

        private const val DEFAULT_DELAY = 0
        private const val DEFAULT_COLOR = Color.WHITE
        private const val DEFAULT_DURATION = 5000
        private const val DEFAULT_REPEAT = INFINITE
        private const val DEFAULT_INTERPOLATOR = INTERPOLATOR_LINEAR

        private fun createInterpolator(type: Int): Interpolator {
            return when (type) {
                INTERPOLATOR_ACCELERATE -> AccelerateInterpolator()
                INTERPOLATOR_DECELERATE -> DecelerateInterpolator()
                INTERPOLATOR_ACCELERATE_DECELERATE -> AccelerateDecelerateInterpolator()
                else -> LinearInterpolator()
            }
        }
    }

}