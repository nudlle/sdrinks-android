package ru.nudlik.widget;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.LinearLayout;
import android.widget.RadioGroup;
import androidx.annotation.Nullable;

public class RadioGroup2Columns extends LinearLayout {
    private RadioGroup rg1;
    private RadioGroup rg2;

    private int resId1 = -1;
    private int resId2 = -1;

    private OnCheckedChangeListener checkedChangeListener;

    private RadioGroup.OnCheckedChangeListener listener1 = new RadioGroup.OnCheckedChangeListener() {
        @Override
        public void onCheckedChanged(RadioGroup radioGroup, int i) {
            if (i != -1) {
                rg2.setOnCheckedChangeListener(null);
                rg2.clearCheck();
                rg2.setOnCheckedChangeListener(listener2);
            }
            if (checkedChangeListener != null) {
                checkedChangeListener.onCheckedChanged(radioGroup, i);
            }

        }
    };

    private RadioGroup.OnCheckedChangeListener listener2 = new RadioGroup.OnCheckedChangeListener() {
        @Override
        public void onCheckedChanged(RadioGroup radioGroup, int i) {
            if (i != -1) {
                rg1.setOnCheckedChangeListener(null);
                rg1.clearCheck();
                rg1.setOnCheckedChangeListener(listener1);
            }
            if (checkedChangeListener != null) {
                checkedChangeListener.onCheckedChanged(radioGroup, i);
            }
        }
    };
    public RadioGroup2Columns(Context context) {
        super(context);
    }

    public RadioGroup2Columns(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
    }

    public RadioGroup2Columns(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @Override
    protected void onLayout(boolean changed, int l, int t, int r, int b) {
        super.onLayout(changed, l, t, r, b);

        if (getChildCount() != 2) {
            throw new IllegalStateException("Must be at least 2 child RadioGroups");
        }
        Object fRG = getChildAt(0);
        Object sRG = getChildAt(1);

        if (!(fRG instanceof RadioGroup) || !(sRG instanceof RadioGroup)) {
            throw new IllegalStateException("Must be at least 2 child RadioGroups");
        }

        this.rg1 = (RadioGroup) fRG;
        this.rg2 = (RadioGroup) sRG;

        this.rg1.setOnCheckedChangeListener(listener1);
        this.rg2.setOnCheckedChangeListener(listener2);

        if (resId1 != -1) {
            this.rg1.check(resId1);
        }
        if (resId2 != -1) {
            this.rg2.check(resId2);
        }
    }

    public void setOnCheckedChangeListener(OnCheckedChangeListener checkedChangeListener) {
        this.checkedChangeListener = checkedChangeListener;
    }

    public interface OnCheckedChangeListener {
        void onCheckedChanged(RadioGroup radioGroup, int id);
    }

    public void checkFirstRG(int resId) {
        if (rg1 == null) {
            resId1 = resId;
        } else {
            rg1.check(resId);
        }
    }

    public void checkSecondRG(int resId) {
        if (rg2 == null) {
            resId2 = resId;
        } else {
            rg2.check(resId);
        }
    }
}
